﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[34] = 
{
	{ 0, 0 } /* 0x06000001 System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean) */,
	{ 0, 0 } /* 0x06000002 System.Boolean UnityEngine.AudioSettings::StartAudioOutput() */,
	{ 0, 0 } /* 0x06000003 System.Boolean UnityEngine.AudioSettings::StopAudioOutput() */,
	{ 0, 0 } /* 0x06000004 System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0 } /* 0x06000005 System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean) */,
	{ 0, 0 } /* 0x06000006 System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object) */,
	{ 0, 0 } /* 0x06000007 System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult) */,
	{ 0, 0 } /* 0x06000008 System.Boolean UnityEngine.AudioSettings/Mobile::get_muteState() */,
	{ 0, 0 } /* 0x06000009 System.Void UnityEngine.AudioSettings/Mobile::set_muteState(System.Boolean) */,
	{ 0, 0 } /* 0x0600000A System.Boolean UnityEngine.AudioSettings/Mobile::get_stopAudioOutputOnMute() */,
	{ 0, 0 } /* 0x0600000B System.Void UnityEngine.AudioSettings/Mobile::InvokeOnMuteStateChanged(System.Boolean) */,
	{ 0, 0 } /* 0x0600000C System.Void UnityEngine.AudioSettings/Mobile::StartAudioOutput() */,
	{ 0, 0 } /* 0x0600000D System.Void UnityEngine.AudioSettings/Mobile::StopAudioOutput() */,
	{ 0, 0 } /* 0x0600000E System.Void UnityEngine.AudioSettings/Mobile::.cctor() */,
	{ 0, 0 } /* 0x0600000F System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[]) */,
	{ 0, 0 } /* 0x06000010 System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32) */,
	{ 0, 0 } /* 0x06000011 System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0 } /* 0x06000012 System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[]) */,
	{ 0, 0 } /* 0x06000013 System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object) */,
	{ 0, 0 } /* 0x06000014 System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult) */,
	{ 0, 0 } /* 0x06000015 System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0 } /* 0x06000016 System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32) */,
	{ 0, 0 } /* 0x06000017 System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object) */,
	{ 0, 0 } /* 0x06000018 System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult) */,
	{ 0, 0 } /* 0x06000019 UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle() */,
	{ 0, 0 } /* 0x0600001A System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable) */,
	{ 0, 0 } /* 0x0600001B UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle() */,
	{ 0, 0 } /* 0x0600001C System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable) */,
	{ 0, 0 } /* 0x0600001D System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesAvailable(System.Int32) */,
	{ 0, 0 } /* 0x0600001E System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesOverflow(System.Int32) */,
	{ 0, 0 } /* 0x0600001F System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0 } /* 0x06000020 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::Invoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32) */,
	{ 0, 0 } /* 0x06000021 System.IAsyncResult UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::BeginInvoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32,System.AsyncCallback,System.Object) */,
	{ 0, 0 } /* 0x06000022 System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::EndInvoke(System.IAsyncResult) */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
IL2CPP_EXTERN_C Il2CppSequencePoint g_sequencePointsUnityEngine_AudioModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_AudioModule[120] = 
{
	{ 10082, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 0 } /* seqPointIndex: 0 */,
	{ 10082, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 1 } /* seqPointIndex: 1 */,
	{ 10082, 1, 331, 331, 9, 10, 0, kSequencePointKind_Normal, 0, 2 } /* seqPointIndex: 2 */,
	{ 10082, 1, 332, 332, 13, 53, 1, kSequencePointKind_Normal, 0, 3 } /* seqPointIndex: 3 */,
	{ 10082, 1, 332, 332, 0, 0, 10, kSequencePointKind_Normal, 0, 4 } /* seqPointIndex: 4 */,
	{ 10082, 1, 333, 333, 17, 63, 13, kSequencePointKind_Normal, 0, 5 } /* seqPointIndex: 5 */,
	{ 10082, 1, 334, 334, 9, 10, 25, kSequencePointKind_Normal, 0, 6 } /* seqPointIndex: 6 */,
	{ 10082, 1, 333, 333, 17, 63, 19, kSequencePointKind_StepOut, 0, 7 } /* seqPointIndex: 7 */,
	{ 10089, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 8 } /* seqPointIndex: 8 */,
	{ 10089, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 9 } /* seqPointIndex: 9 */,
	{ 10089, 1, 359, 359, 17, 21, 0, kSequencePointKind_Normal, 0, 10 } /* seqPointIndex: 10 */,
	{ 10090, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 11 } /* seqPointIndex: 11 */,
	{ 10090, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 12 } /* seqPointIndex: 12 */,
	{ 10090, 1, 360, 360, 17, 29, 0, kSequencePointKind_Normal, 0, 13 } /* seqPointIndex: 13 */,
	{ 10091, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 14 } /* seqPointIndex: 14 */,
	{ 10091, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 15 } /* seqPointIndex: 15 */,
	{ 10091, 1, 367, 367, 17, 18, 0, kSequencePointKind_Normal, 0, 16 } /* seqPointIndex: 16 */,
	{ 10091, 1, 368, 368, 21, 51, 1, kSequencePointKind_Normal, 0, 17 } /* seqPointIndex: 17 */,
	{ 10091, 1, 369, 369, 17, 18, 9, kSequencePointKind_Normal, 0, 18 } /* seqPointIndex: 18 */,
	{ 10092, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 19 } /* seqPointIndex: 19 */,
	{ 10092, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 20 } /* seqPointIndex: 20 */,
	{ 10092, 1, 392, 392, 13, 14, 0, kSequencePointKind_Normal, 0, 21 } /* seqPointIndex: 21 */,
	{ 10092, 1, 393, 393, 17, 39, 1, kSequencePointKind_Normal, 0, 22 } /* seqPointIndex: 22 */,
	{ 10092, 1, 393, 393, 0, 0, 13, kSequencePointKind_Normal, 0, 23 } /* seqPointIndex: 23 */,
	{ 10092, 1, 394, 394, 17, 18, 16, kSequencePointKind_Normal, 0, 24 } /* seqPointIndex: 24 */,
	{ 10092, 1, 395, 395, 21, 38, 17, kSequencePointKind_Normal, 0, 25 } /* seqPointIndex: 25 */,
	{ 10092, 1, 396, 396, 21, 47, 24, kSequencePointKind_Normal, 0, 26 } /* seqPointIndex: 26 */,
	{ 10092, 1, 396, 396, 0, 0, 30, kSequencePointKind_Normal, 0, 27 } /* seqPointIndex: 27 */,
	{ 10092, 1, 397, 397, 21, 22, 33, kSequencePointKind_Normal, 0, 28 } /* seqPointIndex: 28 */,
	{ 10092, 1, 398, 398, 25, 39, 34, kSequencePointKind_Normal, 0, 29 } /* seqPointIndex: 29 */,
	{ 10092, 1, 398, 398, 0, 0, 40, kSequencePointKind_Normal, 0, 30 } /* seqPointIndex: 30 */,
	{ 10092, 1, 399, 399, 29, 47, 43, kSequencePointKind_Normal, 0, 31 } /* seqPointIndex: 31 */,
	{ 10092, 1, 401, 401, 29, 48, 51, kSequencePointKind_Normal, 0, 32 } /* seqPointIndex: 32 */,
	{ 10092, 1, 402, 402, 21, 22, 57, kSequencePointKind_Normal, 0, 33 } /* seqPointIndex: 33 */,
	{ 10092, 1, 403, 403, 21, 52, 58, kSequencePointKind_Normal, 0, 34 } /* seqPointIndex: 34 */,
	{ 10092, 1, 403, 403, 0, 0, 67, kSequencePointKind_Normal, 0, 35 } /* seqPointIndex: 35 */,
	{ 10092, 1, 404, 404, 25, 50, 70, kSequencePointKind_Normal, 0, 36 } /* seqPointIndex: 36 */,
	{ 10092, 1, 405, 405, 17, 18, 82, kSequencePointKind_Normal, 0, 37 } /* seqPointIndex: 37 */,
	{ 10092, 1, 406, 406, 13, 14, 83, kSequencePointKind_Normal, 0, 38 } /* seqPointIndex: 38 */,
	{ 10092, 1, 393, 393, 17, 39, 2, kSequencePointKind_StepOut, 0, 39 } /* seqPointIndex: 39 */,
	{ 10092, 1, 395, 395, 21, 38, 18, kSequencePointKind_StepOut, 0, 40 } /* seqPointIndex: 40 */,
	{ 10092, 1, 396, 396, 21, 47, 24, kSequencePointKind_StepOut, 0, 41 } /* seqPointIndex: 41 */,
	{ 10092, 1, 398, 398, 25, 39, 34, kSequencePointKind_StepOut, 0, 42 } /* seqPointIndex: 42 */,
	{ 10092, 1, 399, 399, 29, 47, 43, kSequencePointKind_StepOut, 0, 43 } /* seqPointIndex: 43 */,
	{ 10092, 1, 401, 401, 29, 48, 51, kSequencePointKind_StepOut, 0, 44 } /* seqPointIndex: 44 */,
	{ 10092, 1, 404, 404, 25, 50, 76, kSequencePointKind_StepOut, 0, 45 } /* seqPointIndex: 45 */,
	{ 10093, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 46 } /* seqPointIndex: 46 */,
	{ 10093, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 47 } /* seqPointIndex: 47 */,
	{ 10093, 1, 409, 409, 13, 14, 0, kSequencePointKind_Normal, 0, 48 } /* seqPointIndex: 48 */,
	{ 10093, 1, 410, 410, 17, 50, 1, kSequencePointKind_Normal, 0, 49 } /* seqPointIndex: 49 */,
	{ 10093, 1, 411, 411, 13, 14, 7, kSequencePointKind_Normal, 0, 50 } /* seqPointIndex: 50 */,
	{ 10093, 1, 410, 410, 17, 50, 1, kSequencePointKind_StepOut, 0, 51 } /* seqPointIndex: 51 */,
	{ 10094, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 52 } /* seqPointIndex: 52 */,
	{ 10094, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 53 } /* seqPointIndex: 53 */,
	{ 10094, 1, 414, 414, 13, 14, 0, kSequencePointKind_Normal, 0, 54 } /* seqPointIndex: 54 */,
	{ 10094, 1, 415, 415, 17, 49, 1, kSequencePointKind_Normal, 0, 55 } /* seqPointIndex: 55 */,
	{ 10094, 1, 416, 416, 13, 14, 7, kSequencePointKind_Normal, 0, 56 } /* seqPointIndex: 56 */,
	{ 10094, 1, 415, 415, 17, 49, 1, kSequencePointKind_StepOut, 0, 57 } /* seqPointIndex: 57 */,
	{ 10095, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 58 } /* seqPointIndex: 58 */,
	{ 10095, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 59 } /* seqPointIndex: 59 */,
	{ 10095, 1, 361, 361, 17, 22, 0, kSequencePointKind_Normal, 0, 60 } /* seqPointIndex: 60 */,
	{ 10095, 1, 363, 363, 13, 64, 6, kSequencePointKind_Normal, 0, 61 } /* seqPointIndex: 61 */,
	{ 10096, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 62 } /* seqPointIndex: 62 */,
	{ 10096, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 63 } /* seqPointIndex: 63 */,
	{ 10096, 1, 606, 606, 9, 10, 0, kSequencePointKind_Normal, 0, 64 } /* seqPointIndex: 64 */,
	{ 10096, 1, 607, 607, 13, 45, 1, kSequencePointKind_Normal, 0, 65 } /* seqPointIndex: 65 */,
	{ 10096, 1, 607, 607, 0, 0, 11, kSequencePointKind_Normal, 0, 66 } /* seqPointIndex: 66 */,
	{ 10096, 1, 608, 608, 17, 43, 14, kSequencePointKind_Normal, 0, 67 } /* seqPointIndex: 67 */,
	{ 10096, 1, 609, 609, 9, 10, 27, kSequencePointKind_Normal, 0, 68 } /* seqPointIndex: 68 */,
	{ 10096, 1, 608, 608, 17, 43, 21, kSequencePointKind_StepOut, 0, 69 } /* seqPointIndex: 69 */,
	{ 10097, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 70 } /* seqPointIndex: 70 */,
	{ 10097, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 71 } /* seqPointIndex: 71 */,
	{ 10097, 1, 613, 613, 9, 10, 0, kSequencePointKind_Normal, 0, 72 } /* seqPointIndex: 72 */,
	{ 10097, 1, 614, 614, 13, 50, 1, kSequencePointKind_Normal, 0, 73 } /* seqPointIndex: 73 */,
	{ 10097, 1, 614, 614, 0, 0, 11, kSequencePointKind_Normal, 0, 74 } /* seqPointIndex: 74 */,
	{ 10097, 1, 615, 615, 17, 52, 14, kSequencePointKind_Normal, 0, 75 } /* seqPointIndex: 75 */,
	{ 10097, 1, 616, 616, 9, 10, 27, kSequencePointKind_Normal, 0, 76 } /* seqPointIndex: 76 */,
	{ 10097, 1, 615, 615, 17, 52, 21, kSequencePointKind_StepOut, 0, 77 } /* seqPointIndex: 77 */,
	{ 10106, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 78 } /* seqPointIndex: 78 */,
	{ 10106, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 79 } /* seqPointIndex: 79 */,
	{ 10106, 2, 49, 49, 9, 10, 0, kSequencePointKind_Normal, 0, 80 } /* seqPointIndex: 80 */,
	{ 10106, 2, 50, 50, 13, 29, 1, kSequencePointKind_Normal, 0, 81 } /* seqPointIndex: 81 */,
	{ 10106, 2, 51, 51, 9, 10, 10, kSequencePointKind_Normal, 0, 82 } /* seqPointIndex: 82 */,
	{ 10107, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 83 } /* seqPointIndex: 83 */,
	{ 10107, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 84 } /* seqPointIndex: 84 */,
	{ 10107, 2, 64, 64, 9, 10, 0, kSequencePointKind_Normal, 0, 85 } /* seqPointIndex: 85 */,
	{ 10107, 2, 65, 65, 13, 53, 1, kSequencePointKind_Normal, 0, 86 } /* seqPointIndex: 86 */,
	{ 10107, 2, 66, 66, 9, 10, 22, kSequencePointKind_Normal, 0, 87 } /* seqPointIndex: 87 */,
	{ 10107, 2, 65, 65, 13, 53, 2, kSequencePointKind_StepOut, 0, 88 } /* seqPointIndex: 88 */,
	{ 10107, 2, 65, 65, 13, 53, 9, kSequencePointKind_StepOut, 0, 89 } /* seqPointIndex: 89 */,
	{ 10107, 2, 65, 65, 13, 53, 14, kSequencePointKind_StepOut, 0, 90 } /* seqPointIndex: 90 */,
	{ 10108, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 91 } /* seqPointIndex: 91 */,
	{ 10108, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 92 } /* seqPointIndex: 92 */,
	{ 10108, 3, 47, 47, 9, 10, 0, kSequencePointKind_Normal, 0, 93 } /* seqPointIndex: 93 */,
	{ 10108, 3, 48, 48, 13, 29, 1, kSequencePointKind_Normal, 0, 94 } /* seqPointIndex: 94 */,
	{ 10108, 3, 49, 49, 9, 10, 10, kSequencePointKind_Normal, 0, 95 } /* seqPointIndex: 95 */,
	{ 10109, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 96 } /* seqPointIndex: 96 */,
	{ 10109, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 97 } /* seqPointIndex: 97 */,
	{ 10109, 3, 62, 62, 9, 10, 0, kSequencePointKind_Normal, 0, 98 } /* seqPointIndex: 98 */,
	{ 10109, 3, 63, 63, 13, 53, 1, kSequencePointKind_Normal, 0, 99 } /* seqPointIndex: 99 */,
	{ 10109, 3, 64, 64, 9, 10, 22, kSequencePointKind_Normal, 0, 100 } /* seqPointIndex: 100 */,
	{ 10109, 3, 63, 63, 13, 53, 2, kSequencePointKind_StepOut, 0, 101 } /* seqPointIndex: 101 */,
	{ 10109, 3, 63, 63, 13, 53, 9, kSequencePointKind_StepOut, 0, 102 } /* seqPointIndex: 102 */,
	{ 10109, 3, 63, 63, 13, 53, 14, kSequencePointKind_StepOut, 0, 103 } /* seqPointIndex: 103 */,
	{ 10110, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 104 } /* seqPointIndex: 104 */,
	{ 10110, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 105 } /* seqPointIndex: 105 */,
	{ 10110, 4, 177, 177, 9, 10, 0, kSequencePointKind_Normal, 0, 106 } /* seqPointIndex: 106 */,
	{ 10110, 4, 178, 178, 13, 47, 1, kSequencePointKind_Normal, 0, 107 } /* seqPointIndex: 107 */,
	{ 10110, 4, 178, 178, 0, 0, 11, kSequencePointKind_Normal, 0, 108 } /* seqPointIndex: 108 */,
	{ 10110, 4, 180, 180, 17, 69, 14, kSequencePointKind_Normal, 0, 109 } /* seqPointIndex: 109 */,
	{ 10110, 4, 181, 181, 9, 10, 28, kSequencePointKind_Normal, 0, 110 } /* seqPointIndex: 110 */,
	{ 10110, 4, 180, 180, 17, 69, 22, kSequencePointKind_StepOut, 0, 111 } /* seqPointIndex: 111 */,
	{ 10111, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 112 } /* seqPointIndex: 112 */,
	{ 10111, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 113 } /* seqPointIndex: 113 */,
	{ 10111, 4, 185, 185, 9, 10, 0, kSequencePointKind_Normal, 0, 114 } /* seqPointIndex: 114 */,
	{ 10111, 4, 186, 186, 13, 46, 1, kSequencePointKind_Normal, 0, 115 } /* seqPointIndex: 115 */,
	{ 10111, 4, 186, 186, 0, 0, 11, kSequencePointKind_Normal, 0, 116 } /* seqPointIndex: 116 */,
	{ 10111, 4, 187, 187, 17, 75, 14, kSequencePointKind_Normal, 0, 117 } /* seqPointIndex: 117 */,
	{ 10111, 4, 188, 188, 9, 10, 28, kSequencePointKind_Normal, 0, 118 } /* seqPointIndex: 118 */,
	{ 10111, 4, 187, 187, 17, 75, 22, kSequencePointKind_StepOut, 0, 119 } /* seqPointIndex: 119 */,
};
#else
extern Il2CppSequencePoint g_sequencePointsUnityEngine_AudioModule[];
Il2CppSequencePoint g_sequencePointsUnityEngine_AudioModule[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[] = {
{ "", { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} }, //0 
{ "/Users/bokken/buildslave/unity/build/Modules/Audio/Public/ScriptBindings/Audio.bindings.cs", { 95, 147, 0, 251, 184, 84, 165, 121, 7, 96, 150, 235, 78, 163, 72, 225} }, //1 
{ "/Users/bokken/buildslave/unity/build/Modules/Audio/Public/ScriptBindings/AudioClipPlayable.bindings.cs", { 136, 110, 11, 239, 4, 37, 180, 165, 136, 112, 116, 151, 134, 78, 48, 235} }, //2 
{ "/Users/bokken/buildslave/unity/build/Modules/Audio/Public/ScriptBindings/AudioMixerPlayable.bindings.cs", { 61, 101, 161, 191, 246, 243, 230, 247, 173, 244, 46, 184, 65, 58, 4, 90} }, //3 
{ "/Users/bokken/buildslave/unity/build/Modules/Audio/Public/ScriptBindings/AudioSampleProvider.bindings.cs", { 47, 120, 50, 45, 60, 26, 245, 52, 137, 63, 13, 94, 178, 230, 94, 160} }, //4 
};
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[6] = 
{
	{ 1846, 1 },
	{ 1845, 1 },
	{ 1849, 1 },
	{ 1852, 2 },
	{ 1853, 3 },
	{ 1856, 4 },
};
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[11] = 
{
	{ 0, 26 },
	{ 0, 11 },
	{ 0, 84 },
	{ 0, 28 },
	{ 0, 28 },
	{ 0, 12 },
	{ 0, 24 },
	{ 0, 12 },
	{ 0, 24 },
	{ 0, 29 },
	{ 0, 29 },
};
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[34] = 
{
	{ 26, 0, 1 } /* System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.AudioSettings::StartAudioOutput() */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.AudioSettings::StopAudioOutput() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean) */,
	{ 0, 0, 0 } /* System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult) */,
	{ 0, 0, 0 } /* System.Boolean UnityEngine.AudioSettings/Mobile::get_muteState() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioSettings/Mobile::set_muteState(System.Boolean) */,
	{ 11, 1, 1 } /* System.Boolean UnityEngine.AudioSettings/Mobile::get_stopAudioOutputOnMute() */,
	{ 84, 2, 1 } /* System.Void UnityEngine.AudioSettings/Mobile::InvokeOnMuteStateChanged(System.Boolean) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioSettings/Mobile::StartAudioOutput() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioSettings/Mobile::StopAudioOutput() */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioSettings/Mobile::.cctor() */,
	{ 28, 3, 1 } /* System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[]) */,
	{ 28, 4, 1 } /* System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[]) */,
	{ 0, 0, 0 } /* System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32) */,
	{ 0, 0, 0 } /* System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult) */,
	{ 12, 5, 1 } /* UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioClipPlayable::GetHandle() */,
	{ 24, 6, 1 } /* System.Boolean UnityEngine.Audio.AudioClipPlayable::Equals(UnityEngine.Audio.AudioClipPlayable) */,
	{ 12, 7, 1 } /* UnityEngine.Playables.PlayableHandle UnityEngine.Audio.AudioMixerPlayable::GetHandle() */,
	{ 24, 8, 1 } /* System.Boolean UnityEngine.Audio.AudioMixerPlayable::Equals(UnityEngine.Audio.AudioMixerPlayable) */,
	{ 29, 9, 1 } /* System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesAvailable(System.Int32) */,
	{ 29, 10, 1 } /* System.Void UnityEngine.Experimental.Audio.AudioSampleProvider::InvokeSampleFramesOverflow(System.Int32) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::.ctor(System.Object,System.IntPtr) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::Invoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32) */,
	{ 0, 0, 0 } /* System.IAsyncResult UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::BeginInvoke(UnityEngine.Experimental.Audio.AudioSampleProvider,System.UInt32,System.AsyncCallback,System.Object) */,
	{ 0, 0, 0 } /* System.Void UnityEngine.Experimental.Audio.AudioSampleProvider/SampleFramesHandler::EndInvoke(System.IAsyncResult) */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
IL2CPP_EXTERN_C const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_AudioModule;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationUnityEngine_AudioModule = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	120,
	(Il2CppSequencePoint*)g_sequencePointsUnityEngine_AudioModule,
	0,
	(Il2CppCatchPoint*)g_catchPoints,
	6,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
