﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[5] = 
{
	{ 2301, 0,  1 } /*tableIndex: 0 */,
	{ 3541, 1,  2 } /*tableIndex: 1 */,
	{ 4233, 2,  3 } /*tableIndex: 2 */,
	{ 4230, 3,  3 } /*tableIndex: 3 */,
	{ 3443, 1,  5 } /*tableIndex: 4 */,
};
#else
static const Il2CppMethodExecutionContextInfo g_methodExecutionContextInfos[1] = { { 0, 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const char* g_methodExecutionContextInfoStrings[4] = 
{
	"obj",
	"test",
	"json",
	"typeEnum",
};
#else
static const char* g_methodExecutionContextInfoStrings[1] = { NULL };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[16] = 
{
	{ 0, 0 } /* 0x06000001 System.Void PhotoBridgeIos::LoadPhoto() */,
	{ 0, 0 } /* 0x06000002 System.Void PhotoBridgeIos::ReceiveMsgFromUnity(System.Int32,System.String) */,
	{ 0, 1 } /* 0x06000003 PhotoBridgeIos PhotoBridgeIos::get_Instance() */,
	{ 1, 1 } /* 0x06000004 System.Void PhotoBridgeIos::Start() */,
	{ 0, 0 } /* 0x06000005 System.Void PhotoBridgeIos::SendMessage(System.Int32,System.String) */,
	{ 2, 2 } /* 0x06000006 System.Void PhotoBridgeIos::ReceiveMsg(System.String) */,
	{ 0, 0 } /* 0x06000007 System.Void PhotoBridgeIos::.ctor() */,
	{ 0, 0 } /* 0x06000008 System.Void PhotoBridgeIos/PhotoBridgeTranslation::.ctor() */,
	{ 0, 0 } /* 0x06000009 PhotoBridgeIos/ReceiveMsgFromIos PhotoBridgeIos/ReceiveMsgFromIos::CreateFromJSON(System.String) */,
	{ 0, 0 } /* 0x0600000A System.Void PhotoBridgeIos/ReceiveMsgFromIos::.ctor() */,
	{ 0, 0 } /* 0x0600000B System.Void TestServicePhotoIos::Start() */,
	{ 0, 0 } /* 0x0600000C System.Void TestServicePhotoIos::Update() */,
	{ 0, 0 } /* 0x0600000D System.Void TestServicePhotoIos::OnDestroy() */,
	{ 0, 0 } /* 0x0600000E System.Void TestServicePhotoIos::testCommIos() */,
	{ 4, 1 } /* 0x0600000F System.Void TestServicePhotoIos::ReceiveMessageCallbackHandler(PhotoBridgeIos/ApbMsgType,System.String) */,
	{ 0, 0 } /* 0x06000010 System.Void TestServicePhotoIos::.ctor() */,
};
#else
static const Il2CppMethodExecutionContextInfoIndex g_methodExecutionContextInfoIndexes[1] = { { 0, 0} };
#endif
#if IL2CPP_MONO_DEBUGGER
IL2CPP_EXTERN_C Il2CppSequencePoint g_sequencePointsAssemblyU2DCSharp[];
Il2CppSequencePoint g_sequencePointsAssemblyU2DCSharp[121] = 
{
	{ 10274, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 0 } /* seqPointIndex: 0 */,
	{ 10274, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 1 } /* seqPointIndex: 1 */,
	{ 10274, 1, 50, 50, 9, 10, 0, kSequencePointKind_Normal, 0, 2 } /* seqPointIndex: 2 */,
	{ 10274, 1, 51, 51, 13, 35, 1, kSequencePointKind_Normal, 0, 3 } /* seqPointIndex: 3 */,
	{ 10274, 1, 51, 51, 0, 0, 13, kSequencePointKind_Normal, 0, 4 } /* seqPointIndex: 4 */,
	{ 10274, 1, 52, 52, 13, 14, 16, kSequencePointKind_Normal, 0, 5 } /* seqPointIndex: 5 */,
	{ 10274, 1, 53, 53, 17, 60, 17, kSequencePointKind_Normal, 0, 6 } /* seqPointIndex: 6 */,
	{ 10274, 1, 54, 54, 17, 64, 28, kSequencePointKind_Normal, 0, 7 } /* seqPointIndex: 7 */,
	{ 10274, 1, 55, 55, 13, 14, 39, kSequencePointKind_Normal, 0, 8 } /* seqPointIndex: 8 */,
	{ 10274, 1, 57, 57, 13, 30, 40, kSequencePointKind_Normal, 0, 9 } /* seqPointIndex: 9 */,
	{ 10274, 1, 58, 58, 9, 10, 48, kSequencePointKind_Normal, 0, 10 } /* seqPointIndex: 10 */,
	{ 10274, 1, 51, 51, 13, 35, 7, kSequencePointKind_StepOut, 0, 11 } /* seqPointIndex: 11 */,
	{ 10274, 1, 53, 53, 17, 60, 22, kSequencePointKind_StepOut, 0, 12 } /* seqPointIndex: 12 */,
	{ 10274, 1, 54, 54, 17, 64, 29, kSequencePointKind_StepOut, 0, 13 } /* seqPointIndex: 13 */,
	{ 10275, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 14 } /* seqPointIndex: 14 */,
	{ 10275, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 15 } /* seqPointIndex: 15 */,
	{ 10275, 1, 62, 62, 5, 6, 0, kSequencePointKind_Normal, 0, 16 } /* seqPointIndex: 16 */,
	{ 10275, 1, 63, 63, 9, 31, 1, kSequencePointKind_Normal, 0, 17 } /* seqPointIndex: 17 */,
	{ 10275, 1, 63, 63, 0, 0, 13, kSequencePointKind_Normal, 0, 18 } /* seqPointIndex: 18 */,
	{ 10275, 1, 64, 64, 9, 10, 16, kSequencePointKind_Normal, 0, 19 } /* seqPointIndex: 19 */,
	{ 10275, 1, 65, 65, 13, 33, 17, kSequencePointKind_Normal, 0, 20 } /* seqPointIndex: 20 */,
	{ 10275, 1, 66, 66, 13, 20, 29, kSequencePointKind_Normal, 0, 21 } /* seqPointIndex: 21 */,
	{ 10275, 1, 69, 69, 9, 39, 31, kSequencePointKind_Normal, 0, 22 } /* seqPointIndex: 22 */,
	{ 10275, 1, 71, 71, 9, 62, 43, kSequencePointKind_Normal, 0, 23 } /* seqPointIndex: 23 */,
	{ 10275, 1, 72, 72, 9, 28, 49, kSequencePointKind_Normal, 0, 24 } /* seqPointIndex: 24 */,
	{ 10275, 1, 74, 74, 9, 21, 56, kSequencePointKind_Normal, 0, 25 } /* seqPointIndex: 25 */,
	{ 10275, 1, 75, 75, 5, 6, 62, kSequencePointKind_Normal, 0, 26 } /* seqPointIndex: 26 */,
	{ 10275, 1, 63, 63, 9, 31, 7, kSequencePointKind_StepOut, 0, 27 } /* seqPointIndex: 27 */,
	{ 10275, 1, 65, 65, 13, 33, 18, kSequencePointKind_StepOut, 0, 28 } /* seqPointIndex: 28 */,
	{ 10275, 1, 65, 65, 13, 33, 23, kSequencePointKind_StepOut, 0, 29 } /* seqPointIndex: 29 */,
	{ 10275, 1, 69, 69, 9, 39, 32, kSequencePointKind_StepOut, 0, 30 } /* seqPointIndex: 30 */,
	{ 10275, 1, 69, 69, 9, 39, 37, kSequencePointKind_StepOut, 0, 31 } /* seqPointIndex: 31 */,
	{ 10275, 1, 71, 71, 9, 62, 43, kSequencePointKind_StepOut, 0, 32 } /* seqPointIndex: 32 */,
	{ 10275, 1, 72, 72, 9, 28, 50, kSequencePointKind_StepOut, 0, 33 } /* seqPointIndex: 33 */,
	{ 10275, 1, 74, 74, 9, 21, 56, kSequencePointKind_StepOut, 0, 34 } /* seqPointIndex: 34 */,
	{ 10276, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 35 } /* seqPointIndex: 35 */,
	{ 10276, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 36 } /* seqPointIndex: 36 */,
	{ 10276, 1, 102, 102, 5, 6, 0, kSequencePointKind_Normal, 0, 37 } /* seqPointIndex: 37 */,
	{ 10276, 1, 103, 103, 9, 44, 1, kSequencePointKind_Normal, 0, 38 } /* seqPointIndex: 38 */,
	{ 10276, 1, 104, 104, 5, 6, 9, kSequencePointKind_Normal, 0, 39 } /* seqPointIndex: 39 */,
	{ 10276, 1, 103, 103, 9, 44, 3, kSequencePointKind_StepOut, 0, 40 } /* seqPointIndex: 40 */,
	{ 10277, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 41 } /* seqPointIndex: 41 */,
	{ 10277, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 42 } /* seqPointIndex: 42 */,
	{ 10277, 1, 120, 120, 5, 6, 0, kSequencePointKind_Normal, 0, 43 } /* seqPointIndex: 43 */,
	{ 10277, 1, 121, 121, 9, 45, 1, kSequencePointKind_Normal, 0, 44 } /* seqPointIndex: 44 */,
	{ 10277, 1, 122, 122, 9, 58, 18, kSequencePointKind_Normal, 0, 45 } /* seqPointIndex: 45 */,
	{ 10277, 1, 123, 123, 9, 87, 25, kSequencePointKind_Normal, 0, 46 } /* seqPointIndex: 46 */,
	{ 10277, 1, 124, 124, 9, 54, 52, kSequencePointKind_Normal, 0, 47 } /* seqPointIndex: 47 */,
	{ 10277, 1, 125, 125, 5, 6, 66, kSequencePointKind_Normal, 0, 48 } /* seqPointIndex: 48 */,
	{ 10277, 1, 121, 121, 9, 45, 7, kSequencePointKind_StepOut, 0, 49 } /* seqPointIndex: 49 */,
	{ 10277, 1, 121, 121, 9, 45, 12, kSequencePointKind_StepOut, 0, 50 } /* seqPointIndex: 50 */,
	{ 10277, 1, 122, 122, 9, 58, 19, kSequencePointKind_StepOut, 0, 51 } /* seqPointIndex: 51 */,
	{ 10277, 1, 123, 123, 9, 87, 30, kSequencePointKind_StepOut, 0, 52 } /* seqPointIndex: 52 */,
	{ 10277, 1, 123, 123, 9, 87, 41, kSequencePointKind_StepOut, 0, 53 } /* seqPointIndex: 53 */,
	{ 10277, 1, 124, 124, 9, 54, 60, kSequencePointKind_StepOut, 0, 54 } /* seqPointIndex: 54 */,
	{ 10279, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 55 } /* seqPointIndex: 55 */,
	{ 10279, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 56 } /* seqPointIndex: 56 */,
	{ 10279, 1, 12, 12, 9, 55, 0, kSequencePointKind_Normal, 0, 57 } /* seqPointIndex: 57 */,
	{ 10279, 1, 13, 13, 9, 60, 11, kSequencePointKind_Normal, 0, 58 } /* seqPointIndex: 58 */,
	{ 10279, 1, 12, 12, 9, 55, 1, kSequencePointKind_StepOut, 0, 59 } /* seqPointIndex: 59 */,
	{ 10279, 1, 13, 13, 9, 60, 12, kSequencePointKind_StepOut, 0, 60 } /* seqPointIndex: 60 */,
	{ 10279, 1, 13, 13, 9, 60, 23, kSequencePointKind_StepOut, 0, 61 } /* seqPointIndex: 61 */,
	{ 10280, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 62 } /* seqPointIndex: 62 */,
	{ 10280, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 63 } /* seqPointIndex: 63 */,
	{ 10280, 1, 112, 112, 9, 10, 0, kSequencePointKind_Normal, 0, 64 } /* seqPointIndex: 64 */,
	{ 10280, 1, 113, 113, 13, 72, 1, kSequencePointKind_Normal, 0, 65 } /* seqPointIndex: 65 */,
	{ 10280, 1, 114, 114, 9, 10, 10, kSequencePointKind_Normal, 0, 66 } /* seqPointIndex: 66 */,
	{ 10280, 1, 113, 113, 13, 72, 2, kSequencePointKind_StepOut, 0, 67 } /* seqPointIndex: 67 */,
	{ 10281, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 68 } /* seqPointIndex: 68 */,
	{ 10281, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 69 } /* seqPointIndex: 69 */,
	{ 10281, 1, 108, 108, 9, 36, 0, kSequencePointKind_Normal, 0, 70 } /* seqPointIndex: 70 */,
	{ 10281, 1, 109, 109, 9, 34, 11, kSequencePointKind_Normal, 0, 71 } /* seqPointIndex: 71 */,
	{ 10281, 1, 109, 109, 9, 34, 19, kSequencePointKind_StepOut, 0, 72 } /* seqPointIndex: 72 */,
	{ 10282, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 73 } /* seqPointIndex: 73 */,
	{ 10282, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 74 } /* seqPointIndex: 74 */,
	{ 10282, 2, 11, 11, 5, 6, 0, kSequencePointKind_Normal, 0, 75 } /* seqPointIndex: 75 */,
	{ 10282, 2, 13, 13, 5, 6, 1, kSequencePointKind_Normal, 0, 76 } /* seqPointIndex: 76 */,
	{ 10283, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 77 } /* seqPointIndex: 77 */,
	{ 10283, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 78 } /* seqPointIndex: 78 */,
	{ 10283, 2, 17, 17, 5, 6, 0, kSequencePointKind_Normal, 0, 79 } /* seqPointIndex: 79 */,
	{ 10283, 2, 19, 19, 5, 6, 1, kSequencePointKind_Normal, 0, 80 } /* seqPointIndex: 80 */,
	{ 10284, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 81 } /* seqPointIndex: 81 */,
	{ 10284, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 82 } /* seqPointIndex: 82 */,
	{ 10284, 2, 22, 22, 5, 6, 0, kSequencePointKind_Normal, 0, 83 } /* seqPointIndex: 83 */,
	{ 10284, 2, 23, 23, 9, 89, 1, kSequencePointKind_Normal, 0, 84 } /* seqPointIndex: 84 */,
	{ 10284, 2, 24, 24, 9, 45, 39, kSequencePointKind_Normal, 0, 85 } /* seqPointIndex: 85 */,
	{ 10284, 2, 23, 23, 9, 89, 1, kSequencePointKind_StepOut, 0, 86 } /* seqPointIndex: 86 */,
	{ 10284, 2, 23, 23, 9, 89, 19, kSequencePointKind_StepOut, 0, 87 } /* seqPointIndex: 87 */,
	{ 10284, 2, 23, 23, 9, 89, 24, kSequencePointKind_StepOut, 0, 88 } /* seqPointIndex: 88 */,
	{ 10284, 2, 24, 24, 9, 45, 39, kSequencePointKind_StepOut, 0, 89 } /* seqPointIndex: 89 */,
	{ 10285, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 90 } /* seqPointIndex: 90 */,
	{ 10285, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 91 } /* seqPointIndex: 91 */,
	{ 10285, 2, 28, 28, 5, 6, 0, kSequencePointKind_Normal, 0, 92 } /* seqPointIndex: 92 */,
	{ 10285, 2, 29, 29, 9, 89, 1, kSequencePointKind_Normal, 0, 93 } /* seqPointIndex: 93 */,
	{ 10285, 2, 30, 30, 9, 107, 39, kSequencePointKind_Normal, 0, 94 } /* seqPointIndex: 94 */,
	{ 10285, 2, 31, 31, 9, 66, 50, kSequencePointKind_Normal, 0, 95 } /* seqPointIndex: 95 */,
	{ 10285, 2, 32, 32, 9, 60, 61, kSequencePointKind_Normal, 0, 96 } /* seqPointIndex: 96 */,
	{ 10285, 2, 33, 33, 5, 6, 78, kSequencePointKind_Normal, 0, 97 } /* seqPointIndex: 97 */,
	{ 10285, 2, 29, 29, 9, 89, 1, kSequencePointKind_StepOut, 0, 98 } /* seqPointIndex: 98 */,
	{ 10285, 2, 29, 29, 9, 89, 19, kSequencePointKind_StepOut, 0, 99 } /* seqPointIndex: 99 */,
	{ 10285, 2, 29, 29, 9, 89, 24, kSequencePointKind_StepOut, 0, 100 } /* seqPointIndex: 100 */,
	{ 10285, 2, 30, 30, 9, 107, 44, kSequencePointKind_StepOut, 0, 101 } /* seqPointIndex: 101 */,
	{ 10285, 2, 31, 31, 9, 66, 55, kSequencePointKind_StepOut, 0, 102 } /* seqPointIndex: 102 */,
	{ 10285, 2, 32, 32, 9, 60, 61, kSequencePointKind_StepOut, 0, 103 } /* seqPointIndex: 103 */,
	{ 10285, 2, 32, 32, 9, 60, 72, kSequencePointKind_StepOut, 0, 104 } /* seqPointIndex: 104 */,
	{ 10286, 0, 0, 0, 0, 0, -1, kSequencePointKind_Normal, 0, 105 } /* seqPointIndex: 105 */,
	{ 10286, 0, 0, 0, 0, 0, 16777215, kSequencePointKind_Normal, 0, 106 } /* seqPointIndex: 106 */,
	{ 10286, 2, 36, 36, 5, 6, 0, kSequencePointKind_Normal, 0, 107 } /* seqPointIndex: 107 */,
	{ 10286, 2, 37, 37, 9, 26, 1, kSequencePointKind_Normal, 0, 108 } /* seqPointIndex: 108 */,
	{ 10286, 2, 38, 38, 9, 75, 7, kSequencePointKind_Normal, 0, 109 } /* seqPointIndex: 109 */,
	{ 10286, 2, 38, 38, 0, 0, 25, kSequencePointKind_Normal, 0, 110 } /* seqPointIndex: 110 */,
	{ 10286, 2, 39, 39, 9, 10, 28, kSequencePointKind_Normal, 0, 111 } /* seqPointIndex: 111 */,
	{ 10286, 2, 40, 40, 13, 99, 29, kSequencePointKind_Normal, 0, 112 } /* seqPointIndex: 112 */,
	{ 10286, 2, 41, 41, 9, 10, 52, kSequencePointKind_Normal, 0, 113 } /* seqPointIndex: 113 */,
	{ 10286, 2, 43, 43, 13, 78, 55, kSequencePointKind_Normal, 0, 114 } /* seqPointIndex: 114 */,
	{ 10286, 2, 45, 45, 9, 25, 78, kSequencePointKind_Normal, 0, 115 } /* seqPointIndex: 115 */,
	{ 10286, 2, 46, 46, 5, 6, 85, kSequencePointKind_Normal, 0, 116 } /* seqPointIndex: 116 */,
	{ 10286, 2, 38, 38, 9, 75, 16, kSequencePointKind_StepOut, 0, 117 } /* seqPointIndex: 117 */,
	{ 10286, 2, 40, 40, 13, 99, 46, kSequencePointKind_StepOut, 0, 118 } /* seqPointIndex: 118 */,
	{ 10286, 2, 43, 43, 13, 78, 72, kSequencePointKind_StepOut, 0, 119 } /* seqPointIndex: 119 */,
	{ 10286, 2, 45, 45, 9, 25, 79, kSequencePointKind_StepOut, 0, 120 } /* seqPointIndex: 120 */,
};
#else
extern Il2CppSequencePoint g_sequencePointsAssemblyU2DCSharp[];
Il2CppSequencePoint g_sequencePointsAssemblyU2DCSharp[1] = { { 0, 0, 0, 0, 0, 0, 0, kSequencePointKind_Normal, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#else
static const Il2CppCatchPoint g_catchPoints[1] = { { 0, 0, 0, 0, } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[] = {
{ "", { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} }, //0 
{ "/Users/baptiste/Public/git/unity-test-reco/Assets/Script/PhotoBridgeIos.cs", { 55, 5, 55, 29, 158, 173, 149, 38, 53, 106, 199, 69, 154, 254, 80, 187} }, //1 
{ "/Users/baptiste/Public/git/unity-test-reco/Assets/Script/Tests/TestServicePhotoIos.cs", { 109, 182, 145, 194, 228, 22, 179, 193, 102, 72, 201, 53, 225, 80, 69, 29} }, //2 
};
#else
static const Il2CppSequencePointSourceFile g_sequencePointSourceFiles[1] = { NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppTypeSourceFilePair g_typeSourceFiles[4] = 
{
	{ 1889, 1 },
	{ 1886, 1 },
	{ 1888, 1 },
	{ 1890, 2 },
};
#else
static const Il2CppTypeSourceFilePair g_typeSourceFiles[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodScope g_methodScopes[6] = 
{
	{ 0, 50 },
	{ 16, 40 },
	{ 0, 63 },
	{ 0, 67 },
	{ 0, 12 },
	{ 0, 86 },
};
#else
static const Il2CppMethodScope g_methodScopes[1] = { { 0, 0 } };
#endif
#if IL2CPP_MONO_DEBUGGER
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[16] = 
{
	{ 0, 0, 0 } /* System.Void PhotoBridgeIos::LoadPhoto() */,
	{ 0, 0, 0 } /* System.Void PhotoBridgeIos::ReceiveMsgFromUnity(System.Int32,System.String) */,
	{ 50, 0, 2 } /* PhotoBridgeIos PhotoBridgeIos::get_Instance() */,
	{ 63, 2, 1 } /* System.Void PhotoBridgeIos::Start() */,
	{ 0, 0, 0 } /* System.Void PhotoBridgeIos::SendMessage(System.Int32,System.String) */,
	{ 67, 3, 1 } /* System.Void PhotoBridgeIos::ReceiveMsg(System.String) */,
	{ 0, 0, 0 } /* System.Void PhotoBridgeIos::.ctor() */,
	{ 0, 0, 0 } /* System.Void PhotoBridgeIos/PhotoBridgeTranslation::.ctor() */,
	{ 12, 4, 1 } /* PhotoBridgeIos/ReceiveMsgFromIos PhotoBridgeIos/ReceiveMsgFromIos::CreateFromJSON(System.String) */,
	{ 0, 0, 0 } /* System.Void PhotoBridgeIos/ReceiveMsgFromIos::.ctor() */,
	{ 0, 0, 0 } /* System.Void TestServicePhotoIos::Start() */,
	{ 0, 0, 0 } /* System.Void TestServicePhotoIos::Update() */,
	{ 0, 0, 0 } /* System.Void TestServicePhotoIos::OnDestroy() */,
	{ 0, 0, 0 } /* System.Void TestServicePhotoIos::testCommIos() */,
	{ 86, 5, 1 } /* System.Void TestServicePhotoIos::ReceiveMessageCallbackHandler(PhotoBridgeIos/ApbMsgType,System.String) */,
	{ 0, 0, 0 } /* System.Void TestServicePhotoIos::.ctor() */,
};
#else
static const Il2CppMethodHeaderInfo g_methodHeaderInfos[1] = { { 0, 0, 0 } };
#endif
IL2CPP_EXTERN_C const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationAssemblyU2DCSharp;
const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationAssemblyU2DCSharp = 
{
	(Il2CppMethodExecutionContextInfo*)g_methodExecutionContextInfos,
	(Il2CppMethodExecutionContextInfoIndex*)g_methodExecutionContextInfoIndexes,
	(Il2CppMethodScope*)g_methodScopes,
	(Il2CppMethodHeaderInfo*)g_methodHeaderInfos,
	(Il2CppSequencePointSourceFile*)g_sequencePointSourceFiles,
	121,
	(Il2CppSequencePoint*)g_sequencePointsAssemblyU2DCSharp,
	0,
	(Il2CppCatchPoint*)g_catchPoints,
	4,
	(Il2CppTypeSourceFilePair*)g_typeSourceFiles,
	(const char**)g_methodExecutionContextInfoStrings,
};
