﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void PhotoBridgeIos::LoadPhoto()
extern void PhotoBridgeIos_LoadPhoto_m8691CA2F388D893999D268E11A6F27C165575009 (void);
// 0x00000002 System.Void PhotoBridgeIos::ReceiveMsgFromUnity(System.Int32,System.String)
extern void PhotoBridgeIos_ReceiveMsgFromUnity_m6A259A50A870D26EA9752CFCE20A35148475D391 (void);
// 0x00000003 PhotoBridgeIos PhotoBridgeIos::get_Instance()
extern void PhotoBridgeIos_get_Instance_m7D2B56959BEA18F889D064DA02216DF227919772 (void);
// 0x00000004 System.Void PhotoBridgeIos::Start()
extern void PhotoBridgeIos_Start_m8EACE3FC6AB843445FA5D244DBCE5717A5873F41 (void);
// 0x00000005 System.Void PhotoBridgeIos::SendMessage(System.Int32,System.String)
extern void PhotoBridgeIos_SendMessage_mE5E33A7D049C3D27DD8BCCD7C78991B62B2FE692 (void);
// 0x00000006 System.Void PhotoBridgeIos::ReceiveMsg(System.String)
extern void PhotoBridgeIos_ReceiveMsg_m9E8507A7BB42342178FFB0A90E09901004FF9DE0 (void);
// 0x00000007 System.Void PhotoBridgeIos::.ctor()
extern void PhotoBridgeIos__ctor_mC75555FC8CD06BE50E3235364216DE2D0BA11D70 (void);
// 0x00000008 System.Void PhotoBridgeIos/PhotoBridgeTranslation::.ctor()
extern void PhotoBridgeTranslation__ctor_mF173B61129A6FDD99204BCAB13D591653B64B551 (void);
// 0x00000009 PhotoBridgeIos/ReceiveMsgFromIos PhotoBridgeIos/ReceiveMsgFromIos::CreateFromJSON(System.String)
extern void ReceiveMsgFromIos_CreateFromJSON_m3123BA18071BA2795C9FCB54A7FAC2ADFCFBF9E4 (void);
// 0x0000000A System.Void PhotoBridgeIos/ReceiveMsgFromIos::.ctor()
extern void ReceiveMsgFromIos__ctor_m5964BBD21DC2FD11EE350EDCD06D4716BD535711 (void);
// 0x0000000B System.Void TestServicePhotoIos::Start()
extern void TestServicePhotoIos_Start_m23FAFD7BD89068FCA2EFB19FAF02EDE9F45E36D0 (void);
// 0x0000000C System.Void TestServicePhotoIos::Update()
extern void TestServicePhotoIos_Update_m2F4B30ABEDCC05401E67B43FC3A7A069A840C921 (void);
// 0x0000000D System.Void TestServicePhotoIos::OnDestroy()
extern void TestServicePhotoIos_OnDestroy_m1F8A10C5E262F34146B846224A2789EB0931540F (void);
// 0x0000000E System.Void TestServicePhotoIos::testCommIos()
extern void TestServicePhotoIos_testCommIos_mAC66DFC03B9589AA5F9CD75AF256C55045E4C0C2 (void);
// 0x0000000F System.Void TestServicePhotoIos::ReceiveMessageCallbackHandler(PhotoBridgeIos/ApbMsgType,System.String)
extern void TestServicePhotoIos_ReceiveMessageCallbackHandler_m074A68946219DDBDB012660D37975D1CB3839438 (void);
// 0x00000010 System.Void TestServicePhotoIos::.ctor()
extern void TestServicePhotoIos__ctor_m2D50548D41B6A25F6B91F5FA53674919B3E479A8 (void);
static Il2CppMethodPointer s_methodPointers[16] = 
{
	PhotoBridgeIos_LoadPhoto_m8691CA2F388D893999D268E11A6F27C165575009,
	PhotoBridgeIos_ReceiveMsgFromUnity_m6A259A50A870D26EA9752CFCE20A35148475D391,
	PhotoBridgeIos_get_Instance_m7D2B56959BEA18F889D064DA02216DF227919772,
	PhotoBridgeIos_Start_m8EACE3FC6AB843445FA5D244DBCE5717A5873F41,
	PhotoBridgeIos_SendMessage_mE5E33A7D049C3D27DD8BCCD7C78991B62B2FE692,
	PhotoBridgeIos_ReceiveMsg_m9E8507A7BB42342178FFB0A90E09901004FF9DE0,
	PhotoBridgeIos__ctor_mC75555FC8CD06BE50E3235364216DE2D0BA11D70,
	PhotoBridgeTranslation__ctor_mF173B61129A6FDD99204BCAB13D591653B64B551,
	ReceiveMsgFromIos_CreateFromJSON_m3123BA18071BA2795C9FCB54A7FAC2ADFCFBF9E4,
	ReceiveMsgFromIos__ctor_m5964BBD21DC2FD11EE350EDCD06D4716BD535711,
	TestServicePhotoIos_Start_m23FAFD7BD89068FCA2EFB19FAF02EDE9F45E36D0,
	TestServicePhotoIos_Update_m2F4B30ABEDCC05401E67B43FC3A7A069A840C921,
	TestServicePhotoIos_OnDestroy_m1F8A10C5E262F34146B846224A2789EB0931540F,
	TestServicePhotoIos_testCommIos_mAC66DFC03B9589AA5F9CD75AF256C55045E4C0C2,
	TestServicePhotoIos_ReceiveMessageCallbackHandler_m074A68946219DDBDB012660D37975D1CB3839438,
	TestServicePhotoIos__ctor_m2D50548D41B6A25F6B91F5FA53674919B3E479A8,
};
static const int32_t s_InvokerIndices[16] = 
{
	1434,
	1285,
	1427,
	833,
	451,
	722,
	833,
	833,
	1374,
	833,
	833,
	833,
	833,
	833,
	451,
	833,
};
extern const Il2CppDebuggerMetadataRegistration g_DebuggerMetadataRegistrationAssemblyU2DCSharp;
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	16,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	&g_DebuggerMetadataRegistrationAssemblyU2DCSharp,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
