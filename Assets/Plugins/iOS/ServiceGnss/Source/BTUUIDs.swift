import CoreBluetooth

struct BTUUIDs {
    static let GnssReadCharacteristic = CBUUID(string: "0000abf2-0000-1000-8000-00805f9b34fb")
    static let GnssWriteCharacteristic = CBUUID(string: "0000abf1-0000-1000-8000-00805f9b34fb")
    static let GnssService = CBUUID(string: "0000abf0-0000-1000-8000-00805f9b34fb")
}
