//
//  UbxTools.swift
//  ServiceGNSS
//
//  Created by ivirtual on 21/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation
import Foundation
// =====================================================================
/// Class which encapsulates a Swift byte array (an Array object with elements of type UInt8) and an
/// index into the array.
open class ByteArrayAndIndex {

   private var _byteArray : [UInt8]
   private var _arrayIndex = 0

   public init(_ byteArray : [UInt8]) {
      _byteArray = byteArray;
   }

   /// Property to provide read-only access to the current array index value.
   public var arrayIndex : Int {
      get { return _arrayIndex }
   }

   /// Property to calculate how many bytes are left in the byte array, i.e., from the index point
   /// to the end of the byte array.
   public var bytesLeft : Int {
      get { return _byteArray.count - _arrayIndex }
   }

   /// Method to get a single byte from the byte array.
   public func getUInt8() -> UInt8 {
      let returnValue = _byteArray[_arrayIndex]
      _arrayIndex += 1
      return returnValue
   }

   /// Method to get an Int16 from two bytes in the byte array (little-endian).
   public func getInt16() -> Int16 {
      return Int16(bitPattern: getUInt16())
   }

   /// Method to get a UInt16 from two bytes in the byte array (little-endian).
   public func getUInt16() -> UInt16 {
      let returnValue = UInt16(_byteArray[_arrayIndex]) |
                        UInt16(_byteArray[_arrayIndex + 1]) << 8
      _arrayIndex += 2
      return returnValue
   }

   /// Method to get a UInt from three bytes in the byte array (little-endian).
   public func getUInt24() -> UInt {
      let returnValue = UInt(_byteArray[_arrayIndex]) |
                        UInt(_byteArray[_arrayIndex + 1]) << 8 |
                        UInt(_byteArray[_arrayIndex + 2]) << 16
      _arrayIndex += 3
      return returnValue
   }

   /// Method to get an Int32 from four bytes in the byte array (little-endian).
   public func getInt32() -> Int32 {
      return Int32(bitPattern: getUInt32())
   }

   /// Method to get a UInt32 from four bytes in the byte array (little-endian).
   public func getUInt32() -> UInt32 {
      let returnValue = UInt32(_byteArray[_arrayIndex]) |
                        UInt32(_byteArray[_arrayIndex + 1]) << 8 |
                        UInt32(_byteArray[_arrayIndex + 2]) << 16 |
                        UInt32(_byteArray[_arrayIndex + 3]) << 24
      _arrayIndex += 4
      return returnValue
   }

   /// Method to get an Int64 from eight bytes in the byte array (little-endian).
 /*  public func getInt64() -> Int64 {
      return Int64(bitPattern: getUInt64())
   }

   /// Method to get a UInt64 from eight bytes in the byte array (little-endian).
   public func getUInt64() -> UInt64 {
      let returnValue = UInt64(_byteArray[_arrayIndex]) |
                        UInt64(_byteArray[_arrayIndex + 1]) << 8 |
                        UInt64(_byteArray[_arrayIndex + 2]) << 16 |
                        UInt64(_byteArray[_arrayIndex + 3]) << 24 |
                        UInt64(_byteArray[_arrayIndex + 4]) << 32 |
                        UInt64(_byteArray[_arrayIndex + 5]) << 40 |
                        UInt64(_byteArray[_arrayIndex + 6]) << 48 |
                        UInt64(_byteArray[_arrayIndex + 7]) << 56
      _arrayIndex += 8
      return returnValue
   }*/
}
// =====================================================================
extension Data {
    
    var uint8: UInt8 {
        get {
            var number: UInt8 = 0
            self.copyBytes(to:&number, count: MemoryLayout<UInt8>.size)
            return number
        }
    }
    
    var uint16: UInt16 {
        get {
            let i16array = self.withUnsafeBytes { $0.load(as: UInt16.self) }
            return i16array
        }
    }
    
    var uint32: UInt32 {
        get {
            let i32array = self.withUnsafeBytes { $0.load(as: UInt32.self) }
            return i32array
        }
    }
    
    var uuid: NSUUID? {
        get {
            var bytes = [UInt8](repeating: 0, count: self.count)
            self.copyBytes(to:&bytes, count: self.count * MemoryLayout<UInt32>.size)
            return NSUUID(uuidBytes: bytes)
        }
    }
    var stringASCII: String? {
        get {
            return NSString(data: self, encoding: String.Encoding.ascii.rawValue) as String?
        }
    }
    
    var stringUTF8: String? {
        get {
            return NSString(data: self, encoding: String.Encoding.utf8.rawValue) as String?
        }
    }

    
}
// =====================================================================
extension Int {
    var data: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<Int>.size)
    }
}
// =====================================================================
extension UInt8 {
    var data: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<UInt8>.size)
    }
}
// =====================================================================
extension UInt16 {
    var data: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<UInt16>.size)
    }
    var byteArrayLittleEndian: [UInt8] {
        return [
            
            UInt8(self & 0x00FF),
            UInt8((self & 0xFF00) >> 8)
        ]
    }
}
// =====================================================================
extension UInt32 {
    var data: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<UInt32>.size)
    }
    
    var byteArrayLittleEndian: [UInt8] {
        return [
            UInt8(self & 0x000000FF),
            UInt8((self & 0x0000FF00) >> 8),
            UInt8((self & 0x00FF0000) >> 16),
            UInt8((self & 0xFF000000) >> 24)
            
            
            
        ]
    }
}
// =====================================================================
extension StringProtocol {
    var hexaData: Data { .init(hexa) }
    var hexaBytes: [UInt8] { .init(hexa) }
    private var hexa: UnfoldSequence<UInt8, Index> {
        sequence(state: startIndex) { startIndex in
            guard startIndex < self.endIndex else { return nil }
            let endIndex = self.index(startIndex, offsetBy: 2, limitedBy: self.endIndex) ?? self.endIndex
            defer { startIndex = endIndex }
            return UInt8(self[startIndex..<endIndex], radix: 16)
        }
    }
}
// =====================================================================
extension Array where Element == UInt8 {
  func bytesToHex(spacing: String) -> String {
    var hexString: String = ""
    var count = self.count
    for byte in self
    {
        hexString.append(String(format:"%02X", byte))
        count = count - 1
        if count > 0
        {
            hexString.append(spacing)
        }
    }
    return hexString
  }
}
// =====================================================================
public class UbxTools {

    /*public static ByteBuffer intToByteBuffer(int x, ByteOrder order){
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(order);
        byteBuffer.putInt(x);
        return  byteBuffer;
    }*/
    public static func intToByteBuffer(x:UInt32,LittleEndian:Bool)->[UInt8]{
        //var conv:ByteArrayAndIndex  = ByteArrayAndIndex()
        if LittleEndian{
            return x.byteArrayLittleEndian
        }else{
            return x.byteArrayLittleEndian.reversed()
        }
        /*
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(order);
        byteBuffer.putInt(x);
        return  byteBuffer;*/
    }
    
    public static func shortToByteBuffer(x:UInt16,LittleEndian:Bool)->[UInt8]{
        if LittleEndian{
            return x.byteArrayLittleEndian
        }else{
            return x.byteArrayLittleEndian.reversed()
        }
        /*ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(order);
        byteBuffer.putShort(x);
        return  byteBuffer;*/
    }

    public static func stringsArrayToBytesArray( array:[String])->[UInt8] {
        var tmp:String=""
        for str in array{
            tmp = tmp + str
        }
        return stringToBytesArray(bufferStr: tmp)
    }
    public static func stringsArrayToBytesArray( array:[Substring])->[UInt8] {
        var tmp:String=""
        for str in array{
            tmp = tmp + str
        }
        return stringToBytesArray(bufferStr: tmp)
    }
    public static func stringToBytesArray( bufferStr:String)->[UInt8] {
        return bufferStr.hexaBytes
    }
    
    public static func bytesToInt(b1:UInt8, b2:UInt8, b3:UInt8, b4:UInt8)->Int32 {
        var array_byte : ByteArrayAndIndex = ByteArrayAndIndex([b4,b3,b2,b1])
        return array_byte.getInt32()
    }

    public static func bytesToShort(b1:UInt8, b2:UInt8)->Int16 {
        var array_byte : ByteArrayAndIndex = ByteArrayAndIndex([b2,b1])
        return array_byte.getInt16();
    }
    
    // TESTS ----------------------------------------------
    public static func test() {

            // Example and unit test
            //UnitTest();
        var  key:UBXKey =  UBXKey();

            // Set default parameters for ZED-F9P (RAM+BBR+FLASH) with UBXValset
        var valset:UBXValSet = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.DynModel(value: UBXMessage.DYNMODEL.PORTABLE);
        valset.Rate_Meas(value: 1000);//1000ms
        valset.Rate_Nav(value: 1); //1 solutions
        valset.MinElev(value: 15);
        valset.TimeOut(value: 30);
        valset.NMEAHighPrecision(value: true);
        valset.OutMessage(keys_value: true,keys: [ key.MSGOUT_GST_UART1, key.MSGOUT_GGA_UART1, key.MSGOUT_GSA_UART1]);
        valset.OutMessage(keys_value: false,keys: [ key.MSGOUT_DTM_UART1, key.MSGOUT_GBS_UART1, key.MSGOUT_GLL_UART1, key.MSGOUT_GNS_UART1, key.MSGOUT_GRS_UART1, key.MSGOUT_GSV_UART1, key.MSGOUT_RMC_UART1, key.MSGOUT_VLW_UART1, key.MSGOUT_VTG_UART1, key.MSGOUT_ZDA_UART1]);
        valset.Uart1Bauderate(val: 115200);
        valset.Uart1InProt(keys_value: true, keys: [key.UART1_INPROT_RTCM3, key.UART1_INPROT_UBX, key.UART1_INPROT_NMEA]);
        valset.Uart1OutProt(keys_value: true,keys: [ key.UART1_OUTPROT_NMEA, key.UART1_INPROT_UBX]);
        valset.Uart1OutProt(keys_value: false,keys: [ key.UART1_OUTPROT_RTCM3]);
        print(valset.getHex());


        // Set dynamic parameters for pedestrian use case (RAM)
        valset = UBXValSet(value: UBXMessage.STORE.RAM);
        valset.DynModel(value: UBXMessage.DYNMODEL.PEDESTRIAN);
        valset.Rate_Nav(value: 500);
        valset.Rate_Nav(value: 1);


            // Get parameters for ZED-F9P (RAM) with UBXValget
        var valget:UBXValGet = UBXValGet(value: UBXMessage.STORE.RAM);
            valget.DynModel();
            valget.Rate_Meas();
            valget.Rate_Nav();
            valget.MinElev();
            valget.TimeOut();
            valget.NMEAHighPrecision();
        valget.OutMessage(keys:[key.MSGOUT_GST_UART1, key.MSGOUT_GGA_UART1, key.MSGOUT_GSA_UART1]);
        valget.OutMessage(keys: [key.MSGOUT_DTM_UART1, key.MSGOUT_GBS_UART1, key.MSGOUT_GLL_UART1, key.MSGOUT_GNS_UART1, key.MSGOUT_GRS_UART1, key.MSGOUT_GSV_UART1, key.MSGOUT_RMC_UART1, key.MSGOUT_VLW_UART1, key.MSGOUT_VTG_UART1, key.MSGOUT_ZDA_UART1]);
            valget.Uart1Bauderate();
        valget.Uart1InProt(keys: [key.UART1_INPROT_NMEA, key.UART1_INPROT_RTCM3, key.UART1_INPROT_UBX]);
        valget.Uart1OutProt(keys: [key.UART1_OUTPROT_NMEA, key.UART1_OUTPROT_UBX,key.UART1_OUTPROT_RTCM3]);


            // Get mon version
        var  msg :[UInt8] =  UBXMessage.getMonVersion();
        }

    public static func UnitTest() {
        var valset:UBXValSet = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.Rate_Meas(value: 100);
        check(tag: "MEAS_RATE", champ1: valset.getHex(), champ2: "B5 62 06 8A 0A 00 00 07 00 00 01 00 21 30 64 00 57 EF");

        valset = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.Rate_Nav(value: 1);
        check(tag: "NAV_RATE", champ1: valset.getHex(), champ2: "B5 62 06 8A 0A 00 00 07 00 00 02 00 21 30 01 00 F5 2F");

        valset = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.DynModel(value: UBXMessage.DYNMODEL.PORTABLE);
        check(tag: "DYN_MODEL", champ1: valset.getHex(), champ2: "B5 62 06 8A 09 00 00 07 00 00 21 00 11 20 00 F2 79");

        valset = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.MinElev(value: 15);
        check(tag: "MIN_ELEV", champ1: valset.getHex(), champ2: "B5 62 06 8A 09 00 00 07 00 00 A4 00 11 20 0F 84 17");

        valset = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.TimeOut(value: 60);
        check(tag: "TIMEOUT", champ1: valset.getHex(), champ2: "B5 62 06 8A 09 00 00 07 00 00 C4 00 11 20 3C D1 E4");

        valset = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.NMEAHighPrecision(value: true);
        check(tag: "NMEA HIGH PRECISION", champ1: valset.getHex(), champ2: "B5 62 06 8A 09 00 00 07 00 00 06 00 93 10 01 4A 59");

        var key:UBXKey = UBXKey();
        valset = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.OutMessage(keys_value: true, keys: [key.MSGOUT_GST_UART1]);
        check(tag: "GST UART1_2", champ1: valset.getHex(), champ2: "B5 62 06 8A 09 00 00 07 00 00 D4 00 91 20 01 26 79");
            //B5 62 06 8B 09 00 01 00 00 00 06 00 93 10 01 45 36

        valset =  UBXValSet(value: UBXMessage.STORE.ALL);
        valset.Rate_Meas(value: 100);
        valset.Rate_Nav(value: 1);
        valset.NMEAHighPrecision(value: true);
        check(tag: "RATE_MEAS_NAV", champ1: valset.getHex(), champ2: "B5 62 06 8A 15 00 00 07 00 00 01 00 21 30 64 00 02 00 21 30 01 00 06 00 93 10 01 60 67");
            print("#####################################################");
        var params:[String:String] = UBXMessage.Parse(msg: "B5 62 06 8B 10 00 01 00 00 00 01 00 21 30 E8 03 02 00 21 30 01 00 33 76");
            print(params);
            print("#####################################################");
        params = UBXMessage.Parse(msg: "B5 62 0A 04 DC 00 45 58 54 20 43 4F 52 45 20 31 2E 30 30 20 28 36 31 62 32 64 64 29 00 00 00 00 00 00 00 00 30 30 31 39 30 30 30 30 00 00 52 4F 4D 20 42 41 53 45 20 30 78 31 31 38 42 32 30 36 30 00 00 00 00 00 00 00 00 00 00 00 46 57 56 45 52 3D 48 50 47 20 31 2E 31 32 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 50 52 4F 54 56 45 52 3D 32 37 2E 31 31 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 4D 4F 44 3D 5A 45 44 2D 46 39 50 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 47 50 53 3B 47 4C 4F 3B 47 41 4C 3B 42 44 53 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 51 5A 53 53 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 BD 36");
            print(params);
            print("#####################################################");
        params = UBXMessage.Parse(msg: "B5 62 05 01 02 00 06 8A 98 C1");
            print(params);
            print("#####################################################");
        params = UBXMessage.Parse(msg: "B5 62 06 8B 90 00 " +
                    "01 00 00 00 21 00 11 20 00 01 00 21 30 C8 00 02 00 21 30 01 00 A4 00 11 20 0F C4 00 11 20 1E 06 00 93 10 01 D4 00 91 20 01 BB 00 91 20 01 C0 00 91 20 01 A7 00 91 20 00 DE 00 91 20 00 CA 00 91 20 00 B6 00 91 20 00 CF 00 91 20 00 C5 00 91 20 00 AC 00 91 20 00 E8 00 91 20 00 B1 00 91 20 00 D9 00 91 20 00 01 00 52 40 00 96 00 00 02 00 73 10 00 04 00 73 10 01 01 00 73 10 01 02 00 74 10 01 02 00 74 10 01 01 00 74 10 01 04 00 74 10 00 60 7A");
            print(params);
            print("#####################################################");
        }

    public static func check( tag:String, champ1:String, champ2:String) {
        var champ1_ = champ1.uppercased();
        var champ2_ = champ2.uppercased();

            if (champ1_==(champ2_)) {
                print(tag+" OK");
            } else {
                print(tag+" : ERROR");
                print(champ1_);
                print(champ2_);
            }
        }

}
// =====================================================================
