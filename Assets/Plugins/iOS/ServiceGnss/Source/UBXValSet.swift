//
//  UBXValSet.swift
//  ServiceGNSS
//
//  Created by ivirtual on 22/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation
/**
 *
 */
public class UBXValSet {
    /**
     *
     */
    //private UBXVector payload = new UBXVector();
    //private UBXMessage msg = new UBXMessage(payload);
    var payload:UBXVector
    var msg:UBXMessage

    /**
     *
     * @param value
     */
    /*public UBXValSet(UBXMessage.STORE value) {
        msg.InitValset(value.get());
    }*/
    init(value:UBXMessage.STORE) {
        payload = UBXVector();
        msg = UBXMessage(msg: payload);
        msg.InitValset(value:value.get());
    }
    /**
     *
     * @param value
     */
    /*public void Rate_Meas(short value) {//Nominal time between GNSS measurements
        payload.add(UBXKey.getInstance().RATE_MEAS);
        payload.addU2(value);
    }*/
    func Rate_Meas(value:UInt16) {//Nominal time between GNSS measurements
        payload.add(value: UBXKey.Instance.RATE_MEAS);
        payload.addU2(i: value);
    }
    /**
     *
     * @param value
     */
    /*public void Rate_Nav(short value) {//Ratio of number of measurements to number of navigation solutions
        payload.add(UBXKey.getInstance().RATE_NAV);
        payload.addU2(value);
    }*/
    func Rate_Nav(value:UInt16) {//Nominal time between GNSS measurements
        payload.add(value: UBXKey.Instance.RATE_NAV);
        payload.addU2(i: value);
    }
    /**
     *
     * @param value
     */
    /*public void DynModel(UBXMessage.DYNMODEL value) {
        payload.add(UBXKey.getInstance().DYNMODEL);
        payload.addU1(value.getI());
    }*/
    func DynModel(value:UBXMessage.DYNMODEL ) {//Ratio of number of measurements to number of navigation solutions
        payload.add(value: UBXKey.Instance.DYNMODEL);
        payload.addU1(i: value.getI());
    }
    /**
     *
     * @param value
     */
    /*public void MinElev(byte value) {
        payload.add(UBXKey.getInstance().MINELEV);
        payload.addU1(value);
    }*/
    func MinElev(value:UInt8) {//Ratio of number of measurements to number of navigation solutions
        payload.add(value: UBXKey.Instance.MINELEV);
        payload.addU1(i: value);
    }
    /**
     *
     * @param value
     */
    func TimeOut(value:UInt8) {//Ratio of number of measurements to number of navigation solutions
        payload.add(value: UBXKey.Instance.TIMEOUT);
        payload.addU1(i: value);
    }
    /*public void TimeOut(byte value) {
        payload.add(UBXKey.getInstance().TIMEOUT);
        payload.addU1(value);
    }*/

    /**
     *
     * @param value
     */
        
    func NMEAHighPrecision(value:Bool) {//Ratio of number of measurements to number of navigation solutions
        let val:UInt8 = value ? 1 : 0;
        payload.add(value: UBXKey.Instance.NMEAHIGHTPRECISION);
        payload.addU1(i: val);
    }
        
    /*public void NMEAHighPrecision(boolean value) {
        byte val = (value) ?(byte) 1 :(byte) 0;
        payload.add(UBXKey.getInstance().NMEAHIGHTPRECISION);
        payload.addU1(val);
    }*/

    /**
     *
     * @param keys_value
     * @param keys
     */
    /*public void OutMessage(boolean keys_value, KeysId... keys) { //Ratio of number of measurements to number of navigation solutions
        byte val = (keys_value) ? (byte)1 :(byte) 0;
        for (KeysId key : keys) {
            payload.add(key);
            payload.addU1(val);
        }
    }*/
    func OutMessage(keys_value:Bool, keys:[KeysId]) { //Ratio of number of measurements to number of navigation solutions
        let val:UInt8 = keys_value ? 1 : 0;
        for key in keys {
            payload.add(value: key);
            payload.addU1(i: val);
        }
    }
    /**
     *
     * @param val
     */
    /*public void Uart2Enabled(boolean val) {
        byte value = (val) ? (byte)1 :(byte) 0;
        payload.add(UBXKey.getInstance().UART2_ENABLED);
        payload.addU1(value);
    }*/
    func Uart2Enabled(val:Bool) {
        let val:UInt8 = val ? 1 : 0;
        payload.add(value: UBXKey.Instance.UART2_ENABLED);
        payload.addU1(i: val);
    }
    /**
     *
     * @param val
     */
    /*public void Uart1Bauderate(int val) {
        payload.add(UBXKey.getInstance().UART1_BAUDERATE);
        payload.addU4(val);
    }*/
    func Uart1Bauderate(val:UInt32) {
        payload.add(value: UBXKey.Instance.UART1_BAUDERATE);
        payload.addU4(i: val);
    }
    /**
     *
     * @param keys_value
     * @param keys
     */
    /*public void Uart1InProt(boolean keys_value, KeysId... keys) { //Ratio of number of measurements to number of navigation solutions
        byte val = (keys_value) ? (byte)1 : (byte)0;
        for (KeysId key : keys) {
            payload.add(key);
            payload.addU1(val);
        }
    }*/
    func Uart1InProt(keys_value:Bool,keys:[KeysId]) { //Ratio of number of measurements to number of navigation solutions
        let val:UInt8 = keys_value ? 1 : 0;
        for key in keys {
            payload.add(value: key);
            payload.addU1(i: val);
        }
    }
    /**
     *
     * @param keys_value
     * @param keys
     */
    /*public void Uart1OutProt(boolean keys_value, KeysId... keys) { //Ratio of number of measurements to number of navigation solutions
        byte val = (keys_value) ?(byte) 1 : (byte)0;
        for (KeysId key : keys) {
            payload.add(key);
            payload.addU1(val);
        }
    }*/
    func Uart1OutProt(keys_value:Bool,keys:[KeysId]) { //Ratio of number of measurements to number of navigation solutions
        let val:UInt8 = keys_value ? 1 : 0;
        for key in keys {
            payload.add(value: key);
            payload.addU1(i: val);
        }
    }
    /**
     *
     * @return
     */
    /*public byte[] getByte() {
        return msg.getByte();
    }*/
    func getByte()->[UInt8] {
        return msg.getByte();
    }
    /**
     *
     * @return
     */
   /* public String getHex() {
        return msg.getHex();
    }*/
    func getHex()->String {
        return msg.getHex();
    }
}
