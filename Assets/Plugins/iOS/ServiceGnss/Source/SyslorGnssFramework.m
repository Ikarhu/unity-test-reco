//
//  IOSPluginTest.m
//  IOSPluginTest
//
//  Created by Tim Regan on 10/02/2017.
//  Copyright © 2017 Tim Regan. All rights reserved.
//

#import <SyslorGnssFramework/SyslorGnssFramework-Swift.h>

@implementation SyslorGnssFramework : NSObject

@end

char* convertNSStringToCString(const NSString* nsString)
{
    if (nsString == NULL)
        return NULL;

    const char* nsStringUtf8 = [nsString UTF8String];
    //create a null terminated C string on the heap so that our string's memory isn't wiped out right after method's return
    char* cString = (char*)malloc(strlen(nsStringUtf8) + 1);
    strcpy(cString, nsStringUtf8);

    return cString;
}

void InitIos(){
    [[UnityPlugin shared] InitIos];
}

char* GetRotationAngleStatusIos(){
    NSString *str =[[UnityPlugin shared] GetRotationAngleStatusIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}

char* GetRotationAngleIos(){
    NSString *str =[[UnityPlugin shared] GetRotationAngleIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}

char* GetMsgBleIos(){
    NSString *str =[[UnityPlugin shared] GetMsgBleIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}

char* GetMsgNtripIos(){
    NSString *str =[[UnityPlugin shared] GetMsgNtripIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}

char* GetAccuracyIos(){
    NSString *str =[[UnityPlugin shared] GetAccuracyIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}

char* GetLocalisationIos(){
    NSString *str =[[UnityPlugin shared] GetLocalisationIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}
char* GetFixModeIos(){
    NSString *str =[[UnityPlugin shared] GetFixModeIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}

char* GetLogDataIos(){
    NSString *str =[[UnityPlugin shared] GetLogDataIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}

char* GetMuDesignInfoDataIos(){
    NSString *str =[[UnityPlugin shared] GetMuDesignInfoDataIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}

char* GetMuDesignBatteryDataIos(){
    NSString *str =[[UnityPlugin shared] GetMuDesignBatteryDataIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}
char* GetMuDesignMagnetoDataIos(){
    NSString *str =[[UnityPlugin shared] GetMuDesignMagnetoDataIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}
char* GetMuDesignStatsDataIos(){
    NSString *str =[[UnityPlugin shared] GetMuDesignStatsDataIos];
    char* stringConverted = convertNSStringToCString(str);

    return stringConverted;
}
bool GetMuDesignConfAckAnswerIos(){
    return [[UnityPlugin shared] GetMuDesignConfAckAnswerIos];
}



void ConnectActIos(){
    [[UnityPlugin shared] ConnectActIos];
}
void DisconnectActIos(){
    [[UnityPlugin shared] DisconnectActIos];
}

NSString* CreateNSString (const char* string)
 {
   if (string)
     return [NSString stringWithUTF8String: string];
   else
         return [NSString stringWithUTF8String: ""];
 }

void SetScenario(int scenario){
    [[UnityPlugin shared] SetScenarioWithScenario:(scenario)];
}

void SetMacAdressIos(const char* string){
    [[UnityPlugin shared] SetMacAdressIosWithString:CreateNSString(string)];
}
void SetServerIpNtripIos(const char* string){
    [[UnityPlugin shared] SetServerIpNtripIosWithString:CreateNSString(string)];
}
void SetServerPortIos(const char* string){
    [[UnityPlugin shared] SetServerPortIosWithString:CreateNSString(string)];
}
void SetUsernameIos(const char* string){
    [[UnityPlugin shared] SetUsernameIosWithString:CreateNSString(string)];
}
void SetPasswordIos(const char* string){
    [[UnityPlugin shared] SetPasswordIosWithString:CreateNSString(string)];
}
void SetMountPointIos(const char* string){
    [[UnityPlugin shared] SetMountPointIosWithString:CreateNSString(string)];
}
