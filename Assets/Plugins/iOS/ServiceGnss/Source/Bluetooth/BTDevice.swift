import Foundation
import CoreBluetooth

// BLE Device Delegate
protocol BTDeviceDelegate: class {
    func streamCallBack()
    func deviceConnected()
    func deviceReady()
    func deviceNotSyslor()
    
    //func deviceBlinkChanged(value: Bool)
    //func deviceSpeedChanged(value: Int)
    func deviceSerialChanged(value: String)
    func deviceDisconnected()
}

//
protocol MuSyslorMessageDelegate: class {
    func updateFirmVersion(version: String)
    func updateCryptactive(value: Bool)
    func updateCryptKey(key : String)
    func updateUartBaudeRate(value : Int)
    func updateStatsOcc(occ : String)
    func updateStatsAccu(Acc : String)
    func updateBattVoltage(val : Float)
    func updateBattCharge(val : Float)
    func updateUnknownMessage(messgae : String)
    func updateNmeaMessage(message : String)
    func updateStatsMessage(occ : Int, accu:Int)
    
    //func updateNmeaMessage(message: String)
    func updateUbxMessage(message: String)
    func updateMagneto(x:Float,y:Float,z:Float);
    func updateStatsOcc(occ:Int)
    func updateConfAnswer(ans:Bool)
    func updateSleepIfInact(delaySec:Int)
    func updateFirmStartAnswer(ans:Bool)
    func updateFirmEndAnswer(ans:Bool)
    func updateFirmewareUpdateProgress(progress:Int)
    
}

protocol MuSyslorMessageUbxDelegate: class {
    func MuSyslorMessageUbxDelegate(msgs:[String:String])
}

// BLE Device Class
class BTDevice: NSObject {
    let debug:Bool = true
    let TAG="     ¡¡ BLE DEVICE ¡¡ "
    // =====================================================================
    func log(msg: String){
        if debug{
            print(TAG,msg)
        }
    }
    // MSG
    public var updading_firmeware:Bool = false;
    public var updating_firmeware_progress:Int = 0;
    // ATTR
    private let peripheral: CBPeripheral
    private let manager: CBCentralManager
    private var readChar: CBCharacteristic?
    private var writeChar: CBCharacteristic?
    weak var delegateBT: BTDeviceDelegate?
    weak var delegateMessage : MuSyslorMessageDelegate?
    weak var delegateMessageUbx : MuSyslorMessageUbxDelegate?
    
    //var pendingSentences = Queue<Data>()

    
    var name: String {
        return peripheral.name ?? "Unknown device"
    }
    var macAdrtess: String {
        return peripheral.identifier.uuidString
        
    }
    
    var detail: String {
        return peripheral.identifier.description
    }
    
    private(set) var serial: String?
    
    
    // Syslor & muDesign
    var crypted : Bool = true
    
    //
    init(peripheral: CBPeripheral, manager: CBCentralManager) {
        self.peripheral = peripheral
        self.manager = manager
        super.init()
        self.peripheral.delegate = self
    }
    
    //
    func connect() {
        manager.connect(peripheral, options: nil)
    }
    
    //
    func disconnect() {
        manager.cancelPeripheralConnection(peripheral)
        log(msg:"Manager is disconnected : = " + manager.description)
        if updading_firmeware{
            CancelUpdateWireframe()
        }
    }
}

//
extension BTDevice {
    // these are called from BTManager, do not call directly
    
    func streamCallback() {
        peripheral.discoverServices([BTUUIDs.GnssService])
        delegateBT?.streamCallBack()
    }
    
    //
    func connectedCallback() {
        peripheral.discoverServices([BTUUIDs.GnssService])
        delegateBT?.deviceConnected()
    }
    
    //
    func disconnectedCallback() {
        delegateBT?.deviceDisconnected()
    }
    
    //
    func errorCallback(error: Error?) {
        log(msg: "Device: error \(String(describing: error))")
    }
}

//
extension BTDevice: CBPeripheralDelegate {
    //
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        log(msg: "Device: discovered services")
        peripheral.services?.forEach {
            //log(msg: "  \($0)")
            //log(msg: "=> ")
            if $0.uuid == BTUUIDs.GnssService {
                peripheral.discoverCharacteristics([BTUUIDs.GnssReadCharacteristic,BTUUIDs.GnssWriteCharacteristic], for: $0)
            }else {
                //peripheral.discoverCharacteristics(nil, for: $0)
            }
        }
        log(msg: "")
    }
    
    //
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        var isReadCharacteristic = false
        var isWriteCharacteristic = false
        log(msg: "Device: discovered characteristics")
        service.characteristics?.forEach {
            log(msg: "---------- ----->   \($0)")
            if $0.uuid == BTUUIDs.GnssReadCharacteristic {
                isReadCharacteristic = true
                self.readChar = $0
                //peripheral.readValue(for: $0)
                peripheral.setNotifyValue(true, for: $0)
            } else if $0.uuid == BTUUIDs.GnssWriteCharacteristic {
                isWriteCharacteristic = true
                self.writeChar = $0
                activateCrypt()
                //desactivateCrypt()
            }
        }
        log(msg: "")
        if isWriteCharacteristic && isReadCharacteristic{
            delegateBT?.deviceReady()
        }else{
            delegateBT?.deviceNotSyslor()
        }
    }
    
    //
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        var bytes = Array(repeating: 0 as UInt8, count:characteristic.value!.count )
        
        characteristic.value!.copyBytes(to: &bytes, count:characteristic.value!.count)
        //print("----------------------------+++++")
        processMessages(messageUInt8: bytes)
        /*do{
            let aes = try AES(keyString: "1234567890ABCDEF")
            let dataOb:Data = Data( bytes)
                let res = try aes.decrypt( dataOb)
            
            if let string = String(data: res, encoding: .ascii) {
                print("0000000000000>",string)
            } else {
                print("not a valid UTF-8 sequence")
            }
        } catch {
            print("Something went wrong: \(error)")
        }*/
    }
}

//
extension BTDevice {
    func cleanString(string : String)->String{
        var tmp : String = string.replacingOccurrences(of: "}\n", with: "")
        tmp = tmp.replacingOccurrences(of: "}", with: "")
        tmp = tmp.replacingOccurrences(of: "\n", with: "")
        tmp = tmp.replacingOccurrences(of: "\r", with: "")
        tmp = tmp.replacingOccurrences(of: "\0", with: "")

        return tmp
    }
    
    
    //
    func processMessages(messageUInt8 : [UInt8]){
        if crypted == true{
            do{
                let aes = try AES(keyString: ENCRYPT_CONFIG.KEY)
                let dataOb:Data = Data( messageUInt8)
                    let res = try aes.decrypt( dataOb)
                
                if let string = String(data: res, encoding: .ascii) {
                    //print("CRYPTED : ")
                    parseMessages(clearMessageStr: string)
                    //print("0000000000000>",string)
                } else {
                    log(msg: "not a valid UTF-8 sequence")
                }
            } catch {
                log(msg: "Something went wrong: \(error)")
            }
        }
        else{
            if let string = String(data: Data(messageUInt8), encoding: .ascii) {
                //print("CLEAR : ")
                parseMessages(clearMessageStr: string)
                //print("0000000000000>",string)
            }
        }
    }
    
    //
    func parseMessages(messageData : Data){
        if let str_msg = String(data: messageData, encoding: .ascii){
            parseMessages(clearMessageStr: str_msg )
        }
    }
        //
    func parseMessages(clearMessageStr : String) {
        //
        // NMEA
        if clearMessageStr.starts(with: RESPONSE_HEADER.NMEA){
            delegateMessage?.updateNmeaMessage(message: cleanString(string : clearMessageStr))
            //delegateMessage?.updateNmeaMessage(message:cleanString(string : clearMessageStr));
            return
        }
        
        // UBX
        if clearMessageStr.starts(with:RESPONSE_HEADER.UBX){
            delegateMessage?.updateUbxMessage(message:cleanString(string : clearMessageStr));
            return
        }
        
        // FIRM VERSION
        if clearMessageStr.starts(with: RESPONSE_HEADER.FIRM_VERSION){
            //print("FIRM_VERSION message received")
            var tmp = ""
            tmp = clearMessageStr.replacingOccurrences(of: RESPONSE_HEADER.FIRM_VERSION, with: "")

            delegateMessage?.updateFirmVersion(version : cleanString(string: tmp))
            return
        }
        // charge level
        if clearMessageStr.starts(with: RESPONSE_HEADER.BATT_CHARGE_LEVEL){
            //print("BATT_CHARGE_LEVEL message received", ClearMessageStr)
            var tmp = ""

            tmp = clearMessageStr.replacingOccurrences(of: RESPONSE_HEADER.BATT_CHARGE_LEVEL, with: "")
            delegateMessage?.updateBattCharge(val: Float(cleanString(string: tmp)) ?? -1)
            return
        }
        
        // voltage level
        if clearMessageStr.starts(with: RESPONSE_HEADER.BATT_VOLTAGE_LEVEL){
            //print("BATT_VOLTAGE_LEVEL message received",clearMessageStr)
            var tmp = ""
            tmp = clearMessageStr.replacingOccurrences(of: RESPONSE_HEADER.BATT_VOLTAGE_LEVEL, with: "")
            delegateMessage?.updateBattVoltage(val: Float(cleanString(string: tmp)) ?? -1)
            return
        }
        
        // CRY active
        if clearMessageStr.starts(with: RESPONSE_HEADER.CRY_ACTIVE){
            
            //print("CRY_ACTIVE message received")
            var tmp = ""
            tmp = clearMessageStr.replacingOccurrences(of: RESPONSE_HEADER.CRY_ACTIVE, with: "")
            delegateMessage?.updateCryptactive(value: (Int(cleanString(string: tmp))==1) )
            return
        }
        
        // CRY KEY
        if clearMessageStr.starts(with: RESPONSE_HEADER.CRY_KEY){
            //print("CRY_KEY message received")
            var tmp = ""
            tmp = clearMessageStr.replacingOccurrences(of: RESPONSE_HEADER.CRY_KEY, with: "")
            delegateMessage?.updateCryptKey(key : cleanString(string: tmp) )
            return
        }
        
        // UART BAUDRATE
        if clearMessageStr.starts(with: RESPONSE_HEADER.UART_BAUDRATE){
            var tmp = ""
            //log(msg: clearMessageStr)
            tmp = clearMessageStr.replacingOccurrences(of: RESPONSE_HEADER.UART_BAUDRATE, with: "")
            delegateMessage?.updateUartBaudeRate(value : Int(cleanString(string: tmp)) ?? 0 )
            return
        }
        
        // MAGNETO ALL
        if clearMessageStr.starts(with: RESPONSE_HEADER.MAGNETO_ALL){
            var msg:String = clearMessageStr.replacingOccurrences(of:RESPONSE_HEADER.MAGNETO_ALL, with: "")
            msg = msg.replacingOccurrences(of:" \"y\": ", with: "")
            msg = msg.replacingOccurrences(of:" \"z\": ", with: "")
            msg = msg.replacingOccurrences(of:"}\n", with: "")
            let result : [Substring] = msg.split(separator: ",")
            if (result.count >= 3) {
            delegateMessage?.updateMagneto(x:Float(result[0]) ?? 0, y:Float(result[1]) ?? 0, z:Float(result[2]) ?? 0);
            }
            return
        }
        
        // STATS OCC
        if clearMessageStr.starts(with: RESPONSE_HEADER.STATS_OCC){
            //message = message.replace(RESPONSE_HEADER.STATS_OCC,"");
            //message = message.replace("}","");
            let msg:String=cleanString(string : clearMessageStr);
            let occ=Int(msg) ?? 0
            delegateMessage?.updateStatsOcc(occ: occ);
            return
        }

        if clearMessageStr.starts(with:RESPONSE_HEADER.SLEEP_IF_INACT){
            //message = message.replace(RESPONSE_HEADER.SLEEP_IF_INACT,"");
            //message = message.replace("}","");
            let msg:String=cleanString(string : clearMessageStr);
            let del=Int(msg) ?? 0
            delegateMessage?.updateSleepIfInact(delaySec: del);
            return
        }
        
        if clearMessageStr.starts(with:RESPONSE_HEADER.CONF_ACK){
            delegateMessage?.updateConfAnswer(ans: true);
            return
        }
        if clearMessageStr.starts(with:RESPONSE_HEADER.CONF_NACK){
            delegateMessage?.updateConfAnswer(ans:false);
            return
        }
        if clearMessageStr.starts(with:RESPONSE_HEADER.STATS_ACK){
            delegateMessage?.updateConfAnswer(ans:true);
            return
        }
        if clearMessageStr.starts(with: RESPONSE_HEADER.STATS_NACK){
            delegateMessage?.updateConfAnswer(ans:false);
            return
        }
        if clearMessageStr.starts(with:RESPONSE_HEADER.WRITE_BEGIN_ACK){
            delegateMessage?.updateFirmStartAnswer(ans:true);
            return
        }
        if clearMessageStr.starts(with:RESPONSE_HEADER.WRITE_BEGIN_NACK){
            delegateMessage?.updateFirmStartAnswer(ans:false);
            return
        }
        if clearMessageStr.starts(with:RESPONSE_HEADER.WRITE_END_ACK){
            delegateMessage?.updateFirmEndAnswer(ans:true);
            return
        }
        if clearMessageStr.starts(with:RESPONSE_HEADER.WRITE_END_NACK){
            delegateMessage?.updateFirmEndAnswer(ans:false);
            updading_firmeware = false
            return
        }
        // else
        //{
                //muDesignMessageUpdate.unknownMessage(message);
        //}

        //  STATS
        if clearMessageStr.starts(with: RESPONSE_HEADER.UART_STATS){
            
            log(msg: "UART_STATS message received")
            var tmp = ""
            tmp = clearMessageStr.replacingOccurrences(of: RESPONSE_HEADER.UART_STATS, with: "")
            tmp = cleanString(string: tmp)
            tmp = tmp.replacingOccurrences(of: "\"RUN_OCC\":", with: "")
            tmp = tmp.replacingOccurrences(of: "\"RUN_ACCU\":", with: "")
            //log(msg: tmp)
            var result : [Substring] = tmp.split(separator: ",")
            if result.count == 2{
                delegateMessage?.updateStatsMessage(occ: Int(result[0]) ?? 0, accu: Int(result[1]) ?? 0)
            }else{
                
            }
            return
        }
        delegateMessage?.updateUnknownMessage(messgae: cleanString(string : clearMessageStr))
    }
    
    //
    func requestMessage(msgStr : String){
        //print("requestMessage str")
        log(msg: "REQ MSG : " + msgStr)
        let buf: [UInt8] = Array(msgStr.utf8)
        requestMessage(msgUInt8 : buf)
    }
    
    //
    func requestMessage(msgUInt8 : [UInt8]){
        let mtu:Int=384
        //print("requestMessage UINT8")
        let r:Data = Data(msgUInt8)
        //print("Clear : All Msg:  [",r.hexStringEncoded(),"]",msgUInt8.count)
        do{
            /*if crypted == true{
                let aes = try AES(keyString : ENCRYPT_CONFIG.KEY)
                let encrypted_msg = try aes.encrypt(msgUInt8)
                let r1:Data = Data(encrypted_msg)
                print("Crypt : [",r1.hexStringEncoded(),"]",encrypted_msg.count)
                if let char = writeChar {
                    peripheral.writeValue(Data(bytes: encrypted_msg), for: char, type: .withoutResponse)
                }
            }else{
                if let char = writeChar {
                    peripheral.writeValue(Data(bytes: msgUInt8), for: char, type: .withoutResponse)
                }
            }*/
            
            var buffer_capacity:Int=0
                        
            if (crypted) {
                buffer_capacity = 16 * ((mtu - 3) / 16 - 1);
                if ( buffer_capacity < 16) {return ;}
            } else {
                buffer_capacity = mtu - 3;
            }
            var n:Int=0 ;
            n = (r.count % buffer_capacity == 0) ? (r.count / buffer_capacity) : (r.count / buffer_capacity) + 1;
            for i in  0..<n {
                var buffer:Data
                if((i+1)*buffer_capacity<r.count){
                    buffer = r[(i*buffer_capacity)..<((i+1)*buffer_capacity)];
                }else{
                    buffer = r[(i*buffer_capacity)..<r.count];
                }
                //print("Clear :",i," : [",buffer.hexStringEncoded(),"]",buffer.count)
                ////Log.d("QUEUEXXX : ", "["+(buffer.length)+"]" +writeQueue.size());
                if(crypted ){
                    ////byte[] cryptedData = muDesignCrypt.code(buffer);
                    //writeQueue.add(cryptedData);
                    let aes = try AES(keyString : ENCRYPT_CONFIG.KEY)
                    let encrypted_msg = try aes.encrypt(Array(buffer))
                    let r1:Data = Data(encrypted_msg)
                    //print("Crypt : [",r1.hexStringEncoded(),"]",encrypted_msg.count)
                    if let char = writeChar {
                        peripheral.writeValue(Data(bytes: encrypted_msg), for: char, type: .withoutResponse)
                    }
                }else {
                    //writeQueue.add(buffer);
                    if let char = writeChar {
                        peripheral.writeValue(buffer, for: char, type: .withoutResponse)
                    }
                }
            }

        } catch {
            log(msg: "Something went wrong: \(error)")
        }
    }
    
    //
    func activateCrypt(){
        crypted = false
        requestMessage(msgStr: CONFIG_MESSAGE.SetCryActive(param: 1))
        crypted = true
    }
    
    //
    func desactivateCrypt(){
        crypted = true
        requestMessage(msgStr: CONFIG_MESSAGE.SetCryActive(param: 0))
        crypted = false

    }
    /** *************************************************************************
         *
         * @param value
         */
    func  SetCryKey( value:String) {
            ////Log.e("KEY : ",MuDesignMessageParser.CONFIG_MESSAGE.SetCryKey(value));
            //byte[] res = (MuDesignMessageParser.CONFIG_MESSAGE.SetCryKey(value)).getBytes();
        requestMessage(msgStr:CONFIG_MESSAGE.SetCryKey(param: value));
        }
        /** *************************************************************************
         *
         * @param value
         */
    func  SetUartBaudrate( value:Int) {
            //byte[] res = (MuDesignMessageParser.CONFIG_MESSAGE.SetUartBaudrate(value)).getBytes();
        requestMessage(msgStr:CONFIG_MESSAGE.SetUartBaudrate(param: value));
        }
        /** *************************************************************************
         *
         * @param value
         */
    func  SetSleepIfInact(value:Int) {
            //byte[] res = (MuDesignMessageParser.CONFIG_MESSAGE.SetSleepIfInact(value)).getBytes();
        requestMessage(msgStr:CONFIG_MESSAGE.SetSleepIfInact(param: value));

        }
        /** *************************************************************************
         *
         */
        func  SetResetStats() {
            //byte[] res = (MuDesignMessageParser.CONFIG_MESSAGE.SetResetStats()).getBytes();
            requestMessage(msgStr:STATS_MESSAGE.SetResetStats());
        }
        /** *************************************************************************
         *
         * @param value
         */
    func SendBeginFirmewareWrite(value:Int) {
            //byte[] res = (MuDesignMessageParser.FIRM_MESSAGE.SetWriteBeginSize(value)).getBytes();
        requestMessage(msgStr:FIRM_MESSAGE.SetWriteBeginSize(octet: value));
        }
        /** *************************************************************************
         *
         * @param firmeware
         */
   
        func  WriteFirmware() {
            updating_firmeware_progress = 0
            updading_firmeware=true
            DispatchQueue.global(qos: .utility).async { [unowned self] in
                var updateFirmeware:MuDesignUpdateFimeware=MuDesignUpdateFimeware()
                updateFirmeware.loadFimewareFile()
                var sz:Int=updateFirmeware.bytesData.count
                var begin_msg = FIRM_MESSAGE.getMessageGetVersion(size: sz)
                print(begin_msg)
                requestMessage(msgStr: begin_msg)
                sleep(5)
                var cnt=updateFirmeware.getBlocksCount()
                for i in 0..<cnt{
                    var dt = updateFirmeware.getBlock(num: i)
                    var aryData = Array(dt)
                        
                    requestMessage(msgUInt8: aryData)
                    updating_firmeware_progress = Int(Float(0.5+100.0 * Float(i) / Float(cnt)))
                    delegateMessage?.updateFirmewareUpdateProgress(progress:updating_firmeware_progress)
                    //print("------->",updating_firmeware_progress,"%")
                    usleep(50000)
                    if !updading_firmeware{
                        log(msg: "Break update "+String(updading_firmeware))
                        break
                    }
                }
            }
            //updading_firmeware=false
        }
        func CancelUpdateWireframe(){
            //Log.d("FIRM", "Cancel updating firmeware");
            updading_firmeware=false;
        }
        func GetUpdadingFirmewareProgress()->Int{
            return updating_firmeware_progress;
        }
        /** *************************************************************************
         *
         * @param bleMuCryptedCommunication
         * @param MTU
         */
        func UpdateMuDesignDynamicData(){
            //byte[] res = MuDesignMessageParser.BATT_MESSAGE.GET_CHARGE_LEVEL.getBytes();
            requestMessage(msgStr:BATT_MESSAGE.GET_CHARGE_LEVEL);

            //res = MuDesignMessageParser.BATT_MESSAGE.GET_VOLTAGE_LEVELE.getBytes();
            requestMessage(msgStr:BATT_MESSAGE.GET_VOLTAGE_LEVELE);

            //res = MuDesignMessageParser.STATS_MESSAGE.RUN_OCC.getBytes();
            requestMessage(msgStr:STATS_MESSAGE.RUN_OCC);

            //res = MuDesignMessageParser.STATS_MESSAGE.RUN_ACCU.getBytes();
            requestMessage(msgStr:STATS_MESSAGE.RUN_ACCU);
        }
        
        /** *************************************************************************
         *
         * @param bleMuCryptedCommunication
         * @param MTU
         */
        func UpdateMuDesignHighDynamicData(){
            //byte[] res = MuDesignMessageParser.MAGNETO_MESSAGE.GET_ALL.getBytes();
            requestMessage(msgStr:MAGNETO_MESSAGE.GET_ALL);
        }

        /**
         *
         * @param bleMuCryptedCommunication
         * @param MTU
         * @param key
         */
    func SendDefaultEsp32Parameters(  key:String){
        //> UART Bauderate : 115200 ;
        SetUartBaudrate(value: CONFIG_MESSAGE.DEFAULT_BAUDRATE_UART_ESP32_ZED); // OK

        //> Délai de mise hors tension si non connecté : 15 min
        SetSleepIfInact(value: CONFIG_MESSAGE.SLEEP_TIME_INACT); // OK

        //> Reset les stats
        SetResetStats(); // OK
       
        //> Clé de cryptage : Nouveau code interne (non connu de MuDesign)
        SetCryKey( value: key);
     
            //> Cryptage : Actif
        activateCrypt();
    }

    //
    /*func requestBatteryEnergy() {
        requestMessage(msgStr : BATT_MESSAGE.GET_CHARGE_LEVEL)
    }
    
    //
    func requestChargeLevel(){
        do{
            let msg :String = CONFIG_MESSAGE.SET_CRY_ACTIVE
            let buf: [UInt8] = Array(msg.utf8)
            let aes = try AES(keyString: "1234567890ABCDEF")
            let res = try aes.encrypt(buf)

            if let char = writeChar {
                peripheral.writeValue(Data(bytes: res), for: char, type: .withoutResponse)
            }
        } catch {
            print("Something went wrong: \(error)")
        }
    }*/
    
    func SendDefaultUbloxParameters(){
        //byte[] res = MuDesignMessageParser.UBLOX_MESSAGE.VERSION;
        requestMessage(msgUInt8: UBLOX_MESSAGE.VERSION);
    }
    
    func ResetSettings(key:String){
        log(msg: "Reset Esp32 settings ")
        SendDefaultEsp32Parameters(key: key)
        log(msg: "Reset UBLOX IC ")
        SendDefaultUbloxParameters()
        
    }
}
extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}
public extension Data {
    private static let hexAlphabet = Array("0123456789abcdef".unicodeScalars)
    func hexStringEncoded() -> String {
        String(reduce(into: "".unicodeScalars) { result, value in
            result.append(Self.hexAlphabet[Int(value / 0x10)])
            
            result.append(Self.hexAlphabet[Int(value % 0x10)])
            result.append(" ")
        })
    }
}
