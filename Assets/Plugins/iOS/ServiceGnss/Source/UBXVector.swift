//
//  UBXVector.swift
//  ServiceGNSS
//
//  Created by ivirtual on 21/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation


/**
 *
 */
class UBXVector {
    
    var data:[UInt8]=[UInt8]()

    func add( value:KeysId) {
        addU4(i: value.getKey());
    }

    func addU4( i:UInt32) {
        let byteBuffer:[UInt8] = UbxTools.intToByteBuffer(x: i,LittleEndian: true);
        for ibyte in byteBuffer{
            data.append(ibyte);
        }
    }

    func addU2( i:UInt16) {
        let byteBuffer = UbxTools.shortToByteBuffer(x:i,LittleEndian: true);
        for ibyte in byteBuffer{
             data.append(ibyte);
        }
    }

    func addU2( position:Int,  i:UInt16) {
        let byteBuffer = UbxTools.shortToByteBuffer(x:i,LittleEndian: false);
        for  ibyte in byteBuffer {
            data.insert(ibyte, at:position);
        }
    }
    
    func toString()->String{
        return data.bytesToHex(spacing: " ")
    }

    func addU1( i:UInt8) {
        data.append(i);
    }

    public static func test(){
        let msgBuffer:UBXVector = UBXVector();
        msgBuffer.addU1(i: 0x24);
        msgBuffer.addU1(i: 0x36);
        msgBuffer.addU1(i: 0x99);
        msgBuffer.addU1(i: 0x01);
        msgBuffer.addU1(i: 0x15);
        msgBuffer.addU1(i: 0x59);
        msgBuffer.addU2(i: 0x6589);
        msgBuffer.addU4(i: 0x01020304);

        print(msgBuffer.toString());
        // must print : [24,36,99,01,15,59,89,65,04,03,02,01,]
    }
    public func size()->Int{
        return data.count
    }
}
