import Foundation

public class ENCRYPT_CONFIG{
    static var KEY = "1234567890ABCDEF"
   // static var KEY = "0000000000000000"
}

//
public  class CONFIG_MESSAGE {
    static let DEFAULT_BAUDRATE_UART_ESP32_ZED:Int=115200;
    static let SLEEP_TIME_INACT:Int=15;
    //static let SET_CRY_ACTIVE:String = "@SYS_CONF_SET#{\"CRY_ACTIVE\":1}\n"
    //static let SET_CRY_DESACTIVE    :String = "@SYS_CONF_SET#{\"CRY_ACTIVE\":0}\n";
    static let IS_CRY_ACTIVE        :String = "@SYS_CONF_GET#{\"CRY_ACTIVE\":null}\n";//OK
    static let GET_KRY_KEY          :String = "@SYS_CONF_GET#{\"CRY_KEY\":null}\n";//OK
    static let GET_UART_BAUDRATE    :String = "@SYS_CONF_GET#{\"UART_BAUDRATE\":null}\n";
    static let GET_SLEEP_IF_INACT   :String = "@SYS_CONF_GET#{\"SLEEP_IF_INACT\":null}\n"; //OK
    static let CONF_ACK             :String = "@SYS_CONF_SET#{\"ACK\":\"Ok\"}\n";
    static let CONF_NACK            :String = "@SYS_CONF_SET#{\"NACK\":\"Xxx\"}\n";
    static let SET_CRY_ACTIVE       :String = "@SYS_CONF_SET#{\"CRY_ACTIVE\":";
    static let SET_CRY_KEY          :String = "@SYS_CONF_SET#{\"CRY_KEY\":";
    static let SET_UART_BAUDRATE    :String = "@SYS_CONF_SET#{\"UART_BAUDRATE\":";
    static let SET_SLEEP_IF_INACT   :String = "@SYS_CONF_SET#{\"SLEEP_IF_INACT\":";  //OK
    static let END                  :String = "}\n";
    /** *************************************************************************
             *
             * @param param
             * @return
             */
    public static func SetCryActive( param:Int) ->String{
        return CONFIG_MESSAGE.SET_CRY_ACTIVE + String(param) + CONFIG_MESSAGE.END;
            }
            /** *************************************************************************
             *
             * @param param
             * @return
             */
    public static func SetCryKey(param : String )->String {
        return CONFIG_MESSAGE.SET_CRY_KEY + "\"" + param + "\"" + CONFIG_MESSAGE.END;
            }
            /** *************************************************************************
             *
             * @param param
             * @return
             */
    public static func SetUartBaudrate( param:Int) ->String{
        return CONFIG_MESSAGE.SET_UART_BAUDRATE + String(param) + CONFIG_MESSAGE.END;
            }
            /** *************************************************************************
             *
             * @param param
             * @return
             */
    public static func SetSleepIfInact( param:Int) ->String{
        return CONFIG_MESSAGE.SET_SLEEP_IF_INACT + String(param) + CONFIG_MESSAGE.END;
            }
}
        

//
public class STATS_MESSAGE {
    static let GET_ALL   : String = "@SYS_STATS_GET#{\"ALL\": null}\n";//@SYS_STATS_GET#{"RUN_OCC":86,"RUN_ACCU":18297}
    static let RUN_OCC   : String = "@SYS_STATS_GET#{\"RUN_OCC\": null}\n";//@SYS_STATS_GET#{"NACK": "RUN_OCC param unknown!"}
    static let RUN_ACCU  : String = "@SYS_STATS_GET#{\"RUN_ACCU\": null}\n";// sans réponse!
    static let RESET_ALL : String = "@SYS_STATS_RESET#";//@SYS_#{"NACK": "Invalid header in request!"}
    /** *************************************************************************
     *
     * @return
     */
    public static func SetResetStats()->String {
        return STATS_MESSAGE.RESET_ALL;
    }
}

//
public  class MAGNETO_MESSAGE {
    static let GET_ALL : String = "@SYS_MAGNETO_GET#{\"ALL\": null}\n";
}

//
public  class BATT_MESSAGE {
    static let GET_ALL              :String = "@SYS_BATT_GET#{\"ALL\":null}\n";//OK
    static let GET_VOLTAGE_LEVELE   :String = "@SYS_BATT_GET#{\"VOLTAGE_LEVEL\":null}\n";//OK
    static let GET_CHARGE_LEVEL     :String = "@SYS_BATT_GET#{\"CHARGE_LEVEL\":null}\n";//OK
}

//
public  class FIRM_MESSAGE {
    static let GET_VERSION      :String = "@SYS_FIRM_GET#{\"VERSION\": null}\n";//OK
    static let WRITE_BEGIN_SIZE :String = "@SYS_FIRM_WRITE_BEGIN#{\"size\":[size of the firmware in octet]}";
    static let WRITE_BEGIN_ACK  :String = "@SYS_FIRM_WRITE_BEGIN#{\"ACK\": \"OK\"}\n";
    static let WRITE_BEGIN_NACK :String = "@SYS_FIRM_WRITE_BEGIN#{\"NACK\": \"Xxx\"}\n";
    static let WRITE_END_ACK    :String = "@SYS_FIRM_WRITE_END#{\"ACK\": \"OK\"}\n";
    static let WRITE_END_NACK   :String = "@SYS_FIRM_WRITE_END#{\"NACK\": \"Xxx\"}\n";
    static let END              :String = "}\n";
            /** *************************************************************************
             *
             * @param octet
             * @return
             */
    static func SetWriteBeginSize(octet:Int) ->String{
        return FIRM_MESSAGE.WRITE_BEGIN_SIZE + String(octet) + FIRM_MESSAGE.END;
            }
    static func getMessageGetVersion(size:Int)->String{
        return "@SYS_FIRM_WRITE_BEGIN#{\"size\":" + String(size) + "}\n"
    }
}

//
public  class UBLOX_MESSAGE {
    static let UART1_UBX_ : [UInt8] = [ 0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00, 0x00, 0x96, 0x00, 0x00, 0x23, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xAD, 0x64];
          
    static let UART1_UBX_NMEA_ : [UInt8] = [ 0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00, 0x00, 0x96, 0x00, 0x00, 0x23, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xAF, 0x70];
          
    static let UART1_UBX_NMEA_RTCM_ : [UInt8] = [ 0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00, 0x00, 0x96, 0x00, 0x00, 0x23, 0x00, 0x23, 0x00, 0x00, 0x00, 0x00, 0x00, 0xCF, 0x30];
          
    static let UART1_NMEA_HI_ACC : [UInt8] = [ 0xB5, 0x62, 0x06, 0x17, 0x14, 0x00, 0x00, 0x41, 0x00, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7D, 0xDF];
          
    static let SAVE_CFG : [UInt8] = [ 0xB5, 0x62, 0x06, 0x09, 0x0D, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x1D, 0xAB];
          
    static let TIMEPULSE_500MS : [UInt8] = [ 0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xF4, 0x01, 0x01, 0x00, 0x00, 0x00, 0x0A, 0x75];
          
    static let TIMEPULSE_1000MS : [UInt8] = [ 0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xE8, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00, 0x37];
          
    static let ACK : [UInt8] = [ 0xB5, 0x62, 0x05, 0x01, 0x02, 0x00, 0x06, 0x06, 0x14, 0x3D];
          
    static let NACK : [UInt8] = [ 0xB5, 0x62, 0x05, 0x00, 0x02, 0x00, 0x06, 0x93, 0xA0, 0xC5];
          
    static let VERSION : [UInt8] = [ 0xB5, 0x62, 0x05, 0x00, 0x02, 0x00, 0x06, 0x93, 0xA0, 0xC5];
}

//
public class RESPONSE_HEADER {
    static let FIRM_VERSION     :String = "@SYS_FIRM_GET#{\"VERSION\": ";// @SYS_FIRM_GET#{"VERSION": "1.0.5"}
    static let BATT_CHARGE_LEVEL    :String = "@SYS_BATT_GET#{\"CHARGE_LEVEL\": ";// @SYS_BATT_GET#{"CHARGE_LEVEL": 57}
    static let BATT_VOLTAGE_LEVEL   :String = "@SYS_BATT_GET#{\"VOLTAGE_LEVEL\": ";// @SYS_BATT_GET#{"VOLTAGE_LEVEL": 3.541}
    static let BATT_ALL         :String = "@SYS_BATT_GET#{\"VOLTAGE_LEVEL\": ";//
    //static let NMEA             :String = "$GN";//$GNVTG,,T,,M,0.088,N,0.162,K,A*38
    static let CRY_ACTIVE       :String = "@SYS_CONF_GET#{\"CRY_ACTIVE\": ";//$GNVTG,,T,,M,0.088,N,0.162,K,A*38
    static let CRY_KEY          :String = "@SYS_CONF_GET#{\"CRY_KEY\": \"";//$GNVTG,,T,,M,0.088,N,0.162,K,A*38
    static let UART_BAUDRATE    :String = "@SYS_CONF_GET#{\"UART_BAUDRATE\": ";//@SYS_CONF_GET#{"UART_BAUDRATE": 115200}
    static let UART_STATS       :String = "@SYS_STATS_GET#{";
    
    static let data :[UInt8] = [0xB5, 0x62]
    static let NMEA:String = String(bytes:data , encoding: .ascii) ?? "" ;
    //static let NMEA             :String = String( [0xB5, 0x62]);
    static let UBX              :String = "$GN";//$GNVTG,,T,,M,0.088,N,0.162,K,A*38
    static let MAGNETO_ALL      :String = "@SYS_MAGNETO_GET#{\"x\": ";
    static let STATS_ALL        :String = "@SYS_STATS_GET#{\"RUN_OCC\":";
    static let STATS_OCC        :String = "@SYS_STATS_GET#{\"RUN_OCC\":";
    static let STATS_ACCU       :String = "@SYS_STATS_GET#{\"RUN_ACCU\":";
    static let STATS_ACK        :String = "@SYS_STATS_RESET#{\"ACK\":";
    static let STATS_NACK       :String = "@SYS_STATS_RESET#{\"ACK\":";
    static let CONF_ACK         :String = "@SYS_CONF_SET#{\"ACK\":";
    static let CONF_NACK        :String = "@SYS_CONF_SET#{\"NACK\":";
    static let SLEEP_IF_INACT   :String = "@SYS_CONF_GET#{\"SLEEP_IF_INACT\": "; //OK
    static let WRITE_BEGIN_ACK  :String = "@SYS_FIRM_WRITE_BEGIN#{\"ACK\":";
    static let WRITE_BEGIN_NACK :String = "@SYS_FIRM_WRITE_BEGIN#{\"NACK\":";
    static let WRITE_END_ACK    :String = "@SYS_FIRM_WRITE_END#{\"ACK\":";
    static let WRITE_END_NACK   :String = "@SYS_FIRM_WRITE_END#{\"NACK\":";

    
 /*   @SYS_CONF_GET#{"CRY_ACTIVE": 0}
    @SYS_CONF_GET#{"CRY_KEY": "**********"}*/
}
