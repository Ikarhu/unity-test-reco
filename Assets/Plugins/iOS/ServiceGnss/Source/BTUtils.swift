import CoreBluetooth

//
extension CBManagerState: CustomStringConvertible {
    //
    public var description: String {
        switch self {
            case .unknown: return "unknown"
            case .resetting: return "resetting"
            case .unsupported: return "unsupported"
            case .unauthorized: return "unauthorized"
            case .poweredOff: return "poweredOff"
            case .poweredOn: return "poweredOn"
        }
    }
}

//
extension Data {
    //
    func parseBool() -> Bool? {
        guard count == 1 else { return nil }
        return self[0] != 0 ? true : false
    }
    //
    func parseInt() -> UInt8? {
        guard count == 1 else { return nil }
        return self[0]
    }
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}
