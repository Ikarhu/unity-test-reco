
import Foundation
import CoreBluetooth
// ==============================================================================================
// ==============================================================================================
@available(iOS 12.0, *)
class ServiceGnss{
    let debug:Bool = true
    let TAG="     ¡¡ SERVICE ¡¡ "
    // =====================================================================
    func log(msg: String){
        if debug{
            print(TAG,msg)
        }
    }
    private var manager:BTManager?
    // =====================================================================
    
    
    struct MuInfoData : Codable {
        var firm_version: String
        var uart_bauderate: Int
        var crypt_active :String
        var crypt_key: String
        var sleep_if_inact: String //TODO recover these information
        
        func toJson() -> String {
           
            let encodedData = try! JSONEncoder().encode(self)
            let jsonString = String(data: encodedData,
                                    encoding: .utf8)!
            return jsonString
          
        }
    }
    
    struct MuStatsData: Codable {
        var run_occ : Int;
        var run_accu: Int;
        
        func toJson() -> String {
           
            let encodedData = try! JSONEncoder().encode(self)
            let jsonString = String(data: encodedData,
                                    encoding: .utf8)!
            return jsonString
          
        }
    }
    
    struct MuBatteryData: Codable {
        var voltage_level : Float;
        var charge_level: Float;
        
        func toJson() -> String {
           
            let encodedData = try! JSONEncoder().encode(self)
            let jsonString = String(data: encodedData,
                                    encoding: .utf8)!
            return jsonString
          
        }
    }
    
    struct MuDesignMagnetoData: Codable {
        var x_magneto : String;
        var y_magneto: String;
        var z_magneto: String;
        func toJson() -> String {
           
            let encodedData = try! JSONEncoder().encode(self)
            let jsonString = String(data: encodedData,
                                    encoding: .utf8)!
            return jsonString
        }
    }
    
    var newkey:String=""
    let muDataRechreshDela:Double = 60 // seconds
    let muDataRechreshHotDela:Double = 2 // seconds
    var muConfAckAnswer = false
    var muStatsData: MuStatsData?
    var muBatteryData: MuBatteryData?
    var muInfoData: MuInfoData?
    var muDesignMagnetoData: MuDesignMagnetoData?
    
    var mode=0
       let MODE_GNSS = 0
       let MODE_UPDATEFIRMEWARE = 1
       let MODE_RESETDEFAULTSETTINGS = 2
       let MODE_UBLOX = 3
       var res : [UInt8] = []

    // =====================================================================
    var delegate: ServiceGNSSDelegate?
    var nmeaParser : NmeaParser = NmeaParser()
    var ntripClient:NtripClient!
    var device: BTDevice? {
        didSet {
            log(msg: "device didset")
            device?.delegateBT = self
            device?.delegateMessage = self
            device?.delegateMessageUbx = self
        }
    }
    
    // ntrip data
    var url:String=""
    var port:__uint16_t=0
    var user:String=""
    var pass:String=""
    var mountPoint:String=""
    //
    
    // =====================================================================
    deinit {
        log(msg: "service deinit")
        self.disconnect()
    }
    // =====================================================================
    init() {
        log(msg: "service init")
        muInfoData = MuInfoData(firm_version: "-", uart_bauderate: 0, crypt_active: "" , crypt_key: "", sleep_if_inact: "")
        muStatsData = MuStatsData(run_occ: 0, run_accu: 0)
        muBatteryData = MuBatteryData (voltage_level: 0, charge_level: 0)
    }
    // =====================================================================
    func connect(macAdress:String, url:String, port:__uint16_t, user:String, pass:String, mountPoint:String,key:String){
        self.mode = MODE_GNSS
           manager = BTManager(timeout: 10,mac: macAdress) //60 sec * 15 minutes
           manager?.delegate = self
           log(msg: "service initialize")
           ENCRYPT_CONFIG.KEY = key
           nmeaParser.delegate = self
           //ntripClient = NtripClient(url: "ntrip.reseau-orpheon.fr",port: 8500,user: "syslor57",pass: "sys191010lor57", mountPoint: "VRS_RTCM-MSM_FULL")
        ntripClient = NtripClient(url: url,port: port,user: user,pass: pass,mountPoint: mountPoint)
           DispatchQueue.global(qos: .utility).async { [unowned self] in
               ntripClient?.delegate = self
               ntripClient?.start()
               }
    }
    
    func UbloxSendScenario(Scenario:Int, macAdress:String, url:String, port:__uint16_t, user:String, pass:String, mountPoint:String, key:String) {
        self.url = url
        self.port = port
        self.user = user
        self.pass = pass
        self.mountPoint = mountPoint
        self.mode = MODE_UBLOX
        log(msg: "Ublox Initialize")
        manager = BTManager(timeout: 10,mac: macAdress)
        manager?.delegate = self
        ENCRYPT_CONFIG.KEY = key
        nmeaParser.delegate = self
        if (Scenario == 0) {
            res = Ublox.Scenario1()
        } else if (Scenario == 1) {
            res = Ublox.Scenario2()
        }
        DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + .seconds(5)){
            self.mode = self.MODE_GNSS
            self.ntripClient = NtripClient(url: url,port: port,user: user,pass: pass,mountPoint: mountPoint)
               DispatchQueue.global(qos: .utility).async { [unowned self] in
                   ntripClient?.delegate = self
                   ntripClient?.start()
                   }
        }
    }
    
    func resetSettings(macAdress:String,currentKey:String, newkey:String){
            self.mode = MODE_RESETDEFAULTSETTINGS
            self.newkey = newkey
            ENCRYPT_CONFIG.KEY = currentKey
            manager = BTManager(timeout: 10,mac: macAdress)
            manager?.delegate = self
            log(msg: "Initialize reset settings")
            nmeaParser.delegate = self
        }
    func updateFirmeware(macAdress:String, key:String){
           self.mode = MODE_UPDATEFIRMEWARE
           ENCRYPT_CONFIG.KEY = key
           manager = BTManager(timeout: 10,mac: macAdress)
           manager?.delegate = self
           log(msg: "Update firmeware initialized")
           nmeaParser.delegate = self
       }
    
    // =====================================================================
    func disconnect(){
        log(msg: "service disconnect")
               device?.disconnect()
               ntripClient?.disconnect()
               timer?.invalidate()
               timer1?.invalidate()
    }
    
    var timer:Timer?
      var timer1:Timer?
    // =====================================================================
    func getMuBoxData(){
            device?.requestMessage(msgStr : FIRM_MESSAGE.GET_VERSION )
            device?.requestMessage(msgStr: CONFIG_MESSAGE.GET_UART_BAUDRATE)
            /*device?.requestMessage(msgStr : STATS_MESSAGE.GET_ALL )
            device?.requestMessage(msgStr : STATS_MESSAGE.RUN_ACCU )
            device?.requestMessage(msgStr : STATS_MESSAGE.RUN_OCC )
            device?.requestMessage(msgStr: BATT_MESSAGE.GET_CHARGE_LEVEL)
            device?.requestMessage(msgStr: BATT_MESSAGE.GET_VOLTAGE_LEVELE)*/
            device?.UpdateMuDesignDynamicData()
            
            timer = Timer.scheduledTimer(withTimeInterval: muDataRechreshDela, repeats: true) { timer in
                /*self.device?.requestMessage(msgStr: BATT_MESSAGE.GET_CHARGE_LEVEL)
                self.device?.requestMessage(msgStr: BATT_MESSAGE.GET_VOLTAGE_LEVELE)*/
                self.device?.UpdateMuDesignDynamicData()
            }
            timer1 = Timer.scheduledTimer(withTimeInterval: muDataRechreshHotDela, repeats: true) { timer in
                /*self.device?.requestMessage(msgStr: BATT_MESSAGE.GET_CHARGE_LEVEL)
                self.device?.requestMessage(msgStr: BATT_MESSAGE.GET_VOLTAGE_LEVELE)*/
                self.device?.UpdateMuDesignHighDynamicData()
            }
        }
    enum BleState: Int{
        case NotConnected = 0
        case DeviceConnected = 1
        case IsSyslorDevice = 2
        case DeviceNotFound = 3
        case StreamCallBack = 4

        var description : String{
            switch self {
            case .NotConnected:
                return "BLE Device not connected"
            case .DeviceConnected:
                return "BLE Device is connected"
            case .IsSyslorDevice:
                return "Syslor Box connected"
            case .DeviceNotFound:
                return "BLE Device is not found"
            case .StreamCallBack:
                return "BT Stream Data"
            }
        }
    }
    enum NtripState: Int{
        case NotConnected = 0
        case HostConnected = 1
        case HostAuthenticationOk = 2
        case AuthenticationError = 3
        case AntennaError = 4
        case InternetError = 5
        case InternetConnected = 6
        
        var description : String{
            switch self {
            case .NotConnected:
                return "Not connected"
            case .HostConnected:
                return "Host connected, stream created"
            case .HostAuthenticationOk:
                return "Host authentication succeded"
            case .AuthenticationError:
                return "User or password incorrect"
            case .AntennaError:
                return "Antenna Error"
            case .InternetError:
                return "Internet Error"
            case .InternetConnected:
                return "Internet connected"
            }
        }
    }
}
// ==============================================================================================
// ==============================================================================================
@available(iOS 12.0, *)
extension ServiceGnss : MuSyslorMessageDelegate{
    func updateFirmewareUpdateProgress(progress: Int) {
           log(msg: "Update progress : " + String(progress) + "%")
           delegate?.didUpdateProgressUpdateFirmeware(progress:progress)
       }
       
       func updateUbxMessage(message: String) {
           log(msg: "Ublox message : " + message)
           var result = nmeaParser.parseSentence(dataStr: message)
       }
       
       func updateMagneto(x: Float, y: Float, z: Float) {
           log(msg: "Magneto message : " + String(x)+" "+String(y)+" "+String(z))
       }
       
       func updateStatsOcc(occ: Int) {
           log(msg: "Occ message : " + String(occ))
       }
       
       func updateConfAnswer(ans: Bool) {
           log(msg: "Conf Answer message : " + String(ans))
       }
       
       func updateSleepIfInact(delaySec: Int) {
           log(msg: "Seep Inact message : " + String(delaySec))
       }
       
       func updateFirmStartAnswer(ans: Bool) {
           log(msg: "Firm answer message : " + String(ans))
       }
       
       func updateFirmEndAnswer(ans: Bool) {
           log(msg: "Firmeupdate end message : " + String(ans))
       }
    // =====================================================================
    func updateFirmVersion(version: String) {
        log(msg: "Version : "+version)
        muInfoData?.firm_version = version
        delegate?.didUpdateMuInfoData(data: self.muInfoData!)
        //labVersion.text = version;
    }
    // =====================================================================
    func updateCryptactive(value: Bool) {
        muInfoData?.crypt_active =  String(value)
        delegate?.didUpdateMuInfoData(data: self.muInfoData!)
        log(msg: "Crypt active : " + String(value))
    }
    // =====================================================================
    func updateCryptKey(key: String) {
        muInfoData?.crypt_key =  key
        delegate?.didUpdateMuInfoData(data: self.muInfoData!)
      log(msg: "Crypt key : " + key)
    }
    // =====================================================================
    func updateUartBaudeRate(value: Int) {
        log(msg: "UART baude rate key : " + String(value))
        //labUart.text = String(value)
        muInfoData?.uart_bauderate = value
        delegate?.didUpdateMuInfoData(data: self.muInfoData!)
    }
    // =====================================================================
    func updateStatsOcc(occ: String) {
        log(msg: "Stats occ : " + occ)
        muStatsData?.run_occ = Int(occ)!
        delegate?.didUpdateMuStatsData(data: self.muStatsData!)
    }
    // =====================================================================
    func updateStatsAccu(Acc: String) {
        log(msg: "Stats acc : " + Acc)
        muStatsData?.run_accu = Int(Acc)!
        delegate?.didUpdateMuStatsData(data: self.muStatsData!)
    }
    // =====================================================================
    func updateBattVoltage(val: Float) {
        //labBattVoltageValue.text = String(val)
        log(msg: "Battery voltage : " + String(val))
        muBatteryData?.voltage_level = val
        delegate?.didUpdateMuBatteryData(data: self.muBatteryData!)
    }
    // =====================================================================
    func updateBattCharge(val: Float) {
        //labBattChargeValue.text = String(val)
        log(msg: "Battery charge : " + String(val))
        muBatteryData?.charge_level = val
        delegate?.didUpdateMuBatteryData(data: self.muBatteryData!)
    }
    // =====================================================================
    func updateUnknownMessage(messgae: String) {
        log(msg: "Unknown message : " + messgae)
    }
    // =====================================================================
    func updateNmeaMessage(message: String) {
       // var result = nmeaParser.parseSentence(dataStr: message)
    }
    // =====================================================================
    func updateStatsMessage(occ : Int, accu:Int){
        log(msg: "Stats : " + "Occ:" + String(occ)+"/Accu:" + String(accu))
        muStatsData?.run_occ = occ
        muStatsData?.run_accu = accu
        delegate?.didUpdateMuStatsData(data: self.muStatsData!)
        //labStats.text = "Occ:" + String(occ)+"/Accu:" + String(accu)
    }
    
    //TODO
    func updatedConfAckAnswer(ackAnswer: Bool){
        log(msg: "ackAnswer : " + String(ackAnswer))
        muConfAckAnswer = ackAnswer
        delegate?.didUpdateMuConfAckAnswer(data: ackAnswer)
    }
    
    //TODO
    func updateMagnetoData(x_magneto : String, y_magneto:String, z_magneto:String){
        muDesignMagnetoData?.x_magneto = x_magneto
        muDesignMagnetoData?.x_magneto = y_magneto
        muDesignMagnetoData?.x_magneto = z_magneto
        delegate?.didUpdateMuMagnetoData(data: self.muDesignMagnetoData!)
        //labStats.text = "Occ:" + String(occ)+"/Accu:" + String(accu)
    }
}
// ==============================================================================================
// ==============================================================================================
@available(iOS 12.0, *)
extension ServiceGnss : NmeaParserDelegate{
    // =====================================================================
    func didUpdateGPSData(_sender: NmeaParser, data: NmeaParser.GPSData) {
        delegate?.didUpdateGPSData(data: data)
    }
    // =====================================================================
    func didParseGGASentence(_ sender: NmeaParser, data: GgaSentence.GGAData,message:String){
        log(msg: "Message GGA : " + message)
        if let ntrip = ntripClient {
            if self.mode != MODE_UBLOX && ntripClient.isNtripLogin {
            ntripClient.send(msg: message+"\r\n")
        }
        }
    }
    
    func errorParseGGASentence(_ sender: NmeaParser) {
        log(msg :"NTrip error antenna : Antenna error")
        //delegate?.didUpdateNtripState(state: NtripState.AntennaError, messsage: "Antenna error")
    }
    // =====================================================================
    // GNGSA
    // GNGGA
    // GNGST ?
    // GNVTG
}
// ==============================================================================================
// ==============================================================================================
extension ServiceGnss : NtripClientDelegate{
    func wasAuthenticate(msg: String) {
        log(msg: "Ntrip autheticate succeded : " + msg)
        delegate?.didUpdateNtripState(state: NtripState.HostAuthenticationOk, messsage: msg)
    }
    
    
    
    func wasDisconnected(msg: String) {
        log(msg: "Ntrip Disconnected : " + msg)
        delegate?.didUpdateNtripState(state: NtripState.NotConnected, messsage: msg)
        disconnect()
    }
    
    func wasConnected() {
        log(msg: "Ntrip connected")
        delegate?.didUpdateNtripState(state: NtripState.HostConnected, messsage: "")
        
    }
    
    func hasInternet(value: Bool) {
        if (value == true) {
            delegate?.didUpdateNtripState(state: .InternetConnected, messsage: "")
        } else {
            delegate?.didUpdateNtripState(state: .InternetError, messsage: "")
        }
    }
    
    func wasNotConnected(errMeg: String, status:Int) {
        log(msg: "Ntrip connection error : " + errMeg)
        switch status {
        case 401:
            delegate?.didUpdateNtripState(state: NtripState.AuthenticationError, messsage: errMeg)
        default:
            delegate?.didUpdateNtripState(state: NtripState.NotConnected, messsage: errMeg);
        }
        disconnect()
    }
    
    // =====================================================================
    func didNtripReceiveData(buffer: [UInt8], count: Int) {
        let data = Array(buffer.prefix(count))
        log(msg: "receive ntrip data -> BLE : ")
        device?.requestMessage(msgUInt8: data)
    }
}
// ==============================================================================================
// ==============================================================================================
extension ServiceGnss: BTDeviceDelegate {
    func deviceNotSyslor() {
        log(msg: "NotSyslor device")
        delegate?.didUpdateBleState(state: BleState.NotConnected, messsage: "Not Syslor box")
    }
    
    // =====================================================================
    func deviceSerialChanged(value: String) {
        log(msg: "Ble device serial changed")
        //delegate?.didUpdateBleState(state: BleState.IsSyslorDevice, messsage: "")
    }
    // =====================================================================
    func deviceConnected() {
        log(msg: "Ble device connected")
        delegate?.didUpdateBleState(state: BleState.DeviceConnected, messsage: "")
        //viewState = .connected
    }
    
    func streamCallBack() {
        delegate?.didUpdateBleState(state: BleState.StreamCallBack, messsage: "")
        //viewState = .connected
    }
    // =====================================================================
    func deviceDisconnected() {
        //viewState = .disconnected
        log(msg: "Ble device disconnected")
        delegate?.didUpdateBleState(state: BleState.NotConnected, messsage: "")
        disconnect()
    }
    // =====================================================================
    func deviceReady() {
          //viewState = .ready
          log(msg: "Ble Syslor device ready")
          delegate?.didUpdateBleState(state: BleState.IsSyslorDevice, messsage: "")
        if (mode == MODE_UBLOX) {
            device?.requestMessage(msgUInt8: res)
            getMuBoxData()
        }
          if mode == MODE_GNSS{
              getMuBoxData()
          }
          if mode == MODE_RESETDEFAULTSETTINGS{
              device?.ResetSettings(key:newkey)
              DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + .seconds(7)){
                  self.disconnect()
              }
              
          }
          if mode == MODE_UPDATEFIRMEWARE{
              device?.WriteFirmware()
              /*DispatchQueue.global(qos: .utility).async { [unowned self] in
                  var updateFirmeware:MuDesignUpdateFimeware=MuDesignUpdateFimeware()
                  updateFirmeware.loadFimewareFile()
                  var sz:Int=updateFirmeware.bytesData.count
                  var begin_msg = FIRM_MESSAGE.getMessageGetVersion(size: sz)
                  print(begin_msg)
                  device?.requestMessage(msgStr: begin_msg)
                  sleep(5)
                  var cnt=updateFirmeware.getBlocksCount()
                  for i in 0..<cnt{
                      var dt = updateFirmeware.getBlock(num: i)
                      var aryData = Array(dt)
                          
                      self.device?.requestMessage(msgUInt8: aryData)
                      
                      //print("------->",i,"/",cnt)
                      usleep(50000)
                  }
              }
              */
          }
      }
}
// ==============================================================================================
// ==============================================================================================
protocol ServiceGNSSDelegate: AnyObject{
    func didUpdateGPSData(data: NmeaParser.GPSData)
    func didUpdateMuBatteryData(data: ServiceGnss.MuBatteryData)
    func didUpdateMuInfoData(data: ServiceGnss.MuInfoData)
    func didUpdateBleState(state:ServiceGnss.BleState, messsage: String)
    func didUpdateNtripState(state:ServiceGnss.NtripState, messsage: String)
    func didUpdateMuStatsData(data: ServiceGnss.MuStatsData)
    func didUpdateMuMagnetoData(data: ServiceGnss.MuDesignMagnetoData)
    func didUpdateMuConfAckAnswer(data : Bool)
    func didUpdateProgressUpdateFirmeware(progress:Int)
}

extension ServiceGnss: BTManagerDelegate {
    func didMacAdressNotFound() {
        print("Device not found")
        self.delegate?.didUpdateBleState(state: .DeviceNotFound, messsage: "Not found")
    }
    
    func didMacAdressFound(device: BTDevice) {
        self.device = device
        print("device.detail :",device.detail)
        self.device?.connect()
        print("didMacAdressFound ")
    }
    
    // =====================================================================
    func didChangeState(state: CBManagerState) {
        //devices = manager.devices
        //updateStatusLabel(msg:"")
    }
    // =====================================================================
    func didDiscover(device: BTDevice) {
        //devices = manager.devices
    }
    // =====================================================================
    func didEnableScan(on: Bool) {
        //updateStatusLabel(msg:"")
        if !on{
            print("Stop scan")
        }
    }
}
    extension ServiceGnss:MuSyslorMessageUbxDelegate{
        func MuSyslorMessageUbxDelegate(msgs: [String : String]) {
            for msg in msgs {
                print("Message UBX : ",msg.key," : ",msg.value)
            }
        }
    }
