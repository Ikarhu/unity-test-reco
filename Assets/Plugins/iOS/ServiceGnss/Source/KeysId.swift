//
//  KeysId.swift
//  ServiceGNSS
//
//  Created by ivirtual on 21/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation
// =====================================================================
class KeysId {
    var key:UInt32 = 0;
    var length:Int = 0;

    init( key:UInt32,  messagelength:Int) {
        self.key = key;
        self.length = messagelength;
    }

    func getKey()->UInt32 {
        return key;
    }

    func getLength()->Int {
        return length;
    }
}
