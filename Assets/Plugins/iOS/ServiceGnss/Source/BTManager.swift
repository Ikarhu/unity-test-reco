//
// Service GN3Syslor for connecting GNSS device to iOS smartphone
//

import Foundation
import CoreBluetooth

// BLE Manager delegate
protocol BTManagerDelegate: class {
    func didDiscover(device: BTDevice)
    func didMacAdressFound(device: BTDevice)
    func didMacAdressNotFound()
    func didEnableScan(on: Bool)
    func didChangeState(state: CBManagerState)
}
// Class : BLE Manager
class BTManager: NSObject {
    let debug:Bool = true
    let TAG="     ¡¡ BLE MANAGER ¡¡ "
    var macAdressToSearch = ""
    var macAdreseeFound = false
    var discoverSyslorUnknownBox=false
    // =====================================================================
    func log(msg: String){
        if debug{
            print(TAG,msg)
        }
    }
    private let manager: CBCentralManager
    private var seenDevices: [UUID:BTDevice] = [:]
    private var seenSyslorUnknownDevices: [UUID:BTDevice] = [:]
    var devices: [BTDevice] {
        return Array(seenDevices.values)
    }
    
    // Attr : Stat
    var state: CBManagerState {
        return manager.state
    }
    weak var delegate: BTManagerDelegate?
    
    // Attr : Scanning
    var scanning: Bool = false {
        didSet {
            if (oldValue == scanning) {
                return
            }
            log(msg: "Manager: scan state set to \(scanning)")
            if (scanning) {
                manager.scanForPeripherals(withServices: [BTUUIDs.GnssService], options: nil)//[BTUUIDs.blinkService]
            } else {
                manager.stopScan()
            }
            delegate?.didEnableScan(on: scanning)
        }
    }
    // Mbre : Init
    override init() {
        manager = CBCentralManager(delegate: nil, queue: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true])
        super.init()
        manager.delegate = self
    }
    // Mbre : Init
    init(timeout: Int) {
        manager = CBCentralManager(delegate: nil, queue: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true])
        super.init()
        manager.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(timeout) ) {
            self.scanning = false
        }
    }
    // Mbre : Init
    init(timeout: Int, mac:String) {
        macAdressToSearch = mac
        macAdreseeFound = false
        manager = CBCentralManager(delegate: nil, queue: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey:true])
        super.init()
        log(msg:"Size seenDevices = " + String(self.seenDevices.count))
        manager.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(timeout) ) {
            self.log(msg:"Size seenDevices = " + String(self.seenDevices.count))
            if (!self.macAdreseeFound){
                if self.seenSyslorUnknownDevices.count>0{
                    self.discoverSyslorUnknownBox=true
                    
                    self.seenSyslorUnknownDevices.first?.value.connect()
                    //self.seenSyslorUnknownDevices[self.seenSyslorUnknownDevices.first!.key] = nil
                    
                }else{
                    self.scanning = false
                    self.delegate?.didMacAdressNotFound()
                }
            }
    }
    }
}

// Impl : Central Manager Delegate
extension BTManager: CBCentralManagerDelegate {
    
    // State changed
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        log(msg: "Manager: state \(manager.state)")
        switch manager.state {
        case .unknown,.resetting:
            break
        case .poweredOn:
            scanning = true
        default:
            scanning = false
            seenDevices = [:]
        }
        delegate?.didChangeState(state: manager.state)
    }
    
    // didConnect callback
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + .seconds(3)){
            if self.discoverSyslorUnknownBox{
            
            self.seenSyslorUnknownDevices[peripheral.identifier]?.disconnect()
            self.seenSyslorUnknownDevices[peripheral.identifier] = nil
            
            if self.seenSyslorUnknownDevices.count>0{
                //self.discoverSyslorUnknownBox=true
                
                self.seenSyslorUnknownDevices.first?.value.connect()
                
                //self.discoverSyslorUnknownBox=false
            }else{
                self.discoverSyslorUnknownBox=false
                self.seenDevices =  [:]
                self.seenSyslorUnknownDevices = [:]
                self.scanning = false
                self.scanning = true
                //self.delegate?.didMacAdressNotFound()
            }
        }else{
            self.seenDevices[peripheral.identifier]?.streamCallback()
        }
        }
    }
    
    // didFailToConnect callback
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        log(msg: "Manager: failed connect \(peripheral), error \(String(describing: error))")
        seenDevices[peripheral.identifier]?.errorCallback(error: error)
    }
    
    // didDisconnectPeripheral callback
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        let msgError:String? = String(describing:error)
        log(msg: "Manager: disconnected \(peripheral), error \(msgError)")
        seenDevices[peripheral.identifier]?.disconnectedCallback()
    }
    
    // didDiscover callback
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        for i in advertisementData{
            print("===========!!!!",i)
        }
        if seenDevices[peripheral.identifier] == nil {
            log(msg: "Manager: discovered \(peripheral), rssi: \(RSSI)")
            let device = BTDevice(peripheral: peripheral, manager: manager)
            seenDevices[peripheral.identifier] = device
            delegate?.didDiscover(device: device)
            
            print(device.name,self.macAdressToSearch.uppercased())
            if device.name.contains("Syslor GNSS") &&  device.name.components(separatedBy: ":").count==6{
                self.log(msg: "Manager: connected \(peripheral)")
                                self.log(msg: peripheral.name ?? "??")
                print("OK Syslor mac adresse : ", device.name.suffix(17))
                if device.name.suffix(17).uppercased() == self.macAdressToSearch.uppercased(){
                    self.seenDevices[peripheral.identifier]?.connectedCallback()
                    print("Mac Found ! OK Syslor mac adresse : ", device.name.suffix(17))

                    macAdreseeFound = true
                    delegate?.didMacAdressFound(device: device)
                    scanning = false
                }
            }else{
                if device.name.contains("Syslor GNSS"){
                    seenDevices[peripheral.identifier]=nil
                    seenSyslorUnknownDevices[peripheral.identifier] = device
                    print ("device name = ", device.name)
                }
            }
        } else {
            log(msg: "Manager: seen again \(peripheral)")
        }
    }
    
}
