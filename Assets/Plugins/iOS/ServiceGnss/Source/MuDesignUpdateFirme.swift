//
//  MuDesignUpdateFirme.swift
//  ServiceGNSS
//
//  Created by ivirtual on 02/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation

/** *****************************************************************************
 *
 */
public class MuDesignUpdateFimeware {
    var bytesData : Data=Data(count:0)  ;
    var blocksCount : Int  = 0;
    var TAG : String  = "MuDesignUpdateFimeware";
    let BLOCK_SIZE : Int = 256;
    /** *************************************************************************
     *
     * @param activity
     */
    func loadFimewareFile(){
            //Instantiate the file object
            if let fileURL = Bundle.main.url(forResource: "espgnssblegateway", withExtension: "bin") {
                // we found the file in our bundle!
                print("OK espgnssblegateway.bin found : ",fileURL)
                if let fileContents = try? Data(contentsOf: fileURL) {
                    // we loaded the file into a string!
                    print("file loaded",fileContents.count)
                    
                    bytesData = fileContents;
                    if ((bytesData.count + BLOCK_SIZE)==0) {
                        blocksCount =  bytesData.count/BLOCK_SIZE;
                    }else {
                        var cnt:Int=bytesData.count;
                        var tmp:Double = Double(cnt)/Double(BLOCK_SIZE);
                        blocksCount = Int(tmp+0.5);
                    }
                    /*for i in 0..<blocksCount{
                        //byte[] data =  getBlock(i);
                        print(i)
                    }*/
                    //Log.d(TAG, "End : "+getBlocksCount());
                }
            }else{
                print("KO espgnssblegateway.bin NOT found")
            }
        
            
      
    }
    /** *************************************************************************
     *
     * @return
     */
    
    func  getBlocksCount()->Int{
        return blocksCount ;
    }
    /** *************************************************************************
     *
     * @param num
     * @return
     */
    func getBlock( num:Int)->Data{
        if num>=blocksCount{
            return Data();
        }
        var lim_max:Int = ((num+1)*(BLOCK_SIZE) );
        if(num == blocksCount-1){
            lim_max = self.bytesData.count;
        }
        var res:Data =  bytesData.subdata(in:num*BLOCK_SIZE..<lim_max);
        if ( blocksCount-num<20){
            print(TAG, "size ",num,": ", num*BLOCK_SIZE,"->",lim_max,res.count);
            print(TAG,res[0], res[res.count-1]);
        }
        return res;
    }
}

