//
//  UBXValGet.swift
//  ServiceGNSS
//
//  Created by ivirtual on 22/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation

/**
 *
 */
public class UBXValGet {
    var payload:UBXVector
    var msg:UBXMessage


    /**
     *
     * @param value
     */
    init(value:UBXMessage.STORE) {
        payload = UBXVector();
        msg = UBXMessage(msg: payload);
        msg.InitValget(value:value.get());
    }

    /**
     *
     */
    func Rate_Meas() {//Nominal time between GNSS measurements
        payload.add(value: UBXKey.Instance.RATE_MEAS);
    }

    /**
     *
     */
    func Rate_Nav() {//Nominal time between GNSS measurements
        payload.add(value: UBXKey.Instance.RATE_NAV);
    }

    /**
     *
     */
    func DynModel() {//Ratio of number of measurements to number of navigation solutions
        payload.add(value: UBXKey.Instance.DYNMODEL);
    }

    /**
     *
     */
    func MinElev() {//Ratio of number of measurements to number of navigation solutions
        payload.add(value: UBXKey.Instance.MINELEV);
    }

    /**
     *
     */
    func TimeOut() {//Ratio of number of measurements to number of navigation solutions
        payload.add(value: UBXKey.Instance.TIMEOUT);
    }

    /**
     *
     */
    func NMEAHighPrecision() {//Ratio of number of measurements to number of navigation solutions
        payload.add(value: UBXKey.Instance.NMEAHIGHTPRECISION);
    }

    /**
     *
     * @param keys
     */
    func OutMessage( keys:[KeysId]) { //Ratio of number of measurements to number of navigation solutions
        for key in keys {
            payload.add(value: key);
        }
    }

    /**
     *
     */
    func Uart2Enabled() {
        payload.add(value: UBXKey.Instance.UART2_ENABLED);
    }

    /**
     *
     */
    func Uart1Bauderate() {
        payload.add(value: UBXKey.Instance.UART1_BAUDERATE);
    }

    /**
     *
     * @param keys
     */
    func Uart1InProt(keys:[KeysId]) { //Ratio of number of measurements to number of navigation solutions
        for key in keys {
            payload.add(value: key);
        }
    }

    /**
     *
     * @param keys
     */
    func Uart1OutProt(keys:[KeysId]) { //Ratio of number of measurements to number of navigation solutions

        for key in keys {
            payload.add(value: key);
        }
    }

    /**
     *
     * @return
     */
    func getByte()->[UInt8] {
        return msg.getByte();
    }

    /**
     *
     * @return
     */
   func getHex()->String {
        return msg.getHex();
    }
}

