//
//  UBXMessage.swift
//  ServiceGNSS
//
//  Created by ivirtual on 21/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation

public class UBXMessage {
    static var uBloxPrefix1B:UInt8 =  0xB5;
    static var uBloxPrefix2B:UInt8 = 0x62;

    var msgBuffer:UBXVector;
    
    
   

    init( msg:UBXVector) {
        self.msgBuffer = msg;
    }

    init() {
        msgBuffer = UBXVector()
        //params = [:];
    }

   
    public static func Parse(msg:String)->[String: String] {
        let hexa_array = msg.split(separator: " ");
        return UBXParser(array: UbxTools.stringsArrayToBytesArray(array: hexa_array));
    }
    
    public static func Parse(byteBuffer:[UInt8])->[String:String] {
        return UBXParser(array: byteBuffer);
    }

     
    public static func getMonVersion()->[UInt8] {
        return [0xB5,  0x62,  UBXClassId.UBX_MON_VER.getClsId(),  UBXClassId.UBX_MON_VER.getMsgId(),  0x00,  0x0E,  0x34];
    }
    
    
    public static func  UBXParser( array:[UInt8])->[String:String] {
        var ubxkey:UBXKey = UBXKey.Instance;
        var header:[UInt8];
        var payload:[UInt8];
        var payload_length:Int16;

        var params:[String:String] = [:];

        payload_length = UbxTools.bytesToShort(b1: array[5], b2: array[4]);
       
            // Check if UBX
        if array[0] == UBXMessage.uBloxPrefix1B && array[1] == UBXMessage.uBloxPrefix2B {
                // Case Valget
                if array[2] == UBXClassId.UBX_CFG_VALGET.getClsId() && array[3] == UBXClassId.UBX_CFG_VALGET.getMsgId() {
                    payload_length = payload_length - 4;
                    
                    let payloadSubArray = array[10...10 + Int(payload_length)]
                    payload = Array<UInt8>(payloadSubArray)//Arrays.copyOfRange(array, 10, 10 + payload_length);
                    
                    //for i in 0..<Int(payload_length){
                    var i=0
                    while i<Int(payload_length){
                        var key_id = UbxTools.bytesToInt(b1: payload[i + 3], b2: payload[i + 2], b3: payload[i + 1], b4: payload[i]);
                        var found:Bool=false
                        for  value in ubxkey.listKeyId.keys {
                            var field:KeysId = ubxkey.listKeyId[value]!;
                            if key_id == field.getKey(){
                                
                                /*ByteBuffer poll_msg = ByteBuffer.allocate(field.getLength());
                                
                                for (int j = 0; j < field.getLength(); j++) {
                                    poll_msg.put((byte) payload[i + 3 + field.getLength() - j]);
                                }*/
                                // ???? ordre des bytes
                                if field.getLength() == 2{
                                    //params.put(value, String.valueOf(ByteBuffer.wrap(poll_msg.array()).getShort()));
                                    params[value] = String(UbxTools.bytesToShort(
                                        b1: (payload[i + 3 + field.getLength() - 0]),
                                        b2: (payload[i + 3 + field.getLength() - 1])))
                                }
                                if field.getLength() == 4 {
                                    //params.put(value, String.valueOf(ByteBuffer.wrap(poll_msg.array()).getInt()));
                                    params[value] = String(UbxTools.bytesToInt(
                                                            b1: (payload[i + 3 + field.getLength() - 0]),
                                                            b2: (payload[i + 3 + field.getLength() - 1]),
                                                            b3: (payload[i + 3 + field.getLength() - 2]),
                                                            b4: (payload[i + 3 + field.getLength() - 3])))
                                    //(b1: (payload[i + 3 + field.getLength() - 0]), b2: (UInt8payload[i + 3 + field.getLength() - 1]))
                                }
                                if field.getLength() == 1 {
                                    //params.put(value, String.valueOf(poll_msg.get(0)));
                                    params[value] = String(payload[i + 3 + field.getLength() - 0])
                                }
                                i = i + 3 + Int(field.getLength());
                                found = true
                                break;
                            }
                            //if !found{
                               
                            //}
                        }
                        i=i+1
                    }
                    // Case Mon version
                } else if (array[2] == UBXClassId.UBX_MON_VER.getClsId() && array[3] == UBXClassId.UBX_MON_VER.getMsgId()) {
                    let payloadSubArray = array[6...6 + Int(payload_length)]
                    payload = Array<UInt8>(payloadSubArray)
                    
                    //payload = Arrays.copyOfRange(array, 6, 6 + payload_length);
                    let swVersion = removeLast0(ar_: Array<UInt8>(payload[0..<30]))//Arrays.copyOfRange(payload, 0, 30);
                    let hwVersion = removeLast0(ar_: Array<UInt8>(payload[30..<40]))//Arrays.copyOfRange(payload, 30, 40);
                    let romBase = removeLast0(ar_: Array<UInt8>(payload[40..<69]))//Arrays.copyOfRange(payload, 40, 69);
                    let fwVersion = removeLast0(ar_:Array<UInt8>(payload[70..<99]))//Arrays.copyOfRange(payload, 70, 99);
                    let protVersion = removeLast0(ar_: Array<UInt8>(payload[100..<129]))//Arrays.copyOfRange(payload, 100, 129);
                    
                    params["SW_VERSION"] = String(bytes:swVersion,encoding: .utf8);//
                    params["HW_VERSION"] = String(bytes:hwVersion,encoding: .utf8)//, new String(hwVersion));
                    params["ROM_BASE"] = String(bytes:romBase,encoding: .utf8);//, new String(romBase));
                    params["FW_VERSION"] = String(bytes:fwVersion,encoding: .utf8);//, new String(fwVersion));
                    params["PROT_VERSION"] = String(bytes:protVersion,encoding: .utf8);//, new String(protVersion));
                }
                // Case ACK
                else if (array[2] == UBXClassId.UBX_ACK_ACK.clsId && array[3] == UBXClassId.UBX_ACK_ACK.msgId) {
                    //payload = Arrays.copyOfRange(array, 6, 6 + payload_length);
                    payload = Array<UInt8>(array[6...(6 + Int(payload_length))])
                    params["UBX_ACK_ACK"] = "UNKNOW"
                    for classid in UBXClassId.values() {
                        if classid.value.clsId == payload[0] && classid.value.msgId == payload[1] {
                            //params.put(UBXClassId.UBX_ACK_ACK.name(), classid.name());
                            params["UBX_ACK_ACK"] = classid.key
                            break
                        }
                    }
                }
                // Case NACK
                else if (array[2] == UBXClassId.UBX_ACK_NACK.clsId && array[3] == UBXClassId.UBX_ACK_NACK.msgId) {
                    //payload = Arrays.copyOfRange(array, 6, 6 + payload_length);
                    payload = Array<UInt8>(array[6...(6 + Int(payload_length))])
                    params["UBX_ACK_NACK"] = "UNKNOW"
                    for classid in UBXClassId.values() {
                        if classid.value.clsId == payload[0] && classid.value.msgId == payload[1] {
                            //params.put(UBXClassId.UBX_ACK_ACK.name(), classid.name());
                            params["UBX_ACK_NACK"] = classid.key
                            break;
                        }
                    }
                    /*for (UBXClassId classid : UBXClassId.values()) {
                        if (classid.clsId == payload[0] && classid.msgId == payload[1]) {
                            params.put(UBXClassId.UBX_ACK_NACK.name(), classid.name());
                        } else {
                            params.put(UBXClassId.UBX_ACK_NACK.name(), "UNKNOW");
                        }
                    }*/
                }
            } else {
                print("Not an UBX Message");
            }
       
        return params;
    }

    public static func removeLast0(ar_:Array<UInt8>)->Array<UInt8>{
        //var Array<UInt8> = Array<UInt8>()
        var ar = ar_
        while (ar.count>0){
            if(ar.last==0){
            ar.remove(at: ar.count-1)
            }else{
                break
            }
        }
        return ar;
    }

  
    func InitValset( value:UInt8) {
        msgBuffer.addU1(i: UBXMessage.uBloxPrefix1B);
        msgBuffer.addU1(i: UBXMessage.uBloxPrefix2B);
        msgBuffer.addU1(i: UBXClassId.UBX_CFG_VALSET.getClsId()); // CFG
        msgBuffer.addU1(i: UBXClassId.UBX_CFG_VALSET.getMsgId()); // Valset
        msgBuffer.addU1(i: 00); // version
        msgBuffer.addU1(i: value); // ram/bbr/flash
        msgBuffer.addU2(i: 0); //reserved
    }

   
    func InitValget( value:UInt8) {
        msgBuffer.addU1(i: UBXMessage.uBloxPrefix1B);
        msgBuffer.addU1(i: UBXMessage.uBloxPrefix2B);
        msgBuffer.addU1(i: UBXClassId.UBX_CFG_VALGET.getClsId()); // CFG
        msgBuffer.addU1(i: UBXClassId.UBX_CFG_VALGET.getMsgId()); // Valset
        msgBuffer.addU1(i: 00); // version
        msgBuffer.addU1(i: value); // ram/bbr/flash
        msgBuffer.addU2(i: 0); //reserved
    }

    
    func checkSum() {
        var CK_A:Int;
        var CK_B:Int;
        CK_A = 0;
        CK_B = 0;
        for i in 2..<msgBuffer.size() {
            CK_A = CK_A + Int(msgBuffer.data[i]);
            CK_B = CK_B + CK_A;
        }
        msgBuffer.addU1(i: UInt8(CK_A & 0xFF));
        msgBuffer.addU1(i: UInt8(CK_B & 0xFF));
    }

    
    func length() {
        var size:UInt16 = UInt16( msgBuffer.size() - 4);
        msgBuffer.addU2(position: 4, i: size);
    }

    
    func getByte()->[UInt8] {
        length();
        checkSum();

        var bytes:[UInt8] = [UInt8](repeating: 0, count: msgBuffer.size());
        
        for i in 0..<msgBuffer.size() {
            bytes[i] = (msgBuffer.data[i] & 0xFF);
        }
        msgBuffer.data.remove(at: msgBuffer.data.count-1);
        msgBuffer.data.remove(at: msgBuffer.data.count-1);
        msgBuffer.data.remove(at: 4);
        msgBuffer.data.remove(at: 5);
        return bytes;
    }

   
    func getHex()->String {
        /*StringBuilder result = new StringBuilder();
        byte[] bytes = getByte();
        for (byte temp : bytes) {
            result.append(String.format("%02x ", temp));
        }
        String hex = result.toString();
        hex = hex.substring(0, hex.length() - 1);
        return hex;*/
        return getByte().bytesToHex(spacing: " ")
    }

    public static func GetUbxMessagesPos(array:[UInt8]) ->[[Int]]{
        var result:[[Int]] = [];

        if(array.count<2){
            return  result;
        }

        for i in 0..<array.count{
                if(array[i]==uBloxPrefix1B && array[i+1]==uBloxPrefix2B ){
                    var item:[Int] = [];
                    item.append(i);

                    if(i+5<array.count){
                        let payload_length = UbxTools.bytesToShort( b1: array[i+5], b2: array[i+4]);

                        if(i+5+Int(payload_length)+2<array.count){
                            item.append(i+5+Int(payload_length)+2);
                            result.append(item);
                        }
                        else{
                            result.append(item);
                            break;
                        }
                    }else{
                        result.append(item);
                        break;
                    }
                }
            }
            return result;
        }

    public class STORE {
        static var FLASH:STORE=STORE(i: 4)
        static var BBR:STORE=STORE(i:2)
        static var RAM:STORE=STORE(i:1)
        static var ALL:STORE=STORE(i:7)
        
        var val:UInt8

        init( i:UInt8) {
            val = i;
        }

        func get()->UInt8 {
            return val;
        }
    }

   
    public class  DYNMODEL {
        static var PORTABLE:DYNMODEL=DYNMODEL(i: 0)
        static var STATIONARY:DYNMODEL=DYNMODEL(i:2)
        static var  PEDESTRIAN:DYNMODEL=DYNMODEL(i:3)
        static var AUTOMOTIVE:DYNMODEL=DYNMODEL(i:4)
        static var  SEA:DYNMODEL=DYNMODEL(i:5)
        static var  AIRBORNE_1G:DYNMODEL=DYNMODEL(i:6)
        static var AIRBORNE_2G:DYNMODEL=DYNMODEL(i:7)
        static var AIRBORNE_3G:DYNMODEL=DYNMODEL(i:8)
        static var WRIST_WORN_WATCH:DYNMODEL=DYNMODEL(i:9)
        static var BIKE:DYNMODEL=DYNMODEL(i:10)

        var i:UInt8
        
        init(i:UInt8) {
            self.i=i
        }

        func  getI()->UInt8 {
            return i;
        }
    }

    
    public class UBXClassId {

        static var UBX_ACK_ACK:UBXClassId = UBXClassId(clsId: 0x05, msgId: 0x01)
        static var UBX_ACK_NACK:UBXClassId = UBXClassId(clsId:0x05, msgId: 0x00)
        static var UBX_MON_VER:UBXClassId = UBXClassId(clsId:0x0A, msgId: 0x04)
        static var UBX_CFG_VALGET:UBXClassId = UBXClassId(clsId:0x06, msgId: 0x8B)
        static var UBX_CFG_VALSET:UBXClassId = UBXClassId(clsId:0x06, msgId: 0x8A)

        var clsId:UInt8;
        var msgId:UInt8;

        init(clsId:UInt8,  msgId:UInt8) {
            self.clsId = clsId;
            self.msgId = msgId;
        }

        func  getClsId()->UInt8 {
            return clsId;
        }

        func getMsgId()->UInt8 {
            return msgId;
        }
        public static func values()->[String:UBXClassId]{
        return [
            "UBX_ACK_ACK":UBXClassId.UBX_ACK_ACK,
            "UBX_ACK_NACK":UBXClassId.UBX_ACK_NACK,
            "UBX_MON_VER":UBXClassId.UBX_MON_VER,
            "UBX_CFG_VALGET":UBXClassId.UBX_CFG_VALGET,
            "UBX_CFG_VALSET":UBXClassId.UBX_CFG_VALSET]
        }
    }
}















