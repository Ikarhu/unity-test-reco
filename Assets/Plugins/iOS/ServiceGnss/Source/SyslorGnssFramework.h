//
//  SyslorGnssFramework.h
//  SyslorGnssFramework
//
//  Created by Vaxelaire Lucas on 18/02/2021.
//

#import <Foundation/Foundation.h>

//! Project version number for SyslorGnssFramework.
FOUNDATION_EXPORT double SyslorGnssFrameworkVersionNumber;

//! Project version string for SyslorGnssFramework.
FOUNDATION_EXPORT const unsigned char SyslorGnssFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SyslorGnssFramework/PublicHeader.h>

@interface SyslorGnssFramework : NSObject
@end

#ifdef __cplusplus
extern "C" {
#endif
    
    void LoadPhoto(void);
    void InitIos(void);
    char *GetMsgBleIos(void);
    char *GetMsgNtipIos(void);
    char *GetAccuracyIos(void);
    char *GetRotationAngleStatusIos(void);
    char *GetRotationAngleIos(void);
    char *GetLocalisationIos(void);
    char *GetFixModeIos(void);
    
    char *GetLogDataIos(void);

char *GetMuDesignInfoDataIos(void);
char *GetMuDesignBatteryDataIos(void);
char *GetMuDesignMagnetoDataIos(void);
char *GetMuDesignStatsDataIos(void);
bool GetMuDesignConfAckAnswerIos(void);

    void ConnectActIos(void);
    void DisconnectActIos(void);

    void SetMacAdressIos(const char* string);
    void SetServerIpNtripIos(const char* string);
    void SetServerPortIos(const char* string);
    void SetUsernameIos(const char* string);
    void SetPasswordIos(const char* string);
    void SetMountPointIos(const char* string);
    void SetScenario(int scenario);

    #ifdef __cplusplus
    }
    #endif

