import Foundation
import UIKit
import CoreBluetooth
import SocketSwift
import Network
import SystemConfiguration

// ==============================================================================================
// ==============================================================================================
@available(iOS 12.0, *)
class NtripClient{
    let monitor = NWPathMonitor()
    let debug:Bool = true
    let TAG="     ¡¡ NTRIP ¡¡ "
    // =====================================================================
    func log(msg: String){
        if debug{
            print(TAG,msg)
        }
    }
    var isSocketConnected:Bool = false
    var isNtripLogin=false
    var must_disconnect:Bool=false
    var url:String=""
    var port:__uint16_t!
    var user:String=""
    var pass:String=""
    var mountPoint:String=""
    var client:Socket!
    
    weak var delegate: NtripClientDelegate?
    // =====================================================================
    init(url:String, port:__uint16_t, user:String, pass:String, mountPoint:String){
        self.url = url
        self.port = port
        self.user = user
        self.pass = pass
        self.mountPoint = mountPoint
        
    }
    // =====================================================================
    
        public static func isConnectedToNetwork() -> Bool {
            var zeroAddress = sockaddr_in()
            zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
            zeroAddress.sin_family = sa_family_t(AF_INET)
     
            let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
                $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                    SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
                }
            }
     
            var flags = SCNetworkReachabilityFlags()
            if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
                return false
            }
            let isReachable = flags.contains(.reachable)
            let needsConnection = flags.contains(.connectionRequired)
            return (isReachable && !needsConnection)
        }
    
    public func start(){
        if (NtripClient.isConnectedToNetwork() == true) {
        self.log(msg: "is internet connected ? + " + String(NtripClient.isConnectedToNetwork()))
            self.delegate?.hasInternet(value: true)
        self.log(msg:"ntrip initialize")
        self.client = try! Socket(.inet, type: .stream, protocol: .tcp) // create client socket
        try! self.client.set(option: Socket.BaseOption.receiveTimeout, TimeValue(seconds: 5, microseconds: 0))
        

            let addr = (try? self.client.addresses(for: self.url, port: self.port).first) ?? nil
            if (addr != nil && (try? self.client.connect(address: addr!)) != nil) { // connect to localhost:8090
                let msgRqst:String = getConnectionRequest(user: user, pass:pass, mountPoint: mountPoint)
                self.send(msg: msgRqst)
                self.delegate?.wasConnected()
                self.isSocketConnected = true
                self.receive()
            } else {
                self.delegate?.hasInternet(value: false)
            }
        } else {
            self.delegate?.hasInternet(value: false)
        }
//            let msgRqst:String = self.getConnectionRequest(user: self.user, pass:self.pass, mountPoint: self.mountPoint)
        }
    // =====================================================================
    func getConnectionRequest(user:String, pass:String, mountPoint:String)->String{
        var requestmsg:String  = "GET /" + mountPoint + " HTTP/1.0\r\n"
        requestmsg += "User-Agent: NTRIP SYSLOR/1.1.0\r\n"
        requestmsg += "Accept: */*\r\n"
        requestmsg += "Connection: close\r\n"
        requestmsg += "Authorization: Basic " + (user + ":" + pass).toBase64();
        requestmsg += "\r\n\r\n";
        //let helloBytes = ([UInt8])(requestmsg.utf8)
        return requestmsg
    }
    // =====================================================================
    func send(msg:String){
        let messageBytes = ([UInt8])(msg.utf8)
        log(msg: "Ntrip Write : " + msg.prefix(5))
        try! client.write(messageBytes) // sending bytes to the client
    }
    func waitForRead(){
        
        do {
            _ = try client.wait(for: .read, timeout: 10, retryOnInterrupt: false)
        }
        catch let error1 as Socket.Error {
            log(msg: "Error! waiting for read"+error1.description)
            disconnecting(restart: true, changestate: false)
            //delegate?.wasDisconnected(msg: "Error! can't read data from ntrip server")
        } catch {
            // Catch any other errors
            //isConnected = false
            log(msg: "Error! waiting for read" )
            
            disconnecting(restart: true, changestate: false)
            //delegate?.wasDisconnected(msg: "Error! waiting for read")
        }
    }
    // =====================================================================
    func receive(){
        var buffer = [UInt8](repeating: 0, count: 4096) // allocate buffer
        var i=0
        while isSocketConnected{
            log(msg: "waiting for read data")
            //waitForRead()
            do {
                var numberOfReadBytes = try client.read(&buffer, size: 4096)
                log(msg: "Receive data : " + String(numberOfReadBytes))
                parseNtripRceivedData(buffer: buffer, count: numberOfReadBytes)
            }
            catch let error1 as Socket.Error {
                log(msg: "Error! "+error1.description)
                disconnecting(restart: true, changestate: false)
                
                //must_disconnect = false
                //isConnected = false
                //delegate?.wasDisconnected(msg: "Error! ")//+error1.description
            } catch {
                // Catch any other errors
                //isConnected = false
                log(msg: "Error! reading")
                disconnecting(restart: true, changestate: false)
                //delegate?.wasDisconnected(msg: "Error! reading")
            }
            if(must_disconnect)
            {
                log(msg: "Must Disconnect")
                disconnecting(restart: false, changestate: false)
            }
            i += 1
            log(msg: "end receiving data")
        }
        close()
    }
    // =====================================================================
    func parseNtripRceivedData(buffer: [UInt8], count: Int){
        //delegate?.didNtripReceiveData(buffer: buffer, count: count)
        var buffer = Array(buffer.prefix(count))
        //buffer = buffer.filter { $0 < 127 }

        if !isNtripLogin{
            var i = 0
            while (i < buffer.count) {
                if (buffer[i] > 127) {
                    buffer.remove(at: i)
                }
                i += 1
            }
            var strBuffer = String(bytes: buffer, encoding: .utf8)
            if ((strBuffer != nil) && strBuffer!.contains( "ICY 200 OK")){
                isNtripLogin = true
                delegate?.wasAuthenticate(msg:"Authenticate succeded")
            }else
            if (strBuffer != nil) && strBuffer!.contains("401 Unauthorized"){
                delegate?.wasNotConnected(errMeg: "user or password unknown", status: 401)
                disconnecting(restart: false, changestate: false)
            }else
            if (strBuffer != nil) && strBuffer!.contains("SOURCETABLE 200 OK"){
                log(msg:"parseNTripReceivedData3")
                isNtripLogin = true
                delegate?.wasAuthenticate(msg:"Sourcetable 200 OK")
            }else
            if (strBuffer != nil) && strBuffer!.contains("\r\nENDSOURCETABLE"){
                log(msg:"parseNTripReceivedData4")
                delegate?.wasNotConnected(errMeg: "end receiving source table",status: 500)
                disconnecting(restart: false, changestate: true)
            }
        }else{
            delegate?.didNtripReceiveData(buffer: buffer, count: count)
        }
    }
    // =====================================================================
    func close(){
        log(msg: "Close")
        client.close()
    }
    // =====================================================================
    func disconnect(){
        must_disconnect=true
    }
    func disconnecting(restart:Bool, changestate: Bool){
        isSocketConnected = false
        isNtripLogin = false
        close()
        log(msg: "Disconnected")
        if (!must_disconnect && restart){
            DispatchQueue.global(qos: .utility).asyncAfter(deadline: .now() + .seconds(3)){
                self.start()
            }
            
        }else if (changestate){
                self.delegate?.wasDisconnected(msg: "disconnected")//+error1.description
        }
    }
}

// ==============================================================================================
// ==============================================================================================
private extension String {
    // =====================================================================
    var bytes: [Byte] {
        return [Byte](self.utf8)
    }
}
// =====================================================================
private extension Array where Element == Byte {
    // =====================================================================
    var string: String? {
        return String(bytes: self, encoding: .utf8)
    }
}
// ==============================================================================================
// ==============================================================================================
extension String {
    // =====================================================================
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    // =====================================================================
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

protocol NtripClientDelegate: AnyObject{
    func didNtripReceiveData( buffer: [UInt8],count: Int)
    func wasConnected()
    func wasNotConnected(errMeg:String, status: Int)
    func wasDisconnected(msg:String)
    func wasAuthenticate(msg:String)
    func hasInternet(value:Bool)
}
/*
extension NtripClientDelegate{
    func didNtripReceiveData( buffer: [UInt8],count: Int){}
}*/
