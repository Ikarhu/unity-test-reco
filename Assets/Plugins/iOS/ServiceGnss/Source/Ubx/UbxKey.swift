//
//  UBXKey.swift
//  ServiceGNSS
//
//  Created by ivirtual on 21/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation
// =====================================================================
class UBXKey {
    public static let Instance = UBXKey()

    public var listKeyId:[String:KeysId] = [:];

    // GNSS PARAMS
    var RATE_MEAS :KeysId =  KeysId(key:0x30210001, messagelength:2);
    var RATE_NAV :KeysId = KeysId(key:0x30210002, messagelength:2);
    var DYNMODEL :KeysId = KeysId(key:0x20110021, messagelength:1);
    var MINELEV :KeysId = KeysId(key:0x201100A4, messagelength:1);
    var TIMEOUT :KeysId = KeysId(key:0x201100C4, messagelength:1);

    // NMEA PARAMS
    var NMEAHIGHTPRECISION : KeysId = KeysId(key:0x10930006,messagelength: 1);

    // NMEA MSGOUT
    var MSGOUT_DTM_UART1 : KeysId = KeysId(key:0x209100A7,messagelength: 1);
    var MSGOUT_GBS_UART1 : KeysId = KeysId(key:0x209100DE, messagelength:1);
    var MSGOUT_GGA_UART1 : KeysId = KeysId(key:0x209100BB,messagelength:1);
    var MSGOUT_GLL_UART1 : KeysId = KeysId(key:0x209100CA,messagelength:1);
    var MSGOUT_GNS_UART1 : KeysId = KeysId(key:0x209100B6,messagelength:1);
    var MSGOUT_GRS_UART1 : KeysId = KeysId(key:0x209100CF,messagelength:1);
    var MSGOUT_GSA_UART1 : KeysId = KeysId(key:0x209100C0,messagelength:1);
    var MSGOUT_GST_UART1 : KeysId = KeysId(key:0x209100D4,messagelength:1);
    var MSGOUT_GSV_UART1 : KeysId = KeysId(key:0x209100C5,messagelength:1);
    var MSGOUT_RMC_UART1 : KeysId = KeysId(key:0x209100AC,messagelength:1);
    var MSGOUT_VLW_UART1 : KeysId = KeysId(key:0x209100E8,messagelength:1);
    var MSGOUT_VTG_UART1 : KeysId = KeysId(key:0x209100B1,messagelength:1);
    var MSGOUT_ZDA_UART1 : KeysId = KeysId(key:0x209100D9,messagelength:1);

    // UART2
    var UART2_ENABLED : KeysId = KeysId(key:0x10530005,messagelength:1);

    // UART1
    var UART1_BAUDERATE : KeysId = KeysId(key:0x40520001, messagelength:4);
    var UART1_INPROT_UBX : KeysId = KeysId(key:0x10730001,messagelength:1);
    var UART1_INPROT_RTCM2 : KeysId = KeysId(key:0x10730003,messagelength:1);
    var UART1_INPROT_RTCM3 : KeysId = KeysId(key:0x10730004,messagelength:1);
    var UART1_INPROT_NMEA : KeysId = KeysId(key:0x10730002,messagelength:1);
    var UART1_OUTPROT_UBX : KeysId = KeysId(key:0x10740001,messagelength:1);
    var UART1_OUTPROT_RTCM3 : KeysId = KeysId(key:0x10740004,messagelength:1);
    var UART1_OUTPROT_NMEA : KeysId = KeysId(key:0x10740002,messagelength:1);

    var SW_VERSION : KeysId = KeysId(key:0x00,messagelength:4);
    var HW_VERSION : KeysId = KeysId(key:0x00,messagelength:4);
    var ROM_BASE : KeysId = KeysId(key:0x00,messagelength:4);
    var FW_VERSION : KeysId = KeysId(key:0x00,messagelength:4);
    var PROT_VERSION : KeysId = KeysId(key:0x00,messagelength:4);

    init() {

        listKeyId["RATE_MEAS"] = RATE_MEAS
        listKeyId["RATE_NAV"] = RATE_NAV
        listKeyId["DYNMODEL"] = DYNMODEL
        listKeyId["MINELEV"] = MINELEV
        listKeyId["TIMEOUT"] = TIMEOUT
        listKeyId["MSGOUT_DTM_UART1"] = MSGOUT_DTM_UART1
        listKeyId["MSGOUT_GBS_UART1"] = MSGOUT_GBS_UART1
        listKeyId["MSGOUT_GGA_UART1"] = MSGOUT_GGA_UART1
        listKeyId["MSGOUT_GLL_UART1"] = MSGOUT_GLL_UART1
        listKeyId["MSGOUT_GNS_UART1"] = MSGOUT_GNS_UART1
        listKeyId["MSGOUT_GRS_UART1"] = MSGOUT_GRS_UART1
        listKeyId["MSGOUT_GSA_UART1"] = MSGOUT_GSA_UART1
        listKeyId["MSGOUT_GSV_UART1"] = MSGOUT_GSV_UART1
        listKeyId["MSGOUT_RMC_UART1"] = MSGOUT_RMC_UART1
        listKeyId["MSGOUT_VLW_UART1"] = MSGOUT_VLW_UART1
        listKeyId["MSGOUT_ZDA_UART1"] = MSGOUT_ZDA_UART1
        listKeyId["MSGOUT_VTG_UART1"] = MSGOUT_VTG_UART1
        listKeyId["UART2_ENABLED"] = UART2_ENABLED
        listKeyId["UART1_BAUDERATE"] = UART1_BAUDERATE
        listKeyId["UART1_INPROT_RTCM2"] = UART1_INPROT_RTCM2
        listKeyId["UART1_INPROT_RTCM3"] = UART1_INPROT_RTCM3
        listKeyId["UART1_INPROT_NMEA ="] = UART1_INPROT_NMEA
        listKeyId["UART1_OUTPROT_UBX"] = UART1_OUTPROT_UBX
        listKeyId["UART1_OUTPROT_RTCM3"] = UART1_OUTPROT_RTCM3
        listKeyId["UART1_OUTPROT_NMEA"] = UART1_OUTPROT_NMEA
        listKeyId["ROM_BASE"] = SW_VERSION
        listKeyId["HW_VERSION"] = HW_VERSION
        listKeyId["ROM_BASE"] = ROM_BASE
        listKeyId["FW_VERSION"] = FW_VERSION
        listKeyId["PROT_VERSION"] = PROT_VERSION
    }
}
