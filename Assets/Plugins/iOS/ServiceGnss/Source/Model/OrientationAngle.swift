//
//  SensorManager.swift
//  SyslorGnssFramework
//
//  Created by Baptiste on 18/08/2021.
//

import Foundation
import CoreMotion
import CoreLocation

var location = CLLocationManager()
var motion = CMMotionManager()
var devicemotion = CMDeviceMotion()


struct OrientationAngle : Codable {
    
    var azimuth:Double = 0
    var pitch:Double = 0
    var roll:Double = 0
    var angleAccuracy:Int = 0
    var screenOrientation:Int = 0
    
    var dataAccelerometers:[Float] = [0,0,0]
    var dataMagnetometers:[Float] = [0,0,0]
    
    var dataAccelerometersArray:[[Float]] = [[Float]]()
    var dataMagnetoArray:[[Float]] = [[Float]]()
    
    var meanAccelerometerReading:[Float] = [0,0,0]
    var meanMagnetometerReading:[Float] = [0,0,0]
    
    var rotationMatrix:[Float] = [0,0,0,0,0,0,0,0,0]
    
    private enum CodingKeys: String, CodingKey {
        case azimuth
        case pitch
        case roll
        case angleAccuracy
        case screenOrientation
        case meanAccelerometerReading
        case meanMagnetometerReading
        case rotationMatrix
    }
    
    mutating func initData()
    {
        location.startUpdatingHeading()
        location.startUpdatingLocation()
        if motion.isGyroAvailable {
            motion.startGyroUpdates()
            motion.gyroUpdateInterval = 1.0 / 60
        }
        if motion.isAccelerometerAvailable {
           motion.accelerometerUpdateInterval = 1.0 / 60.0  // 60 Hz
           motion.startAccelerometerUpdates()
        }
        if motion.isMagnetometerAvailable {
           motion.magnetometerUpdateInterval = 1.0 / 60.0  // 60 Hz
           motion.startMagnetometerUpdates()
        }
    }
    
    mutating func updateLocation() {

        location.requestAlwaysAuthorization()
        location.dismissHeadingCalibrationDisplay()

        if let data = location.heading {
            self.angleAccuracy = Int(data.headingAccuracy)
            print("angleaccuracy = " + String(self.angleAccuracy))
            self.azimuth = data.magneticHeading
            if angleAccuracy < 0 {
                angleAccuracy = 0
            } else if angleAccuracy <= 30 {
                angleAccuracy = 3
            } else if angleAccuracy <= 50 {
                angleAccuracy = 2
            } else {
                angleAccuracy = 1
            }
            
        }
    }
    
    mutating func updateMotion() {
        if motion.isGyroAvailable {
            motion.startDeviceMotionUpdates()
            if let data = motion.deviceMotion {
                pitch = (180/Double.pi)*data.attitude.pitch
                roll = (180/Double.pi)*data.attitude.roll
                
            }
        }
    }
    
    mutating func updateAccelerometers() {
        
       // Make sure the accelerometer hardware is available.
       if motion.isAccelerometerAvailable {
             if let data = motion.accelerometerData {
               
                self.dataAccelerometers[0] = Float(data.acceleration.x)
                self.dataAccelerometers[1] = Float(data.acceleration.y)
                self.dataAccelerometers[2] = Float(data.acceleration.z)
                
                dataAccelerometersArray.append(dataAccelerometers)
    }
    }
        
    }
    
    mutating func updateMagnetometers() {

       // Make sure the accelerometer hardware is available.
       if motion.isMagnetometerAvailable {
        
             if let data = motion.magnetometerData {
                self.dataMagnetometers[0] = Float(data.magneticField.x)
                self.dataMagnetometers[1] = Float(data.magneticField.y)
                self.dataMagnetometers[2] = Float(data.magneticField.z)
                
                    dataMagnetoArray.append(dataMagnetometers)
    }
    }

    }
    
    func toJson() -> String {
        let encodedData = try! JSONEncoder().encode(self)
        let jsonString = String(data: encodedData,
                                encoding: .utf8)!
        return jsonString
    }
}

