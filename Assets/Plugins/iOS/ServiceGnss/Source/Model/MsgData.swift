//
//  MsgData.swift
//  SyslorGnssFramework
//
//  Created by Baptiste on 04/08/2021.
//

import Foundation

struct MsgData : Codable {
    var msg:String = "";
    var vars:[String] = [];
    var tag:String = "";
    var code:Int = 0;
    var critic:Bool = false;
    var iterate:Bool = true;

    func toJson() -> String {
        let encodedData = try! JSONEncoder().encode(self)
        let jsonString = String(data: encodedData,
                                encoding: .utf8)!
        return jsonString
    }
}

