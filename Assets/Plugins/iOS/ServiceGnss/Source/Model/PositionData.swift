//
//  PositionData.swift
//  SyslorGnssFramework
//
//  Created by Baptiste on 04/08/2021.
//

import Foundation

struct PositionData : Codable {
    var time:Double = -1
    var lat:Double = 0.0
    var lon:Double = 0.0
    var fixType:Int = 10
    var satsTracked:Int = 0
    var correctionAge:String = "?"
    var elevation:Float = 0
    
    var speed:Float = 0
    var heading:Float = 0
    var dgpsStationId:String = ""
    
    var gstCq2d:Float = 0
    var gstCq3d:Float = 0
    
    var gsaPdop:Float = 0
    var gsaHdop:Float = 0
    var gsaVdop:Float = 0
    
    func toJson() -> String {
        let encodedData = try! JSONEncoder().encode(self)
        let jsonString = String(data: encodedData,
                                encoding: .utf8)!
        return jsonString
    }
    
    public mutating func PositionData(data: NmeaParser.GPSData) {
        self.time = Double(String(format:"%.0f", data.deltaTimeSeconds)) ?? -1
        if (self.time.isNaN) {
            self.time = -1
        }
        self.lat = Double(String(format:"%2.8f", data.latitude)) ?? 0
        self.lon = Double(String(format:"%2.8f", data.longitude)) ?? 0
        self.elevation = Float(String(format:"%2.8f", data.altitude + data.altitudeCorrection)) ?? 0
        self.fixType = data.fixType.rawValue
        self.satsTracked = data.satCount
        self.speed = Float(data.speedKph)
        self.dgpsStationId = data.stationID
        
        self.gstCq2d = Float(String(format: "%.4f", data.CQ2D)) ?? 0
        self.gstCq3d = Float(String(format: "%.4f", data.CQ3D)) ?? 0
        self.gsaPdop = Float(String(format: "%.4f", data.PDOP)) ?? 0
        self.gsaHdop = Float(String(format: "%.4f", data.HDOP)) ?? 0
        self.gsaVdop = Float(String(format: "%.4f", data.VDOP)) ?? 0
    }
}
