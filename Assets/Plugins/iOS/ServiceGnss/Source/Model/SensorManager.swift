//
//  SensorManager.swift
//  SyslorGnssFramework
//
//  Created by Baptiste on 24/08/2021.
//

import Foundation

struct SensorManager {
    
    
    private static var sTempMatrix:[Float] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    
    
    public static func getRotationMatrix( _ R: inout [Float], _ I: inout [Float], _ gravity: [Float], _ geomagnetic: [Float]) -> Bool {
        //  TODO: move this to native code for efficiency
        var Ax: Float = gravity[0]
        var Ay: Float = gravity[1]
        var Az: Float = gravity[2]
        let normsqA: Float = (Ax * Ax) + (Ay * Ay) + (Az * Az)
        let g: Float = 9.81
        let freeFallGravitySquared: Float = 0.01 * g * g
        if normsqA < freeFallGravitySquared {
            //  gravity less than 10% of normal value
            return false
        }
        let Ex: Float = geomagnetic[0]
        let Ey: Float = geomagnetic[1]
        let Ez: Float = geomagnetic[2]
        var Hx: Float = (Ey * Az) - (Ez * Ay)
        var Hy: Float = (Ez * Ax) - (Ex * Az)
        var Hz: Float = (Ex * Ay) - (Ey * Ax)
        let normH: Float = Float(((Hx * Hx) + (Hy * Hy) + (Hz * Hz)).squareRoot())
        if normH < 0.1 {
            //  device is close to free fall (or in space?), or close to
            //  magnetic north pole. Typical values are  > 100.
            return false
        }
        let invH: Float = 1.0 / normH
        Hx = Hx * invH
        Hy = Hy * invH
        Hz = Hz * invH
        let invA: Float = Float(1.0 / ((Ax * Ax) + (Ay * Ay) + (Az * Az)).squareRoot())
        Ax = Ax * invA
        Ay = Ay * invA
        Az = Az * invA
        let Mx: Float = (Ay * Hz) - (Az * Hy)
        let My: Float = (Az * Hx) - (Ax * Hz)
        let Mz: Float = (Ax * Hy) - (Ay * Hx)
        if R.count != 0 {
            if R.count == 9 {
                R[0] = Hx
                R[1] = Hy
                R[2] = Hz
                R[3] = Mx
                R[4] = My
                R[5] = Mz
                R[6] = Ax
                R[7] = Ay
                R[8] = Az
            } else {
                if R.count == 16 {
                    R[0] = Hx
                    R[1] = Hy
                    R[2] = Hz
                    R[3] = 0
                    R[4] = Mx
                    R[5] = My
                    R[6] = Mz
                    R[7] = 0
                    R[8] = Ax
                    R[9] = Ay
                    R[10] = Az
                    R[11] = 0
                    R[12] = 0
                    R[13] = 0
                    R[14] = 0
                    R[15] = 1
                }
            }
        }
        if I.count != 0 {
            let invE: Float = Float(1.0 / ((Ex * Ex) + (Ey * Ey) + (Ez * Ez)).squareRoot())
            let c: Float = ((Ex * Mx) + (Ey * My) + (Ez * Mz)) * invE
            let s: Float = ((Ex * Ax) + (Ey * Ay) + (Ez * Az)) * invE
            if I.count == 9 {
                I[0] = 1
                I[1] = 0
                I[2] = 0
                I[3] = 0
                I[4] = c
                I[5] = s
                I[6] = 0
                I[7] = -s
                I[8] = c
            } else {
                if I.count == 16 {
                    I[0] = 1
                    I[1] = 0
                    I[2] = 0
                    I[4] = 0
                    I[5] = c
                    I[6] = s
                    I[8] = 0
                    I[9] = -s
                    I[10] = c
                    I[14] = 0
                    I[13] = 0
                    I[12] = 0
                    I[11] = 0
                    I[7] = 0
                    I[3] = 0
                    I[15] = 1
                }
            }
        }
        return true
    }
    
    public static func remapCoordinateSystem(_ inR: [Float], _ X: Int, _ Y: Int, _ outR: inout [Float]) -> Bool {
        if inR == outR {
            var temp: [Float] = sTempMatrix
            synced(temp) {
            //  we don't expect to have a lot of contention
                if remapCoordinateSystemImpl(inR, X, Y, &temp) {
                    let size: Int = outR.count
                for i in 0 ... size - 1 {
                    outR[i] = temp[i]
                }
                return true
            }
                return remapCoordinateSystemImpl(inR, X, Y, &outR)
            }
        }
        return remapCoordinateSystemImpl(inR, X, Y, &outR)
    }
    
    private static func remapCoordinateSystemImpl(_ inR: [Float], _ X: Int, _ Y: Int, _ outR: inout [Float]) -> Bool {
        let length: Int = outR.count
        if inR.count != length {
            return false
            //  invalid parameter
        }
        if ((X & 0x7C) != 0) || ((Y & 0x7C) != 0) {
            return false
            //  invalid parameter
        }
        if ((X & 0x3) == 0) || ((Y & 0x3) == 0) {
            return false
            //  no axis specified
        }
        if (X & 0x3) == (Y & 0x3) {
            return false
            //  same axis specified
        }
        //  Z is "the other" axis, its sign is either +/- sign(X)*sign(Y)
        //  this can be calculated by exclusive-or'ing X and Y; except for
        //  the sign inversion (+/-) which is calculated below.
        var Z: Int = X ^ Y
        let x: Int = (X & 0x3) - 1
        let y: Int = (Y & 0x3) - 1
        let z: Int = (Z & 0x3) - 1
        let axis_y: Int = (z + 1) % 3
        let axis_z: Int = (z + 2) % 3
        if ((x ^ axis_y) | (y ^ axis_z)) != 0 {
            Z = Z ^ 0x80
        }
        let sx: Bool = X >= 0x80
        let sy: Bool = Y >= 0x80
        let sz: Bool = Z >= 0x80
        let rowLength: Int = (length == 16 ? 4 : 3)
        for j in 0 ... 3 - 1 {
            let offset: Int = j * rowLength
            for i in 0 ... 3 - 1 {
                if x == i {
                    outR[offset + i] = (sx ? -inR[offset + 0] : inR[offset + 0])
                }
                if y == i {
                    outR[offset + i] = (sy ? -inR[offset + 1] : inR[offset + 1])
                }
                if z == i {
                    outR[offset + i] = (sz ? -inR[offset + 2] : inR[offset + 2])
                }
            }
        }
        if length == 16 {
            outR[14] = 0
            outR[13] = 0
            outR[12] = 0
            outR[11] = 0
            outR[7] = 0
            outR[3] = 0
            outR[15] = 1
        }
        return true
    }
}

public func synced(_ lock: Any, closure: () -> Bool) {
    objc_sync_enter(lock)
    closure()
    objc_sync_exit(lock)
}
