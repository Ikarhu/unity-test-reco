
//
//  UBXValGet.swift
//  ServiceGNSS
//
//  Created by ivirtual on 22/03/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation


class Ublox{
    
    public static func Scenario1()->[UInt8]
    {
        let key:UBXKey = UBXKey.Instance;

        // Set default parameters for ZED-F9P (RAM+BBR+FLASH) with UBXValset
        let valset:UBXValSet = UBXValSet(value: UBXMessage.STORE.RAM);
        valset.DynModel(value: UBXMessage.DYNMODEL.PORTABLE);
        valset.Rate_Meas(value: 1000);//1000ms
        valset.Rate_Nav(value: 1); //1 solutions
        var res = valset.getByte()
        print("----->>",valset.getHex());
        return res
    }
    
    public static func Scenario2()->[UInt8]
    {
        let key:UBXKey = UBXKey.Instance;
           // High speed scenario
        let valset:UBXValSet = UBXValSet(value : UBXMessage.STORE.RAM);
        valset.DynModel(value : UBXMessage.DYNMODEL.PEDESTRIAN);
        valset.Rate_Meas(value :500);
        valset.Rate_Nav(value :1);
        var res = valset.getByte();
        print("GETHEX", valset.getHex());
        return (res);
    }
    
    public static func TestUbloxMessages(bleMuCryptedCommunication:Bool,  MTU:Int)->[UInt8] {
        /*byte[] res = MuDesignMessageParser.UBLOX_MESSAGE.VERSION;
        bleMesssagesToWriteQueue.postMessage(res, bleMuCryptedCommunication, MTU);*/

// Example and unit test
        //UnitTest();
        let key:UBXKey = UBXKey.Instance;

        // Set default parameters for ZED-F9P (RAM+BBR+FLASH) with UBXValset
        let valset:UBXValSet = UBXValSet(value: UBXMessage.STORE.ALL);
        valset.DynModel(value: UBXMessage.DYNMODEL.PORTABLE);
        valset.Rate_Meas(value: 1000);//1000ms
        valset.Rate_Nav(value: 1); //1 solutions
        valset.MinElev(value: 15);
        valset.TimeOut(value: 30);
        valset.NMEAHighPrecision(value: true);
        valset.OutMessage(keys_value: true,keys: [ key.MSGOUT_GST_UART1, key.MSGOUT_GGA_UART1, key.MSGOUT_GSA_UART1]);
        
        valset.OutMessage(keys_value:false,keys: [key.MSGOUT_DTM_UART1, key.MSGOUT_GBS_UART1, key.MSGOUT_GLL_UART1, key.MSGOUT_GNS_UART1, key.MSGOUT_GRS_UART1, key.MSGOUT_GSV_UART1, key.MSGOUT_RMC_UART1, key.MSGOUT_VLW_UART1, key.MSGOUT_VTG_UART1, key.MSGOUT_ZDA_UART1]);
        
        valset.Uart1Bauderate(val: 115200);
        valset.Uart1InProt(keys_value: true, keys: [key.UART1_INPROT_RTCM3, key.UART1_INPROT_UBX, key.UART1_INPROT_NMEA]);
        valset.Uart1OutProt(keys_value: true, keys: [key.UART1_OUTPROT_NMEA, key.UART1_INPROT_UBX]);
        valset.Uart1OutProt(keys_value: false, keys: [key.UART1_OUTPROT_RTCM3]);
        print(valset.getHex());
        return valset.getByte()
        
        //byte[] res = new com.syslor.gn3syslorsrv.Ubx.UBXMessage().getMonVersion();
        //bleMesssagesToWriteQueue.postMessage(valset.getByte(), bleMuCryptedCommunication, MTU);

        /* // Set dynamic parameters for pedestrian use case (RAM)
        valset = new UBXValSet(com.syslor.gn3syslorsrv.Ubx.UBXMessage.STORE.RAM);
        valset.DynModel(com.syslor.gn3syslorsrv.Ubx.UBXMessage.DYNMODEL.PEDESTRIAN);
        valset.Rate_Nav(500);
        valset.Rate_Nav(1);


        // Get parameters for ZED-F9P (RAM) with UBXValget
        UBXValGet valget = new UBXValGet(com.syslor.gn3syslorsrv.Ubx.UBXMessage.STORE.RAM);
        valget.DynModel();
        valget.Rate_Meas();
        valget.Rate_Nav();
        valget.MinElev();
        valget.TimeOut();
        valget.NMEAHighPrecision();
        valget.OutMessage(key.MSGOUT_GST_UART1, key.MSGOUT_GGA_UART1, key.MSGOUT_GSA_UART1);
        valget.OutMessage(key.MSGOUT_DTM_UART1, key.MSGOUT_GBS_UART1, key.MSGOUT_GLL_UART1, key.MSGOUT_GNS_UART1, key.MSGOUT_GRS_UART1, key.MSGOUT_GSV_UART1, key.MSGOUT_RMC_UART1, key.MSGOUT_VLW_UART1, key.MSGOUT_VTG_UART1, key.MSGOUT_ZDA_UART1);
        valget.Uart1Bauderate();
        valget.Uart1InProt(key.UART1_INPROT_NMEA, key.UART1_INPROT_RTCM3, key.UART1_INPROT_UBX);
        valget.Uart1OutProt(key.UART1_OUTPROT_NMEA, key.UART1_OUTPROT_UBX,key.UART1_OUTPROT_RTCM3);


        // Get mon version
        byte[] res = new com.syslor.gn3syslorsrv.Ubx.UBXMessage().getMonVersion();
        bleMesssagesToWriteQueue.postMessage(res, bleMuCryptedCommunication, MTU);*/

    }
}
