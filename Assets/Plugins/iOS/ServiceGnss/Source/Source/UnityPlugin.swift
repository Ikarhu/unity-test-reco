//
//  UnityPlugin.swift
//  ServiceGNSS
//
//  Created by Vaxelaire Lucas on 15/02/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation
import CoreLocation
import CoreMotion
import UIKit
import SwiftUI

@available(iOS 14.0, *)
@objc public class UnityPlugin : NSObject, ServiceGNSSDelegate {
    
    @objc public var msgAccuracy:String = "";
    @objc public var msgFixMode:String = "";
    
    @objc public var msgMuDesignInfo:String = "";
    @objc public var msgMuDesignBattery:String = "";
    @objc public var msgMuDesignMagneto:String = "";
    @objc public var msgMuDesignStats:String = "";
    @objc public var msgMuDesignConfAckAnswer:Bool = false;
    
    @objc public var msgLogData:String = "";
    @objc public var msgRotationAngle:String = "";
    
    @objc public var MACADDRESS:String = "";
    @objc public var SERVERIPNTRIP:String = "";
    @objc public var SERVERPORT:String = "";
    @objc public var USERNAME:String = "";
    @objc public var PASSWORD:String = "";
    @objc public var MOUNTPOINT:String = "";
    
    @objc public var USESCENARIO:Int = 0;

    var orientationAngle:OrientationAngle = OrientationAngle(dataAccelerometers: [0,0,0], dataMagnetometers: [0,0,0], dataAccelerometersArray: [], dataMagnetoArray: [])
    var msgBleData:MsgData?
    var msgNtripData:MsgData?
    var positionData:PositionData?
    var fixModeData:MsgData?
    var msgRotationAngleStatus:MsgData?
    var timer:Timer?
    
    
    var Matrix:[Float] = [0,0,0,0,0,0,0,0,0]
    
    
    let monitorNetwork:NetworkMonitor = NetworkMonitor()
    
    @objc public  func SetScenario(scenario:Int) {
        USESCENARIO = scenario;
    }
    
    @objc public func SetMacAdressIos(string:NSString){
        MACADDRESS = string as String;
       
    }
    @objc public func SetServerIpNtripIos(string:NSString){
        SERVERIPNTRIP = string as String;
    }
    @objc public func SetServerPortIos(string:NSString){
        SERVERPORT = string as String;
      
        
    }
    @objc public func SetUsernameIos(string:NSString){
        USERNAME = string as String;
       
        
    }
    @objc public func SetPasswordIos(string:NSString){
        PASSWORD = string as String;
       
    }
    @objc public func SetMountPointIos(string:NSString){
        MOUNTPOINT = string as String;
       
    }
    
    @objc public func GetMsgBleIos() -> NSString{
        if (msgBleData != nil) {
            return msgBleData!.toJson() as NSString;
        }
        return ""
    }
    
    @objc public func GetMsgNtripIos() -> NSString{
        if (msgNtripData != nil) {
            return msgNtripData!.toJson() as NSString;
        }
        return ""
    }
    
    @objc public func GetRotationAngleStatusIos() -> NSString{
        
        if (msgRotationAngleStatus != nil ) {
            return msgRotationAngleStatus!.toJson() as NSString;
        }
        return ""
    }
    
    @objc public func GetRotationAngleIos() -> NSString{
        didUpdateMsgRotationAngle(data: self.orientationAngle)
        return msgRotationAngle as NSString;
    }
    
    
    @objc public func GetAccuracyIos() -> NSString{
        return msgAccuracy as NSString;
    }
    @objc public func GetLocalisationIos() -> NSString{
        if (positionData != nil) {
            return positionData!.toJson() as NSString
        }
            return ""
    }
    @objc public func GetFixModeIos() -> NSString{
        if (fixModeData != nil) {
            return fixModeData!.toJson() as NSString;
        }
        return ""
    }
    @objc public func GetMuDesignInfoDataIos() -> NSString{
        return msgMuDesignInfo as NSString;
    }
    @objc public func GetMuDesignBatteryDataIos() -> NSString{
        return msgMuDesignBattery as NSString;
    }
    @objc public func GetMuDesignMagnetoDataIos() -> NSString{
        return msgMuDesignMagneto as NSString;
    }
    @objc public func GetMuDesignStatsDataIos() -> NSString {
        return msgMuDesignStats as NSString;
    }
    @objc public func GetMuDesignConfAckAnswerIos() -> Bool{
        return msgMuDesignConfAckAnswer;
    }
    
    @objc public func GetLogDataIos() -> NSString{
        return msgLogData as NSString;
    }
    
    let debug:Bool = true
    let TAG="     ¡¡ UnityPlugin ¡¡ "
    
    var serviceGnss: ServiceGnss?
   
    @objc public static let shared = UnityPlugin()
    
    func didUpdateProgressUpdateFirmeware(progress: Int) {
        
    }
    func didUpdateGPSData(data: NmeaParser.GPSData) {
        msgAccuracy = String(format: "%.4f;%.4f;%.4f;%.4f;%.4f", data.PDOP, data.HDOP, data.VDOP, data.CQ2D, data.CQ3D);
        
        //TODO ELEVATION (altitude) ELEVATION CORRECTION?
        
        // Done
        // data.geoidalSeparation <===> ElevationCorrection
        // data.altitude <===> Elevation
        

        msgFixMode = data.fixType.description;
        fixModeData = MsgData(msg: data.fixType.description, vars: [""], tag: "GNSS", code: 1100 + data.fixType.rawValue, critic: false, iterate: false)
        positionData?.PositionData(data: data)
        //TODO les logs que nous récuperons sur android elles sont ou sur IOS
        // ????
        //GET MsgLogData => (android) fonction UpdateLogAppend
    }
    func didUpdateMuMagnetoData(data: ServiceGnss.MuDesignMagnetoData) {
        msgMuDesignMagneto = data.toJson()
    }
    
    func didUpdateMuInfoData(data: ServiceGnss.MuInfoData) {
        
        msgMuDesignInfo = data.toJson()
        }
    
    func didUpdateMuStatsData(data: ServiceGnss.MuStatsData) {
        msgMuDesignStats = data.toJson()
    }
    
    func didUpdateMuBatteryData(data: ServiceGnss.MuBatteryData) {
        msgMuDesignBattery = data.toJson()
    }
    
    func didUpdateMuConfAckAnswer(data: Bool) {
        msgMuDesignConfAckAnswer = data
    }
    
    func didUpdateMsgRotationAngle(data: OrientationAngle) {
                msgRotationAngle = data.toJson()
        if(data.angleAccuracy != 0 && data.angleAccuracy < 3) {
                    msgRotationAngleStatus = MsgData(msg:"Error with smartphone sensor. Please, contact us", vars:[""], tag:"SENSOR", code:1301, critic:true, iterate: true)
                } else {
                    msgRotationAngleStatus = MsgData(msg:"", vars:[""], tag:"", code:0, critic:false, iterate: false)
                }
    }
    
    func didUpdateBleState(state: ServiceGnss.BleState, messsage: String) {
        switch state {
        case ServiceGnss.BleState.NotConnected:
            msgBleData = MsgData(msg: "Bluetooth disconnected...", vars: [""], tag: "BLE", code: 1018, critic: true, iterate: false)
            //msgLogData = "@BLE_DISCONNECTED...";
            
            break;
        case ServiceGnss.BleState.DeviceNotFound:
            msgBleData = MsgData(msg: "Bluetooth device not found", vars: [""], tag: "BLE", code: 1019, critic: true, iterate: false)
            //msgLogData = "@BLE_DEVICE_NOT_FOUND"
            break;
        case ServiceGnss.BleState.DeviceConnected:
            msgBleData = MsgData(msg: "Bluetooth connected...", vars: [""], tag: "BLE", code: 1015, critic: false, iterate: false)
            //msgLogData = "@BLE_CONNECTED...";
            break;
        case ServiceGnss.BleState.IsSyslorDevice:
            break;
        case ServiceGnss.BleState.StreamCallBack:
            msgBleData = MsgData(msg: "Bluetooth stream data", vars: [""], tag: "BT", code: 1017, critic: false, iterate: false)
            //msgLogData = "@BT_STREAM_DATA";
            break;
        }
    }
    
    func didUpdateNtripState(state: ServiceGnss.NtripState, messsage: String) {
        switch state {
        case ServiceGnss.NtripState.NotConnected:
            msgNtripData = MsgData(msg: "Network: Disconnected", vars: [""], tag: "NTRIP", code: 1211, critic: false, iterate: false)
            break;
        case ServiceGnss.NtripState.HostConnected:
            msgNtripData = MsgData(msg: "Connected to the network #1 : #2", vars: [SERVERIPNTRIP, SERVERPORT], tag: "NTRIP", code: 1208, critic: false, iterate: false)
            break;
        case ServiceGnss.NtripState.HostAuthenticationOk:
            break;
        case ServiceGnss.NtripState.AuthenticationError:
            msgNtripData = MsgData(msg: "Unknown login (#1) or password (#2) for current mountpoint (access denied).", vars: [USERNAME, PASSWORD], tag: "NTRIP", code: 1205, critic: true, iterate: false)
            break;
        case ServiceGnss.NtripState.AntennaError:
            msgNtripData = MsgData(msg: "Error Antenna Connection", vars: [""], tag: "NTRIP", code: 1223, critic: true, iterate: false)
            break;
        case ServiceGnss.NtripState.InternetError:
            msgNtripData = MsgData(msg:"Error Internet connexion", vars: [""], tag: "NTRIP", code: 1224, critic: true, iterate: false)
            break;
        case ServiceGnss.NtripState.InternetConnected:
            msgNtripData = MsgData(msg:"Internet connected", vars: [""], tag: "NTRIP", code: 1225, critic: false, iterate: false)
            break;
        }
    }
    
    func InitialiseData(){
        positionData = PositionData(time: -1, lat: 0, lon: 0, fixType: 10, satsTracked: 0, correctionAge: "?", elevation: 0, speed: 0, heading: 0, dgpsStationId: "", gstCq2d: 0, gstCq3d: 0, gsaPdop: 0, gsaHdop: 0, gsaVdop: 0)
        msgBleData = MsgData(msg: "", vars: [""], tag: "", code: 0, critic: false, iterate: false)
        msgNtripData = MsgData(msg: "", vars: [""], tag: "", code: 0, critic: false, iterate: false)
        fixModeData = MsgData(msg: "", vars: [""], tag: "", code: 0, critic: false, iterate: false)
        msgRotationAngleStatus = MsgData(msg: "", vars: [""], tag: "", code: 0, critic: false, iterate: false)
        msgAccuracy = "";
        msgFixMode = "";
        msgLogData = "";
        msgMuDesignInfo = "";
        msgMuDesignStats = "";
        msgMuDesignBattery = "";
        msgMuDesignConfAckAnswer = false;
        msgMuDesignMagneto = "";
        msgRotationAngle = "";
      }
   
    @objc func log(msg: String){
            print(TAG,msg)
    }
    
    @objc public  func computemean(array:[[Float]]) -> [Float] {
        var float1:Float = 0
        var float2:Float = 0
        var float3:Float = 0
            
            for val in array {
                float1 += val[0]
                float2 += val[1]
                float3 += val[2]
            }
             
        let mean0:Float = float1 / Float(array.count)
        let mean1:Float = float2 / Float(array.count)
        let mean2:Float = float3 / Float(array.count)
                
            return [mean0,mean1,mean2]
    }
    
    func ScreenOrientationChanged() {
        var newRotMtx: [Float] = [0,0,0,0,0,0,0,0,0]
        let AXIS_X:Int = 1
        let AXIS_Y:Int = 2

        
        let AXIS_MINUS_X = AXIS_X | 0x80
        let AXIS_MINUS_Y = AXIS_Y | 0x80
        
        switch UIDevice.current.orientation {
        case .portrait:
            newRotMtx = self.Matrix
            self.orientationAngle.screenOrientation = 0
        case .landscapeRight:
            SensorManager.remapCoordinateSystem(Matrix, AXIS_Y, AXIS_MINUS_X, &newRotMtx)
            self.orientationAngle.screenOrientation = 90
        case .portraitUpsideDown:
            SensorManager.remapCoordinateSystem(Matrix, AXIS_MINUS_Y, AXIS_MINUS_X, &newRotMtx)
            self.orientationAngle.screenOrientation = 180
        case .landscapeLeft:
            SensorManager.remapCoordinateSystem(Matrix, AXIS_MINUS_Y, AXIS_X, &newRotMtx)
            self.orientationAngle.screenOrientation = 270
        case .unknown:
            break
        case .faceUp:
            break
        case .faceDown:
            break
        @unknown default:
            break
        }
        self.orientationAngle.rotationMatrix = newRotMtx
    }
    
    @objc public  func listenSensorChanged() -> Void {
        var I:[Float] = []
        self.timer = Timer(fire: Date(), interval: (1.0/60.0),
              repeats: true, block: { (timer) in
                self.orientationAngle.updateAccelerometers()
                self.orientationAngle.updateMagnetometers()
                self.orientationAngle.updateMotion()
                self.orientationAngle.updateLocation()

                if (self.orientationAngle.dataAccelerometersArray.count == 5 && self.orientationAngle.dataMagnetoArray.count == 5) {
                    
                    self.orientationAngle.meanMagnetometerReading = self.computemean(array: self.orientationAngle.dataMagnetoArray)
                    self.orientationAngle.meanAccelerometerReading = self.computemean(array: self.orientationAngle.dataAccelerometersArray)

                    
                    SensorManager.getRotationMatrix(&self.Matrix, &I, self.orientationAngle.meanAccelerometerReading, self.orientationAngle.meanMagnetometerReading)
                    self.ScreenOrientationChanged()
                    
                    
                    self.didUpdateMsgRotationAngle(data: self.orientationAngle)
                    
                    self.orientationAngle.dataAccelerometersArray.removeAll()
                    self.orientationAngle.dataMagnetoArray.removeAll()
                
                }
              })
        RunLoop.current.add(self.timer!, forMode: .default)
    }
    
    @objc public  func ConnectActIos() -> Void
    {
        monitorNetwork.startMonitoring()
        
        InitialiseData();
        orientationAngle.initData()
        listenSensorChanged()
        //serviceGnss?.device?.activateCrypt()
       // serviceGnss?.device?.desactivateCrypt()
        //serviceGnss?.connect(macAdress:"f0:08:d1:85:97:da", url: "78.24.131.136",port: 2101,user: "SYSLORQS",pass: "01041910",mountPoint: "RTKMSM",device: device)
        log(msg: MACADDRESS + " " + SERVERIPNTRIP + " " + SERVERPORT + " " + USERNAME + " " + PASSWORD + MOUNTPOINT);
            serviceGnss?.UbloxSendScenario(Scenario: USESCENARIO, macAdress:MACADDRESS, url: SERVERIPNTRIP,port: UInt16(SERVERPORT)!,user: USERNAME,pass: PASSWORD,mountPoint: MOUNTPOINT, key: "1234567890ABCDEF")
    }
    
    @objc public  func DisconnectActIos()
    {
        monitorNetwork.stopMonitoring()
        InitialiseData()
        serviceGnss?.disconnect()
        timer?.invalidate()
        timer = nil

    }
    
    var device: BTDevice?
    
    @objc public func InitIos(){
        
        serviceGnss = ServiceGnss()
        serviceGnss?.delegate = self
        log(msg: "Service init");
      
    }
}
