//
//  Helpers.swift
//  IOSPluginTestFramework
//
//  Created by Tim Regan on 13/02/2017.
//  Copyright © 2017 Tim Regan. All rights reserved.
//

import Foundation

@objc public class Helpers : NSObject {
    @objc public static let shared = Helpers()
    @objc public func square(number:Int32) -> Int32 {
        return number * number;
    }
    
    @objc public let welcome = "Bonjour";
    
    @objc public func getWelcomeString() -> NSString {
        return welcome as NSString;
    }
}
