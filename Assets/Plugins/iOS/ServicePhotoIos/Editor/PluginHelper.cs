using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class PluginHelper : MonoBehaviour
{
    [SerializeField] private Text textResult;

    [DllImport("__Internal")]
    private static extern void LoadPhoto();

    void Start()
    {
        LoadPhoto();
    }
}