//
//  ViewController.swift
//  SyslorGnssFramework
//
//  Created by Baptiste on 06/01/2022.
//

import UIKit
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    class func loadStoryboard() -> UIViewController {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraView") as! UIViewController
        return vc

    } 

    public func presentViewController() {
        let viewController = self // I had viewController passed in as a function,
                                  // but otherwise you can do this

        let vc = ViewController.loadStoryboard()
        // Present the view controller
        let currentViewController = UIApplication.shared.keyWindow?.rootViewController
        currentViewController?.dismiss(animated: true, completion: nil)

        if viewController.presentedViewController == nil {
            currentViewController?.present(vc, animated: true, completion: nil)
        } else {
            viewController.present(vc, animated: true, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
