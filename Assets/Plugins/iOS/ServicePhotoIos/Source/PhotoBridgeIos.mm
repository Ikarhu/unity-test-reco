//
//  IOSPluginTest.m
//  IOSPluginTest
//
//  Created by Tim Regan on 10/02/2017.
//  Copyright © 2017 Tim Regan. All rights reserved.
//

#import "PhotoBridgeIos.h"

#import "UnityFramework/UnityFramework-Swift.h"

#pragma mark - C interface

char* convertNSStringToCString(const NSString* nsString)
{
    if (nsString == NULL)
        return NULL;

    const char* nsStringUtf8 = [nsString UTF8String];
    //create a null terminated C string on the heap so that our string's memory isn't wiped out right after method's return
    char* cString = (char*)malloc(strlen(nsStringUtf8) + 1);
    strcpy(cString, nsStringUtf8);

    return cString;
}

NSString* CreateNSString (const char* string)
 {
   if (string)
     return [NSString stringWithUTF8String: string];
   else
         return [NSString stringWithUTF8String: ""];
 }

void LoadPhoto(){
    if (@available(iOS 14.0, *)) {
        [[PhotoBridgeIos shared] LoadPhoto];
    } else {
        // Fallback on earlier versions
    }
}

void ReceiveMsgFromUnity(int type, const char* string){
    [[PhotoBridgeIos shared] ReceiveMsgFromUnityWithType:(type) msg:CreateNSString(string)];
}

char  *SendTranslationToUnity() {
    return convertNSStringToCString([[PhotoBridgeIos shared] sendTranslationToUnity]);
}
