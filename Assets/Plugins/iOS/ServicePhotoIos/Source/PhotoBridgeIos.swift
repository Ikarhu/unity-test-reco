//
//  UnityPlugin.swift
//  ServiceGNSS
//
//  Created by Vaxelaire Lucas on 15/02/2021.
//  Copyright © 2021 Abdelhak. All rights reserved.
//

import Foundation
import CoreLocation
import CoreMotion
import UIKit
import SwiftUI


@available(iOS 14.0, *)
@objc public class PhotoBridgeIos : NSObject {
    
    class PhotoBridgeTranslation : Codable {
        let TAG="PhotoBridgeTranslation"
        var keys : [String] = []
        var sentences : [String] = []
        
        func translate(sentence: String) -> String {
                   if (keys.contains(sentence)) {
                       let index = sentences[keys.firstIndex(of: sentence) ?? 0]
                       return index
                   } else {
                       // Log.d(TAG, "getTranslation: ")
                       return ""
                   }
               }
    }
    
    let debug:Bool = true
    let TAG="debugServicePhotoiOS"
    
    var translation = PhotoBridgeTranslation()
    @objc public static let shared = PhotoBridgeIos()
    
    
    var recolementId: String = ""
    var photosNumber: Int = 0
    var sequence: Int = 0
    var jsonObject: [Any]  = []
    
    func log(msg: String){
        if debug{
            print(TAG,msg)
        }
    }

    enum ApbMsgType: Int
        {
            case Debug = 0
            case RecoId
            case PhotoAcquisitionSuccess
            case PhotoAcquisitionError
            case PhotoAcquisitionCancel
            case PhotoAcquisitionSetting
            case PhotoAcquisitionPhotosNumber
            case PhotoAcquisitionPhotosInfos
            case PhotoAcquisitionTranslationReceive
            case PhotoAcquisitionConversionError
            case PhotoAcquisitionSequenceNumber

        }
    
    public func SendMessage(type : Int, msg: String) {
        let jsonObject: NSMutableDictionary = NSMutableDictionary()
        jsonObject.setValue(type, forKey: "enum_type")
        jsonObject.setValue(msg, forKey: "message")
        
        let jsonData: NSData
        
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            UnityFramework.getInstance().sendMessageToGO(withName: "PhotoBridgeIos", functionName: "ReceiveMsg", message: jsonString)

        } catch _ {
            UnityFramework.getInstance().sendMessageToGO(withName: "PhotoBridgeIos", functionName: "ReceiveMsg", message: "Error sending data")
        }
        
    }

    @objc public func startActivity() {
        
        
        print("LoadPhoto() Swift")
        let viewController = ViewController.loadStoryboard()
        let vcDisplay = ViewController()
        vcDisplay.presentViewController()
    }
    
    @objc public func ReceiveMsgFromUnity(type: Int, msg: String) {
        let enumType = ApbMsgType(rawValue: type)
    
        switch enumType {
        case .Debug:
            if (msg == "unitTest") {
                log(msg: "Test 01 OK : Msg Receive From Unity, typeEnum: " + String(type) + ",Message: " + msg)
                log(msg:"Test 02 : Send Message To Unity")
                SendMessage(type: ApbMsgType.Debug.rawValue, msg: "unitTest")
            }
            break
        case .RecoId:
            recolementId = msg
            break
        case .PhotoAcquisitionSuccess:
            break
        case .PhotoAcquisitionError:
            break
        case .PhotoAcquisitionCancel:
            break
        case .PhotoAcquisitionSetting:
            break
        case .PhotoAcquisitionPhotosNumber:
            photosNumber = Int(msg) ?? 0
            break
        case .PhotoAcquisitionPhotosInfos: break
        case .PhotoAcquisitionTranslationReceive:
            break
        case .PhotoAcquisitionConversionError:
            break
        case .PhotoAcquisitionSequenceNumber:
            sequence = Int(msg) ?? 0
            break
        case .none:
            break
        }
        
    }
    
    @objc public func sendTranslationToUnity() -> String {
        let trad : [String] = [
            "recolement_start_capture_warning_box_not_fixeRtk",
            "text_popup_title",
            "informationPopupWarningText",
            "recolement_pause_warning_box_not_connected",
            "recolement_start_capture_warning_box_not_connected",
            "recolement_capturing_warning_box_not_connected",
            "Yes",
            "No",
            "handle_error_bad_identifier_password_for_mountpoint",
            "handle_error_bad_mountpoint",
            "handle_error_ntrip_con",
            "handle_error_error_rtk",
            "measuring_activity_warning_title_unothorized",
            "handle_error_bluetooth",
            "handle_error_bluetooth_user_msg",
            "measuring_activity_warning_title_Btfailed",
            "measuring_activity_message_cganotrecieved",
            "measuring_act_body_init_text",
            "measuring_act_init_text",
            "gnss_state_msg_init_text",
            "gnss_state_msg_box_connection_text",
            "problem_recovering_gnss_position_null_exception",
            "force_acquisition_message_popup",
            "No",
            "project_gps_firstuse_text",
            "calibration_cancel_btn_text",
            "helper_take_photo_hint",
            "recolement_validate"
        ]
        
        var translationListString = "";
        for string in trad {
            translationListString += string + ";"
        }
        return translationListString
    }
}
