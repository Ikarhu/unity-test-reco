//
//  SyslorGnssFramework.h
//  SyslorGnssFramework
//
//  Created by Vaxelaire Lucas on 18/02/2021.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ServicePhoto : NSObject
@end

#ifdef __cplusplus
extern "C" {
#endif

void LoadPhoto(void);
void ReceiveMsgFromUnity(int type, const char* string);
char *SendTranslationToUnity();
#ifdef __cplusplus
}
#endif
