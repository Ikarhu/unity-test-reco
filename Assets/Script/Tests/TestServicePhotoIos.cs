using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestServicePhotoIos : MonoBehaviour
{
    private const string TAG = "TestServiceIos";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        PhotoBridgeIos.Instance.ReceiveMessageCallback -= ReceiveMessageCallbackHandler;
        throw new NotImplementedException();
    }

    public void testCommIos()
    {
        PhotoBridgeIos.Instance.ReceiveMessageCallback += ReceiveMessageCallbackHandler;
        Debug.Log($"{TAG} ------ Launch Unit Test For Communication between ServicePhoto and iOS ------");
        Debug.Log($"{TAG} Unit test 01 : Send a Message to iOS");
        PhotoBridgeIos.Instance.SendMessage(0, "unitTest");
    }

    public void ReceiveMessageCallbackHandler(PhotoBridgeIos.ApbMsgType msgType, string s)
    {
        string test = "";
        if (msgType == PhotoBridgeIos.ApbMsgType.Debug && s == "unitTest")
        {
            test = $"{TAG} Unit test 01 : Message Receive On iOS, iOS Reply [{msgType},{s}] : OK";
        }
        else
            test = $"{TAG} Message Receive : Msg : {msgType} / String : {s}";
        
        Debug.Log(test);
    }
}
