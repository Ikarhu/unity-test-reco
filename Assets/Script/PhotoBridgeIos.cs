using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;

public class PhotoBridgeIos : MonoBehaviour
{
    class PhotoBridgeTranslation
    {
        public List<string> keys = new List<string>();
        public List<string> sentences = new List<string>();
    }

    public enum ApbMsgType
    {
        Debug,
        RecoId,
        PhotoAcquisitionSuccess,
        PhotoAcquisitionError,
        PhotoAcquisitionCancel,
        PhotoAcquisitionSetting,
        PhotoAcquisitionPhotosNumber,
        PhotoAcquisitionPhotosInfos,
        PhotoAcquisitionTranslationReceive,
        PhotoAcquisitionConversionError,
        PhotoAcquisitionSequenceNumber

    }

    [DllImport("__Internal")]
    private static extern void LoadPhoto();

    [DllImport("__Internal")]
    private static extern void ReceiveMsgFromUnity(int type, string message);

    /*[DllImport("__Internal")]
    private static extern string sendTranslationToUnity();*/

    private const string TAG = "debugServicePhotoiOS";

    #region Singleton implementation

    private static PhotoBridgeIos _instance;

    public static PhotoBridgeIos Instance
    {
        get
        {
            if (_instance == null)
            {
                var obj = new GameObject("PhotoBridgeIos");
                _instance = obj.AddComponent<PhotoBridgeIos>();
            }

            return _instance;
        }
    }

    void Start()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
#if DEBUG
        TestServicePhotoIos test = new TestServicePhotoIos();
        test.testCommIos();
#endif      
        LoadPhoto();
    }

    #endregion


    /* public void ReceiveTranslationFromIos()
     {
         string toTranslate = sendTranslationToUnity();
         if (toTranslate != null)
         {
             List<String> listTranslation = toTranslate.Split(';').ToList();
             PhotoBridgeTranslation photoBridgeTranslation = new PhotoBridgeTranslation();
             listTranslation.RemoveAll(string.IsNullOrWhiteSpace);
             for (int i = 0; i < listTranslation.Count; i++)
             {
                 string key = listTranslation[i];
                 string translation = "";
                 //LanguageManager.instance.translate(listTranslation[i]);
                 photoBridgeTranslation.keys.Add(key);
                 photoBridgeTranslation.sentences.Add(translation);
             }
             string translationJson = JsonUtility.ToJson(photoBridgeTranslation);
             SendMessage(8, translationJson);
         }
     }*/

    public void SendMessage(int type, string content)
    {
        ReceiveMsgFromUnity(type, content);
    }

    public class ReceiveMsgFromIos
    {
        public string message = "";
        public int enum_type = 0;

        public static ReceiveMsgFromIos CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<ReceiveMsgFromIos>(jsonString);
        }
    }

    public System.Action<ApbMsgType, string> ReceiveMessageCallback;

    public void ReceiveMsg(string msg)
    {
        Debug.Log($"{TAG} Message : {msg}");
        var json = ReceiveMsgFromIos.CreateFromJSON(msg);
        var typeEnum = (ApbMsgType) Enum.ToObject(typeof(ApbMsgType), json.enum_type);
        ReceiveMessageCallback.Invoke(typeEnum, msg);
    }
}
